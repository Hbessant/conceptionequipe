package src;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;


public class Organisation {
	
	private HashMap<Integer, Equipe> liste_equipe;
	private final static String FILE_NAME_EQUIPES = "resources/equipes.csv";
	private final String FILE_NAME_BENEVOLES = "resources/benevoles.csv";
    private static File FILE=Csv.getResource(FILE_NAME_EQUIPES);
	
	public Organisation() {
		this.liste_equipe=new HashMap<Integer, Equipe>();
		if(!(FILE.exists())) {
            FILE = new File(FILE_NAME_EQUIPES);
        }
	}

	public HashMap<Integer, Equipe> getListe_equipe() {
		return liste_equipe;
	}

	public void setListe_equipe(HashMap<Integer, Equipe> liste_equipe) {
		this.liste_equipe = liste_equipe;
	}
	
	public void createEquipeAutoDumb(int nb_pers_equipe) {
		double nb_benevoles=Csv.recupSizeList(this.FILE_NAME_BENEVOLES);
		int nb_equipe;
		double nb_equipe_temp =((double)nb_benevoles)/nb_pers_equipe;
		nb_equipe = (int)Math.ceil(nb_equipe_temp);
		int a=0;
		
		for(int i=0;i<nb_equipe;i++) {
			Equipe e=new Equipe(i+1);
			for(int j=a;j<a+nb_pers_equipe;j++) {
				if(j<nb_benevoles) {
					e.ajouterBenevole((Benevole)Csv.recupLigne(FILE_NAME_BENEVOLES, j,""));
				}
				
			}			
			e.completeInformationsEquipe();
			//System.out.println("OLA");
			this.liste_equipe.put(i, e);
			a+=nb_pers_equipe;
		}
		
	}
	
	
	//DEBUT SMART
	
		/**
		 * Fonction qui return vrai ou faux en fonction de si 
		 * il reste un benevole du niveau en parametre.
		 * @param int lvl le niveau minimum voulu
		 * @param List<Benevole> benevoles
		 * @return true si il en reste false sinon.
		 */
		public boolean encoreSpeleo(int lvl, List<Benevole> benevoles) {
			//Parcour de la liste de benevole
			for(int i=0;i<benevoles.size();i++) {
				//Si le benevole a le niveau voulu ou plus que le niveau voulu on return true
				if(lvl==0) {
					System.out.println(benevoles.get(i).getCompetenceSpeleo());
				}
				if(benevoles.get(i).getCompetenceSpeleo()>=lvl) {
					return true;
				}
			}
			//Pas de benevole trouvé ayant au minimun le niveau voulu
			return false;
		}
		
		/**
		 * Fonction qui return le prochain benevole libre en 
		 * fonction du niveau en parametre.
		 * @param int lvl le niveau minimum voulu
		 * @param List<Benevole> benevoles
		 * @return un benevole non affecté à une équipe.
		 */
		public Benevole prochainBenevoleSpeleo(int lvl,List<Benevole> benevoles) {
			//Initialisation du benevole qui va être retourner à null
			Benevole toReturn = null;
			//Si le niveau voulu est 2:
			if(lvl==2) {
				//Parcour de la liste de benevole
				for(int i=0;i<benevoles.size();i++) {
					//Quand un benevole est bien du niveau 2 on le place dans toReturn
					//Et on le supprime de la liste des benevoles à affecter
					if(benevoles.get(i).getCompetenceSpeleo()==2) {
						toReturn=benevoles.get(i);
						System.out.printf("Remove d'abonne\n");
						benevoles.remove(i);
						break;
					}
				}
			//Si le niveau voulu est minimum 1:
			}else if(lvl==1) {
				//Parcour de la liste de benevole
				for(int i=0;i<benevoles.size();i++) {
					//Quand un benevole est bien du niveau 1 on le place dans toReturn
					//Et on le supprime de la liste des benevoles à affecter
					if(benevoles.get(i).getCompetenceSpeleo()==1) {
						toReturn=benevoles.get(i);
						System.out.printf("Remove d'abonne\n");
						benevoles.remove(i);
						break;
					}
				} 
				if(toReturn==null) {
					//Parcour de la liste de benevole
					for(int i=0;i<benevoles.size();i++) {
						//Quand un benevole est bien du niveau 2 on le place dans toReturn
						//Et on le supprime de la liste des benevoles à affecter
						if(benevoles.get(i).getCompetenceSpeleo()==2) {
							toReturn=benevoles.get(i);
							System.out.printf("Remove d'abonne\n");
							benevoles.remove(i);
							break;
						}
					}
				}
			//Si le niveau voulu est minimum 1:
			}else if(lvl==0) {
				//Parcour de la liste de benevole
				for(int i=0;i<benevoles.size();i++) {
					//Quand un benevole est bien du niveau 0 on le place dans toReturn
					//Et on le supprime de la liste des benevoles à affecter
					if(benevoles.get(i).getCompetenceSpeleo()==0) {
						toReturn=benevoles.get(i);
						System.out.printf("Remove d'abonne\n");
						benevoles.remove(i);
						break;
					}
				}
				if(toReturn==null) {
					//Parcour de la liste de benevole
					for(int i=0;i<benevoles.size();i++) {
						//Quand un benevole est bien du niveau 1 on le place dans toReturn
						//Et on le supprime de la liste des benevoles à affecter
						if(benevoles.get(i).getCompetenceSpeleo()==1) {
							toReturn=benevoles.get(i);
							System.out.printf("Remove d'abonne\n");
							benevoles.remove(i);
							break;
						}
					}
				}
				if(toReturn==null) {
					//Parcour de la liste de benevole
					for(int i=0;i<benevoles.size();i++) {
						//Quand un benevole est bien du niveau 2 on le place dans toReturn
						//Et on le supprime de la liste des benevoles à affecter
						if(benevoles.get(i).getCompetenceSpeleo()==2) {
							toReturn=benevoles.get(i);
							System.out.printf("Remove d'abonne\n");
							benevoles.remove(i);
							break;
						}
					}
				}
			}
			return toReturn;
		}
		
		/**
		 * Fonction qui créer automatiquement les équipes en les
		 * équilibrant en fonction de leur niveau speléologique.
		 */
		public void createEquipeAutoSmartV1(int nb_pers_equipe) {
			List<Benevole> benevoles =new ArrayList<Benevole>();
			List<Object> list_bene2=Csv.recupListe(this.FILE_NAME_BENEVOLES, "");
			for(int a=0;a<list_bene2.size();a++) {
				benevoles.add((Benevole)list_bene2.get(a));
			}
			double nb_benevoles=Csv.recupSizeList(this.FILE_NAME_BENEVOLES);
			int nb_equipe;
			double nb_equipe_temp =((double)nb_benevoles)/nb_pers_equipe;
			nb_equipe = (int)Math.ceil(nb_equipe_temp);
			for(int i=0;i<nb_equipe;i++) {
				System.out.printf("\nEquipe n°");
				System.out.println(i);
				Equipe e=new Equipe(i+1);
				if(encoreSpeleo(2,benevoles)) {
					System.out.println("\nEquipe avec speleo2");
					e.ajouterBenevole(prochainBenevoleSpeleo(2,benevoles));
					System.out.println("ajout du beenvole lvl 2");
					while((nb_pers_equipe!=e.getNbBenevole())&&encoreSpeleo(0,benevoles)) {
						System.out.println("ajout des autres benevoles");
						if(encoreSpeleo(1,benevoles)) {
							e.ajouterBenevole(prochainBenevoleSpeleo(1,benevoles));
							System.out.println("ajout du benevole de lvl1");
						}else {
							e.ajouterBenevole(prochainBenevoleSpeleo(0,benevoles));
							System.out.println("ajout du benevole de lvl0");
						}
					}
				}else {
					System.out.println("Equipe sans speleo de lvl:2\n");
					System.out.printf("Nombre de perso par équipe ");
					System.out.println(nb_pers_equipe);
					System.out.printf("\nNombre de personne actuelement dans l'équipe ");
					System.out.println(e.getNbBenevole());
					System.out.printf("Encore spéléo de niveau 0 dispo: ");
					System.out.println(encoreSpeleo(0,benevoles));
					while((nb_pers_equipe!=e.getNbBenevole())&&encoreSpeleo(0,benevoles)) {
						System.out.println("ajout du benevole de lvl0 ou 1");
						e.ajouterBenevole(prochainBenevoleSpeleo(0,benevoles));
						
					}
				}
				System.out.println(e);
				e.completeInformationsEquipe();
				this.liste_equipe.put(i, e);
			}
		}
		
		
		
		//FIN SMART
	
	
	public void createEquipeManuel(HashMap<Integer, Equipe> liste_equipe) {
		for(Map.Entry<Integer, Equipe> liste: liste_equipe.entrySet()) {
			liste.getValue().completeInformationsEquipe();
		}
		this.liste_equipe=liste_equipe;
	}
	
	public Equipe recupEquipe(int id) {
		if(this.liste_equipe.get(id-1)!=null) {
			return this.liste_equipe.get(id-1); 
		}
		
		return null;
	}
	
	public void supprimerUneEquipe(int id) {
		if(this.liste_equipe.get(id)!=null) {
			this.liste_equipe.remove(id);
		}		
	}
	
	public void supprimerAllEquipe() {
		this.liste_equipe.clear();		
	}
	

	public String toString() {
		String str="";
		for(@SuppressWarnings("rawtypes") HashMap.Entry m : this.liste_equipe.entrySet()) {
			str+=this.liste_equipe.get(m.getKey()).toString();
		}
		return str;
	}
}
