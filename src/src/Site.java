package src;

import java.io.File;

/**
 * <b> Class Site qui permet de représenter un site. </b>
 * <p> Un site est caractérisé par:
 * <ul>
 * <li> Un String représentant le nom du site.</li>
 * <li> Un booléen représentant la nécessité d'une compétence spéléologique.</li> 
 * </ul></p>
 * <p> Entre autre cette class permet de gérer et de comparer les sites </p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.0
 */
public class Site {
	/**
	 * Le nom du site à modéliser.
	 * 
	 * @since 1.0
	 */
	private String nom;
	/**
	 * Si le site nécessite la compétence spéléo
	 * 
	 * @since 1.0
	 */
	private boolean speleoRequis;
	
	private Heure duree;
	
	private String coordonnees;
	
	private String equipementRequis;
	
	private final static String FILE_NAME = "resources/sites.csv";
	private static File FILE = Csv.getResource(FILE_NAME);
	
	public Site(String n,boolean s, Heure d,String c,String e) { 
		this.nom=n;
		this.speleoRequis=s;
		this.duree=d;
		this.coordonnees=c;
		this.equipementRequis=e;
		if(!(FILE.exists())) {
			FILE = new File(FILE_NAME);
		}
		
	}
	/**
	 * Retourne le nom du site.
	 * 
	 * @return une chaine de caractere représentant le nom du site.
	 * @since 1.0
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Met à jour le string représentant le nom du site.
	 * 
	 * @param nom le nouveau nom du site.
	 * @since 1.0
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Retourne si il faut la compétence spéléo ou pas.
	 * 
	 * @return true si il faut la compétence spéléo.
	 * @since 1.0
	 */
	public boolean isSpeleoRequis() {
		return speleoRequis;
	}
	/**
	 * Met à jour le booleen représentant si il faut avoir la compétence spéléo dans le site.
	 * 
	 * @param identificateur nécéssite la compétence ou pas.
	 * @since 1.0
	 */
	public void setSpeleoRequis(boolean speleoRequis) {
		this.speleoRequis = speleoRequis;
	}
	public Heure getDuree() {
		return duree;
	}
	public void setDuree(Heure duree) {
		this.duree = duree;
	}
	public String getCoordonnees() {
		return coordonnees;
	}
	public void setCoordonnees(String coordonnees) {
		this.coordonnees = coordonnees;
	}
	public String getEquipementRequis() {
		return equipementRequis;
	}
	public void setEquipementRequis(String equipementRequis) {
		this.equipementRequis = equipementRequis;
	}
	
}
