package src;
/**
 * <b> Class Voiture qui permet de représenter une voiture. </b>
 * <p> Une voiture est caractérisé par:
 * <ul>
 * <li> Un entier représentant un nombre de place dans la voiture.</li>
 * </ul></p>
 * <p> Entre autre cette classe permet de gérer et de comparer les voitures.</p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.0
 */
public class Voiture {
	/**
	 * Le nombre de place dans la voiture
	 * 
	 * @since 1.0
	 */
	private int nbPlace;
	private String immatriculation;
	private String modele;
	
	/**
	 * Retourne le nombre de place dans la voiture.
	 * 
	 * @return un entier représentant le nombre de place dans la voiture.
	 * @since 1.0
	 */
	public int getNbPlace() {
		return nbPlace;
	}
	/**
	 * Met à jour l'entier représentant le nombre de place d'une voiture.
	 * 
	 * @param nbPlace le nombre de place d'une voiture.
	 * @since 1.0
	 */
	public void setNbPlace(int nbPlace) {
		this.nbPlace = nbPlace;
	}
	
	public String getImmatriculation() {
		return immatriculation;
	}
	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}
	public String getModele() {
		return modele;
	}
	public void setModele(String modele) {
		this.modele = modele;
	}
	public Voiture(int nb_place, String immatriculation, String modele) {
		this.nbPlace=nb_place;
		this.immatriculation=immatriculation;
		this.modele=modele;
	}
	
	public String toString() {
		String str="				Nombre de place : "+this.nbPlace+"\n";
		str+="				Immatriculation : "+this.immatriculation+"\n";
		str+="				Modele : "+this.modele+"\n";	
		return str;
	}
	
}
