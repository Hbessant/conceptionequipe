package src;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;


public class FenetreV2 {
	private Shell shell;
	private Display display;
	private ArrayList<ScrolledComposite> sc;
	private ArrayList<Composite> c = new ArrayList<Composite>();
	private Date current_date=new Date();
	
	/**
	 * Constructeur pour lancer une fenetre par default
	 * @since 1.1
	 */
	FenetreV2(){
		setDisplay(new Display());
		shell = new Shell(getDisplay(), SWT.CLOSE | SWT.MAX  | SWT.RESIZE);
		shell.setSize((getDisplay().getPrimaryMonitor().getClientArea().width)-1000, getDisplay().getPrimaryMonitor().getClientArea().height-1000);
		
		shell.open();

	    while (!shell.isDisposed())
	      if (!getDisplay().readAndDispatch())
	        getDisplay().sleep();

	    getDisplay().dispose();
	}
	
	/**
	 * Constructeur pour lancer une fenetre avec une taille et un nom
	 * 
	 * @param name nom de la fenetre
	 * @param sizeX taille de la fenetre en X
	 * @param sizeY taille de la fenetre en y
	 * @since 1.1
	 */	
	FenetreV2(String name,int sizeX,int sizeY){
		setDisplay(new Display());
		shell = new Shell(getDisplay(), SWT.CLOSE | SWT.MAX  | SWT.RESIZE);
		shell.setSize(sizeX, sizeY);
	    shell.setText(name);
	    
	    setSc(new ArrayList<ScrolledComposite>());

	    shell.open();
	}
	
	/**
	 * Constructeur pour lancer une fenetre avec un nom
	 * 
	 * @param name nom de la fenetre
	 * @since 1.1
	 */	
	FenetreV2(String name){
		setDisplay(new Display());
		shell = new Shell(getDisplay(), SWT.CLOSE | SWT.MAX  | SWT.RESIZE);
		shell.setSize(getDisplay().getPrimaryMonitor().getClientArea().width-500, getDisplay().getPrimaryMonitor().getClientArea().height-200);
		shell.setText(name);
		
		setSc(new ArrayList<ScrolledComposite>());


	}
	
	FenetreV2(String name, Display d){
		setDisplay(d);
		shell = new Shell(getDisplay(), SWT.CLOSE | SWT.MAX  | SWT.RESIZE);
		shell.setSize(550,400);
		shell.setText(name);
		shell.setLayout(new GridLayout());
		
		setSc(new ArrayList<ScrolledComposite>());


	}
	
	void ajouterGroup(String name) {
		Group g = new Group(this.getShell(), SWT.BORDER);
		g.setText (name);
	}
	
	void AjouterScrolledComposite(Object o) {
		ScrolledComposite scc = null;
		if (o instanceof Shell) {
			scc = new ScrolledComposite((Shell) o ,SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
			//scc.setMinSize(((Shell) o).computeSize(SWT.DEFAULT, SWT.DEFAULT));
			
		}else if(o instanceof Group) {
			scc = new ScrolledComposite((Group) o ,SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
			scc.setMinSize(((Group) o).computeSize(SWT.DEFAULT, SWT.DEFAULT));
			
		}
		
		scc.setLayout(new GridLayout());
		scc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	    scc.setExpandHorizontal(true);
	    scc.setExpandVertical(true);
	    
		
		 getSc().add(scc);
		}
	
	void AjouterScrolledComposite(Object o,int x,int y) {
		ScrolledComposite scc = null;
		if (o instanceof Shell) {
			scc = new ScrolledComposite((Shell) o ,SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
			scc.setMinSize(((Shell) o).computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}else if(o instanceof Group) {
			scc = new ScrolledComposite((Group) o ,SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
			scc.setMinSize(((Group) o).computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}
		
		scc.setLayout(new GridLayout());

	    scc.setLocation(x, y);
	    
		
		 getSc().add(scc);
		}
	
	
	void AjouterCompositeWithSize(final int scrolled,int sizeX,int sizeY) {
		final Composite cc = new Composite(getSc().get(scrolled) ,SWT.NONE);
		
		getC().add(cc);
		
		 getSc().get(scrolled).setSize(sizeX, sizeY);
		 getSc().get(scrolled).setContent(cc);
		 
		
	}
	
	void AjouterComposite(final int scrolled,int sizeX,int sizeY) {
		final Composite cc = new Composite(getSc().get(scrolled) ,SWT.NONE);
		
		getC().add(cc);
		
		 getSc().get(scrolled).setMinSize(sizeX, sizeY);
		 getSc().get(scrolled).setContent(cc);
		 
		
	}
	
	void AjouterComposite(final int scrolled) {
		final Composite cc = new Composite(getSc().get(scrolled) ,SWT.NONE);
		
		getC().add(cc);
		
		 getSc().get(scrolled).setContent(cc);
		 
		
	}
	
	
	void addGridLayout(int nbColumns, Object o){
		GridLayout g = new GridLayout();
		g.numColumns = nbColumns;
		
		if(o instanceof Composite) {
			((Composite)o).setLayout(g);
		}else if(o instanceof ScrolledComposite) {
			((ScrolledComposite)o).setLayout(g);
		}
		
		
	}
	
	void addFillLayout(Object o){
		FillLayout f = new FillLayout();
		
		if(o instanceof Composite) {
			((Composite)o).setLayout(f);
		}else if(o instanceof ScrolledComposite) {
			((ScrolledComposite)o).setLayout(f);
		}
		
		
	}
	
	Button AjouterButtonPush(final Object o, String name, final String nameFonction) {
		Button button1 = null;
		if(o instanceof Shell) {
			button1 = new Button((Shell)o, SWT.NONE);
		}else if(o instanceof Composite) {
			button1 = new Button((Composite)o, SWT.NONE);
		}

	     button1.setText(name);
	     button1.addSelectionListener(new SelectionListener() {
	            
	            @Override
	            public void widgetSelected(SelectionEvent e) {
	            	if(o instanceof Shell) {
	            		searchFonction(nameFonction, (Shell) o);
	        		}else {
	        			searchFonction(nameFonction, getShell());
	        		}
	                
	            }
	            
	            @Override
	            public void widgetDefaultSelected(SelectionEvent e) {
	                // TODO Auto-generated method stub
	                
	            }
	        });
	     return button1;
	}
	
	Button AjouterButtonCheck(Composite c1) {
		Button b = new Button(c1, SWT.CHECK | SWT.FLAT);
		return b;
	}
	
	Combo AjouterCombo(Composite c1,String[] items) {
		final Combo combo = new Combo(c1, SWT.DROP_DOWN);
        combo.setItems(items);
        combo.setText(items[0]);
		return combo;
	}
	Combo AjouterComboRempli(Composite c1,String[] items,String s) {
		final Combo combo = new Combo(c1, SWT.DROP_DOWN);
        combo.setItems(items);
        combo.setText(s);
        return combo;
	}
	
	Label AjouterLabelBorder(Composite c1, String name) {
        Label l = new Label(c1, SWT.BORDER);
        l.setText(name);
        return l;
        
    } 
	
	Label AjouterLabel(Composite c1, String name,int[] valeurs) {
		Label l = new Label(c1, SWT.CENTER);
		l.setText(name);
		if(valeurs!=null) {
			l.setSize(valeurs[0],valeurs[1]);
			if(valeurs[2]!=-1 && valeurs[3]!=-1) {
				l.setLocation(valeurs[2],valeurs[3]);
			}
			
		}
		return l;
		
	}
	
	Label AjouterLabelPush(Composite c1, String name,int[] valeurs) {
		Label l = new Label(c1, SWT.CENTER | SWT.PUSH);
		l.setText(name);
		if(valeurs!=null) {
			l.setSize(valeurs[0],valeurs[1]);
			if(valeurs[2]!=-1 && valeurs[3]!=-1) {
				l.setLocation(valeurs[2],valeurs[3]);
			}
			
		}
		
		l.addMouseListener(new MouseAdapter() {
			   @Override
			   public void mouseUp(MouseEvent event) {
			      super.mouseUp(event);

			      if (event.getSource() instanceof Label) {
			         Label label = (Label)event.getSource();

			         label.setText(".....");
			      }
			   }
			});
		return l;
		
	}
	
	void AjouterExpandBar(Composite c1, List<String> valeurs, int[] valeurs2) {
		ExpandBar expandBar = new ExpandBar(c1, SWT.NONE);
        expandBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        final ExpandItem expandItem = new ExpandItem(expandBar, SWT.NONE);
        expandItem.setText(valeurs.get(0));
        expandBar.setLocation(valeurs2[2],valeurs2[3]);
        expandBar.setSize(valeurs2[0],valeurs2[1]);
        
        final Composite expandContent = new Composite(expandBar, SWT.NONE);
        expandContent.setLayout(new GridLayout());

        
        for(int i=1;i<valeurs.size();i++) {
        	final Label label = new Label(expandContent, SWT.NONE);
            label.setText(valeurs.get(i));
            
        }

        // Set the Composite as the control for the ExpandItem
        expandItem.setControl(expandContent);
        // Set the height of the ExpandItem to be the computed size of its content
        expandItem.setHeight(expandContent.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        expandItem.setExpanded(true);
		
	}
	
	Text AjouterTexte(Composite c1) {
		 Text text1 = new Text(c1, SWT.BORDER);
	     text1.setSize(200, 10);
		return text1;
	}
	
	Text AjouterTexteRempli(Composite c1,String s) {
		 Text text1 = new Text(c1, SWT.BORDER);
	     text1.setSize(200, 10);
	     text1.setText(s);
	     return text1;
	}
	
	Composite AjouterCompositeInCompiste(Composite c1) {
		Composite c2 = new Composite(c1, SWT.NONE);
		
		return c2;
	}
	
	Group getGroup(int g) {
		Control[] c = this.getShell().getChildren();
		return (Group) c[g];
	}
	
	void AjouterMenu(String[]menu,String[][]sous_Menu) {
		Menu Menu = new Menu(shell, SWT.BAR);
		for(int i=0;i<menu.length;i++) {
			final String M = menu[i];
			MenuItem m = new MenuItem(Menu, SWT.CASCADE);
			m.setText(menu[i]);
			Menu sous_m = new Menu(shell, SWT.DROP_DOWN);
			for(int j=0;j<sous_Menu[i].length;j++) {
				MenuItem sm = new MenuItem(sous_m, SWT.PUSH);
				final String s_M = sous_Menu[i][j];
				
				 if(s_M.equals("Quitter\tCtrl+Q")){
					sm.setAccelerator(SWT.CTRL+'Q');
				}
				
				sm.setText(sous_Menu[i][j]);
				sm.addSelectionListener(new SelectionListener() {
			      public void widgetSelected(SelectionEvent e) {
		                try {
							searchFonction(s_M,M);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			      }
			      public void widgetDefaultSelected(SelectionEvent e) {
			      }
				});
			}
			m.setMenu(sous_m);
		}
		shell.setMenuBar(Menu);
	}
	
	

	Shell getShell() {
		return this.shell;
	}
	
	void setLayout(Layout l,Object o) {
		if(o instanceof Shell) {
			((Shell) o).setLayout(l);
		}else if(o instanceof Group) {
			((Group) o).setLayout(l);
		}
		
	}
	
	private void ajouter(String name){
		FenetreV2 f = new FenetreV2("Ajouter "+name,getDisplay());

		f.getShell().setSize(750, 400);
		f.setLayout(new GridLayout(), f);
		f.AjouterScrolledComposite(f.getShell());
		f.AjouterComposite(0);
		FenetreFonctionAnnexe.AjouterEventListenerScrolled(f.getSc().get(0), f.getC().get(0),100,500);
		f.addGridLayout(2, f.getC().get(0));
		
		
			
		if(name.equals("Benevole")) {
			f.AjouterLabel(f.getC().get(0), "Nom",null);
			f.AjouterTexte(f.getC().get(0));
			f.AjouterLabel(f.getC().get(0), "Prénom",null);
			f.AjouterTexte(f.getC().get(0));
			f.AjouterLabel(f.getC().get(0), "Niveau Spéléologie",null);
			f.AjouterCombo(f.getC().get(0), new String[] { "0", "1", "2" });
			f.AjouterLabel(f.getC().get(0), "Niveau Identification",null);
			f.AjouterCombo(f.getC().get(0), new String[] { "0", "1", "2" });
			f.AjouterLabel(f.getC().get(0), "Permis",null);
			f.AjouterButtonCheck(f.getC().get(0));
			f.AjouterLabel(f.getC().get(0), "Voiture",null);
			Composite c1 = f.AjouterCompositeInCompiste(f.getC().get(0));
			f.addGridLayout(5, c1);
			final Combo c = f.AjouterCombo(c1, new String[] { "0", "1", "2","3","4","5","6" });
			final Label l1 = f.AjouterLabel(c1, "Immatriculation",null);
			final Text t1 = f.AjouterTexte(c1);
			final Label l2 = f.AjouterLabel(c1, "Modele",null);
			final Text t2 = f.AjouterTexte(c1);
			l1.setVisible(false); l2.setVisible(false);
			t1.setVisible(false); t2.setVisible(false);
			c.addSelectionListener(new SelectionAdapter()
			  {
			    @Override
			    public void widgetSelected(final SelectionEvent event)
			    {
			      if(Integer.valueOf(c.getText()) > 0) {
			    	  l1.setVisible(true); l2.setVisible(true);
					  t1.setVisible(true); t2.setVisible(true);
			      }else {
			    	  l1.setVisible(false); l2.setVisible(false);
					  t1.setVisible(false); t2.setVisible(false);
			      }
			    }
			  });
			GridLayout layout3 = new GridLayout(); 
		    layout3.numColumns = 3;
		    final Composite c4 = new Composite(f.getShell(), SWT.NONE); 
		    c4.setLayout(layout3); 
			f.AjouterButtonPush(c4, "Ajouter Contrainte", "ajouterContrainte");
			f.AjouterButtonPush(c4, "Valider", "ajouterBenev");
			f.AjouterButtonPush(c4, "Annuler", "annuler"); 
			
		}else if(name.equals("Site")){
			f.AjouterLabel(f.getC().get(0), "Nom du site",null);
			f.AjouterTexte(f.getC().get(0));
			f.AjouterLabel(f.getC().get(0), "Spéléologue Requis",null);
			f.AjouterButtonCheck(f.getC().get(0));
			f.AjouterLabel(f.getC().get(0), "Durée (00H00)",null);
			Composite c1 = f.AjouterCompositeInCompiste(f.getC().get(0));
		    f.addGridLayout(3, c1);
			
			f.AjouterCombo(c1,  new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
	        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"});	
			f.AjouterLabel(c1, "H",null);
			f.AjouterCombo(c1, new String[] { "00", "10", "20","30","40","50"});
			f.AjouterLabel(f.getC().get(0), "Coordonnées",null);
			f.AjouterTexte(f.getC().get(0));
			f.AjouterLabel(f.getC().get(0), "Materiel",null);
			f.AjouterTexte(f.getC().get(0));
			GridLayout layout3 = new GridLayout(); 
		    layout3.numColumns = 2;
		    final Composite c4 = new Composite(f.getShell(), SWT.NONE); 
		    c4.setLayout(layout3); 
			f.AjouterButtonPush(c4, "Valider", "ajouterSite");
			f.AjouterButtonPush(c4, "Annuler", "annuler");
			f.getShell().addListener(SWT.Close, new Listener() {
	        	public void handleEvent(Event e) {
	        		FenetreFonctionAnnexe.disposeSite(getC().get(2));
	        		afficherSite(getC().get(2));
		          }
			});
		}
		
		f.getShell().open();
	}
	
	void modifier(String name) {
		FenetreV2 f = new FenetreV2("Modifier "+name,getDisplay());
        f.setLayout(new GridLayout(), f);
        f.AjouterScrolledComposite(f.getShell());
        f.AjouterComposite(0);
       
        int y=0;
        if(name.equals("Benevole")) {
        	
            f.addGridLayout(9, f.getC().get(0));
            f.AjouterLabelBorder(f.getC().get(0), "Nom");
            f.AjouterLabelBorder(f.getC().get(0), "Prenom");
            f.AjouterLabelBorder(f.getC().get(0), "Niveau Spéléologie");
            f.AjouterLabelBorder(f.getC().get(0), "Niveau Identification");
            f.AjouterLabelBorder(f.getC().get(0), "Permis");
            f.AjouterLabelBorder(f.getC().get(0), "Voiture");
            f.AjouterLabelBorder(f.getC().get(0), "Place");
            f.AjouterLabelBorder(f.getC().get(0), "Immatriculation");
            f.AjouterLabelBorder(f.getC().get(0), "Modele");
            List<Object> benevs = Csv.recupListe("resources/benevoles.csv", "");
            for(int i=0;i<benevs.size();i++) {
            
            	y += 29;
            	Benevole b = (Benevole) benevs.get(i);
            	f.AjouterTexteRempli(f.getC().get(0), b.getNom());
            	f.AjouterTexteRempli(f.getC().get(0), b.getPrenom());
            	f.AjouterComboRempli(f.getC().get(0),new String[] { "0", "1", "2" } ,String.valueOf(b.getCompetenceSpeleo()));
            	f.AjouterComboRempli(f.getC().get(0), new String[] { "0", "1", "2" },String.valueOf(b.getIdentificateur()));
            	f.AjouterButtonCheck(f.getC().get(0)).setSelection(b.isPermis());
            	if(b.getVoiture() != null) {
            		f.AjouterLabelBorder(f.getC().get(0), "OUI");
                	final Combo c = f.AjouterComboRempli(f.getC().get(0), new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
        	        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"},String.valueOf(b.getVoiture().getNbPlace()));
                	final Text t1 =f.AjouterTexteRempli(f.getC().get(0), b.getVoiture().getImmatriculation());
                	final Text t2 = f.AjouterTexteRempli(f.getC().get(0), b.getVoiture().getModele());
                	c.addSelectionListener(new SelectionAdapter(){
          			    @Override
          			    public void widgetSelected(final SelectionEvent event)
          			    {
          			      if(Integer.valueOf(c.getText()) > 0) {
          					  t1.setVisible(true); t2.setVisible(true);
          			      }else {
          					  t1.setVisible(false); t2.setVisible(false);
          			      }
          			    }
          			  });
            	}else {
            		f.AjouterLabelBorder(f.getC().get(0), " NON ");
                	final Combo c = f.AjouterCombo(f.getC().get(0),  new String[] { "0", "1", "2","3","4","5","6" });
                	final Text t1 = f.AjouterTexte(f.getC().get(0));
                	final Text t2 = f.AjouterTexte(f.getC().get(0));
                	t1.setVisible(false);
                	c.addSelectionListener(new SelectionAdapter(){
          			    @Override
          			    public void widgetSelected(final SelectionEvent event)
          			    {
          			      if(Integer.valueOf(c.getText()) > 0) {
          					  t1.setVisible(true); t2.setVisible(true);
          			      }else {
          					  t1.setVisible(false); t2.setVisible(false);
          			      }
          			    }
          			  });
            	}
            	
            }
            f.AjouterButtonPush(f.getShell(), "Modifier", "modifierBenevole");
        }else {
        	f.addGridLayout(5, f.getC().get(0));
            f.AjouterLabelBorder(f.getC().get(0), "Nom");
            f.AjouterLabelBorder(f.getC().get(0), "Speleologue requis");
            f.AjouterLabelBorder(f.getC().get(0), "Durée");
            f.AjouterLabelBorder(f.getC().get(0), "Coordonnée");
            f.AjouterLabelBorder(f.getC().get(0), "Equipement requis");
            List<Object> sites = Csv.recupListe("resources/Sites.csv", "");
            for(int i=0;i<sites.size();i++) {
            	y+=40;
            	Site site = (Site) sites.get(i);
            	f.AjouterTexteRempli(f.getC().get(0), site.getNom());
            	f.AjouterButtonCheck(f.getC().get(0)).setSelection(site.isSpeleoRequis());
            	
            	GridLayout layoutDuree = new GridLayout();
   		     	layoutDuree.numColumns = 3;
   		     	final Composite c3 = new Composite(f.getC().get(0), SWT.NONE);
   		     	c3.setLayout(layoutDuree);
            	f.AjouterComboRempli(c3, new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
    	        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"}, String.valueOf(site.getDuree().getH()));
            	f.AjouterLabel(c3, "H", null);
            	f.AjouterComboRempli(c3, new String[] { "00", "10", "20","30","40","50" }, String.valueOf(site.getDuree().getM()));
            	f.AjouterTexteRempli(f.getC().get(0), site.getCoordonnees());
            	f.AjouterTexteRempli(f.getC().get(0), String.valueOf(site.getEquipementRequis()));
            }
            f.AjouterButtonPush(f.getShell(), "Modifier", "modifierSite");
        }
        
        FenetreFonctionAnnexe.AjouterEventListenerScrolled(f.getSc().get(0), f.getC().get(0),10*100,y);
        f.getShell().open();
	}
	
	void supprimer(String name) {
		FenetreV2 f = new FenetreV2("Supprimer "+name,getDisplay());
        f.setLayout(new GridLayout(), f);
        f.AjouterScrolledComposite(f.getShell());
        f.AjouterComposite(0);
       
        int y=0;
        if(name.equals("Benevole")) {
        	
            f.addGridLayout(10, f.getC().get(0));
            f.AjouterLabelBorder(f.getC().get(0), "");
            f.AjouterLabelBorder(f.getC().get(0), "Nom");
            f.AjouterLabelBorder(f.getC().get(0), "Prenom");
            f.AjouterLabelBorder(f.getC().get(0), "Permis");
            f.AjouterLabelBorder(f.getC().get(0), "Niveau Spéléologie");
            f.AjouterLabelBorder(f.getC().get(0), "Niveau Identification");
            f.AjouterLabelBorder(f.getC().get(0), "Voiture");
            f.AjouterLabelBorder(f.getC().get(0), "Place");
            f.AjouterLabelBorder(f.getC().get(0), "Immatriculation");
            f.AjouterLabelBorder(f.getC().get(0), "Modele");
            List<Object> benevs = Csv.recupListe("resources/benevoles.csv", "");
            for(int i=0;i<benevs.size();i++) {
            
            	y += 29;
            	Benevole b = (Benevole) benevs.get(i);
            	f.AjouterButtonCheck(f.getC().get(0));
            	f.AjouterLabelBorder(f.getC().get(0), b.getNom());
            	f.AjouterLabelBorder(f.getC().get(0), b.getPrenom());
            	f.AjouterLabelBorder(f.getC().get(0), String.valueOf(b.getCompetenceSpeleo()));
            	f.AjouterLabelBorder(f.getC().get(0), String.valueOf(b.getIdentificateur()));
            	f.AjouterLabelBorder(f.getC().get(0), String.valueOf(b.isPermis()));
            	if(b.getVoiture() != null) {
            		f.AjouterLabelBorder(f.getC().get(0), "OUI");
                	f.AjouterLabelBorder(f.getC().get(0), String.valueOf(b.getVoiture().getNbPlace()));
                	f.AjouterLabelBorder(f.getC().get(0), b.getVoiture().getImmatriculation());
                	f.AjouterLabelBorder(f.getC().get(0), b.getVoiture().getModele());
            	}else {
            		f.AjouterLabelBorder(f.getC().get(0), " NON ");
                	f.AjouterLabel(f.getC().get(0), " ",null);
                	f.AjouterLabel(f.getC().get(0), " ",null);
                	f.AjouterLabel(f.getC().get(0), " ",null);
            	}
            	
            }
            f.AjouterButtonPush(f.getShell(), "Supprimer", "supprimerBenevole");
        }else {
        	f.addGridLayout(6, f.getC().get(0));
            f.AjouterLabelBorder(f.getC().get(0), "");
            f.AjouterLabelBorder(f.getC().get(0), "Nom");
            f.AjouterLabelBorder(f.getC().get(0), "Spéléologue requis");
            f.AjouterLabelBorder(f.getC().get(0), "Durée");
            f.AjouterLabelBorder(f.getC().get(0), "Coordonnée");
            f.AjouterLabelBorder(f.getC().get(0), "Equipement requis");
            List<Object> sites = Csv.recupListe("resources/Sites.csv", "");
            for(int i=0;i<sites.size();i++) {
            	y+=29;
            	Site site = (Site) sites.get(i);
            	f.AjouterButtonCheck(f.getC().get(0));
            	f.AjouterLabelBorder(f.getC().get(0), site.getNom());
            	f.AjouterLabelBorder(f.getC().get(0), String.valueOf(site.isSpeleoRequis()));
            	f.AjouterLabelBorder(f.getC().get(0), site.getDuree().toString());
            	f.AjouterLabelBorder(f.getC().get(0), site.getCoordonnees());
            	f.AjouterLabelBorder(f.getC().get(0), String.valueOf(site.getEquipementRequis()));
            	
            }
            f.AjouterButtonPush(f.getShell(), "Supprimer", "supprimerSite");
            f.getShell().addListener(SWT.Close, new Listener() {
	        	public void handleEvent(Event e) {
	        		FenetreFonctionAnnexe.disposeSite(getC().get(2));
	        		afficherSite(getC().get(2));
		          }
			});
        }
        
        FenetreFonctionAnnexe.AjouterEventListenerScrolled(f.getSc().get(0), f.getC().get(0),10*100,y);
        f.getShell().open();
	}
	
	
	void prepa(String name) throws IOException {
		if(name.equals("Manuelle")) {
			FenetreV2 f = new FenetreV2("Preparation "+name,getDisplay());
		    f.addGridLayout(2, f.getShell());
			
			f.AjouterScrolledComposite(f.getShell());
			f.AjouterComposite(0,100,250);
			f.addGridLayout(1, f.getC().get(0));
			f.AjouterScrolledComposite(f.getShell());
			f.AjouterComposite(1,8,250);
			
			
			//afficherBenev
			List<Object> benevs = Csv.recupListe("resources/benevoles.csv", "");
			int y=0;
	        for(int i=0;i<benevs.size();i++) {
	        	y+=28;
	        	Benevole b = (Benevole) benevs.get(i);
	        	Label l = f.AjouterLabelBorder(f.getC().get(0), b.getNom()+" "+b.getPrenom());
	        	FontData[] fD = l.getFont().getFontData();
				fD[0].setHeight(12);
				l.setFont( new Font(display,fD[0]));
	        	FenetreFonctionAnnexe.setDrag(l);
	        }
	        FenetreFonctionAnnexe.AjouterEventListenerScrolled(f.getSc().get(0), f.getC().get(0),40,y);
	        
	        //afficher equipe
	         	//recup du nb de personne par equipe
	        Object nb = Csv.recupLigne("resources/parametre.csv", 0,"nb_personne_equipe");
	        int nb_e = (int)(nb);
	        double nb_benevoles=Csv.recupSizeList("resources/benevoles.csv");
		    double nb_equipe_temp =((double)nb_benevoles)/nb_e;
		    final int nb_equipe = (int)Math.ceil(nb_equipe_temp);
	        
	        	//ajout grid Layout
	        f.addGridLayout(nb_e+1, f.getC().get(1));
			
				//ajout des labels
			Label lab1=f.AjouterLabel(f.getC().get(1), "Equipe/Membre", null);
			FontData[] fD = lab1.getFont().getFontData();
			fD[0].setHeight(16);
			lab1.setFont( new Font(display,fD[0]));
			
			int x = 100;
			int y2 = 25;
			for(int i=1;i<nb_e+1;i++) {
				Label l = f.AjouterLabel(f.getC().get(1), "Membre " + i+"      ", null);
				FontData[] fD2 = l.getFont().getFontData();
				fD2[0].setHeight(16);
				l.setFont( new Font(display,fD2[0]));
				x+= 100;
			}
			
			for(int i=0;i<nb_equipe;i++) {
				//recup equipe en cours
				@SuppressWarnings("unchecked")
				List<String> mb_e = (List<String>) Csv.recupLigne("resources/equipes.csv", i, "");
				for(int j=0;j<nb_e+1;j++) {
					if(j==0) {
						Label l = f.AjouterLabel(f.getC().get(1), "Equipe "+(i+1), null);
						FontData[] fD3 = l.getFont().getFontData();
						fD3[0].setHeight(16);
						l.setFont( new Font(display,fD3[0]));
						
					}else {
						
						if(mb_e != null && mb_e.size() != 0 &&  mb_e.get(0).equals("Equipe "+(i+1))){
							
							if(mb_e.size() >j) {
								Benevole b = (Benevole) Csv.recupLigne("resources/benevoles.csv", Integer.valueOf(mb_e.get(j)), "");
								if(b!=null) {
									Label l = f.AjouterLabelPush(f.getC().get(1), b.getNom()+" "+b.getPrenom(), null);
									FontData[] fD4 = l.getFont().getFontData();
									fD4[0].setHeight(12);
									l.setFont( new Font(display,fD4[0]));
									FenetreFonctionAnnexe.setDrop(l);
								}
								
							}else {
								
								Label l = f.AjouterLabelPush(f.getC().get(1), ".....", null);
								FontData[] fD4 = l.getFont().getFontData();
								fD4[0].setHeight(12);
								l.setFont( new Font(display,fD4[0]));
								FenetreFonctionAnnexe.setDrop(l);
							}
						}else {
							Label l = f.AjouterLabelPush(f.getC().get(1), ".....", null);
							FontData[] fD4 = l.getFont().getFontData();
							fD4[0].setHeight(12);
							l.setFont( new Font(display,fD4[0]));
							FenetreFonctionAnnexe.setDrop(l);
						}

					}
					
				}
				y2+= 25;
			}
			 FenetreFonctionAnnexe.AjouterEventListenerScrolled(f.getSc().get(1), f.getC().get(1),x,y2);
			AjouterButtonPush(f.getShell(), "Valider", "envoyerEquipe");
			f.getShell().open();
			f.getShell().addListener(SWT.Close, new Listener() {
	        	public void handleEvent(Event e) {
	        		afficherEquipes(getC().get(0));
		          }
			});
			
		}else {
			Csv.deleteAllInFile("resources/equipes.csv", new File("resources/equipes.csv"));
			int nb_pers_equipe=(int)(Csv.recupLigne("resources/parametre.csv", 0, "nb_personne_equipe"));
			Organisation org=new Organisation();
			org.createEquipeAutoSmartV1(nb_pers_equipe);
			Csv.writeInFile("resources/equipes.csv", org);
			afficherEquipes(getC().get(0));
		}
		
	}
	
	@SuppressWarnings("unchecked")
	void paramHorraire() throws IOException {
		FenetreV2 f = new FenetreV2("Parametres Horaires",getDisplay());
		f.setLayout(new GridLayout(), f);
		f.AjouterScrolledComposite(f.getShell());
		f.AjouterComposite(0,500,500);
		f.addGridLayout(2, f.getC().get(0));
		f.AjouterLabel(f.getC().get(0), "Echelle de temps",null);
		List<String> param_ph = (List<String>) Csv.recupLigne("resources/parametre.csv", 1, "");
		f.AjouterCombo(f.getC().get(0), new String[] { "00H15","00H30","01H00"}).setText(param_ph.get(0));
		for(String j : new String[] { "Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"}) {
			List<String> param_j = (List<String>) Csv.recupLigne("resources/parametre.csv", -1, j);
			System.out.println(j+" "+param_j);
			f.AjouterLabel(f.getC().get(0), j,null);
			Composite c1 = f.AjouterCompositeInCompiste(f.getC().get(0));
		    f.addGridLayout(4, c1);
		    f.AjouterLabel(c1, "Heure début",null);
		    f.AjouterCombo(c1,  new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
	        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"}).setText((param_j.get(0).split("H"))[0]);
			f.AjouterLabel(c1, "H",null);
			f.AjouterCombo(c1, new String[] { "00", "10", "20","30","40","50"}).setText((param_j.get(0).split("H"))[1]);
			
			 f.AjouterLabel(c1, "Heure Fin",null);
		    f.AjouterCombo(c1,  new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
    		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"}).setText((param_j.get(1).split("H"))[0]);	
			f.AjouterLabel(c1, "H",null);
			f.AjouterCombo(c1, new String[] { "00", "10", "20","30","40","50"}).setText((param_j.get(1).split("H"))[1]);
		}
		f.AjouterButtonPush(f.getShell(), "Valider", "changerParamHorraire");
		f.getShell().addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
        		FenetreFonctionAnnexe.disposePH(getShell());
            	afficherPlageHorraire(getC().get(3),null,null);
	          }
		});
		f.getShell().open();
	}
	
	void paramEquipe(){
		FenetreV2 f = new FenetreV2("Parametres Equipe",getDisplay());
		f.setLayout(new GridLayout(), f);
		f.AjouterScrolledComposite(f.getShell());
		f.AjouterComposite(0,100,150);
		f.DisposeElement(f);
		f.addGridLayout(4, f.getC().get(0));
		f.AjouterLabel(f.getC().get(0), "Nombre de personne par equipe",null);
		f.AjouterTexte(f.getC().get(0)).setText(String.valueOf((int)Csv.recupLigne("resources/parametre.csv", 0, "")));
		f.AjouterButtonPush(f.getC().get(0), "-", "MoinsUn");
		f.AjouterButtonPush(f.getC().get(0), "+", "plusUn");
		f.AjouterButtonPush(f.getShell(), "Valider", "changerParamEquipe");
		f.getShell().open();
		f.getShell().addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
        		Control[] cont=getC().get(0).getChildren();
	    		for(int i=0;i<cont.length;i++) {
	    		
	    			cont[i].dispose();
	    		}
				try {
					prepa("Automatique");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	          }
		});
	}
	
	void fonctionalite() {
		FenetreV2 f = new FenetreV2("Fonctionalités",getDisplay());
		f.DisposeElement(f);
	}
	
	
	
	void createur() {
		FenetreV2 f = new FenetreV2("Créateurs",getDisplay());
		f.DisposeElement(f);
	}
	
	void DisposeElement(Object o) {
		if(o instanceof Shell) {
			shell.open();

		    while (!shell.isDisposed())
		      if (!getDisplay().readAndDispatch())
		        getDisplay().sleep();

		     
		}
	}
	
	public void afficherSite(Composite comp) {
		Path file = Paths.get("resources/Sites.csv");
		List<String> list = null;
		String separator = ",";
		
		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if(list == null) {
			 System.out.println("Il n'y a rien dans le fichier");
		     return;
		 }
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return;
	     }
		 int[] valeurs=new int[4];
		 valeurs[0]=200;
		 valeurs[1]=25;
		 valeurs[2]=0;
		 valeurs[3]=0;
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 Label l = AjouterLabel(comp, split[0],valeurs);
			 valeurs[3]+=25;
			 FenetreFonctionAnnexe.setDrag(l);
				
		 }
		 FenetreFonctionAnnexe.AjouterEventListenerScrolled(getSc().get(2), comp, 50, valeurs[3]+25);
		 
	}
	
	@SuppressWarnings("unchecked")
	public void afficherPlageHorraire(Composite composite,Site si,Equipe eq) {
		int taillex;
		int tailley;
		int[] valeurs=new int[4];
		 valeurs[0]=100;
		 valeurs[1]=35;
		 valeurs[2]=50;
		 valeurs[3]=50;
		 
		Label l = AjouterLabel(composite,"Heure",valeurs);
		FontData[] fD = l.getFont().getFontData();
		fD[0].setHeight(16);
		l.setFont( new Font(display,fD[0]));
		valeurs[2]+=110;
		List<Object> infos=Csv.recupListe("resources/edt.csv", current_date.toString());
		List<Object> liste=Csv.recupListe("resources/equipes.csv", "");
		HashMap<Integer, Equipe> liste_equipe=new HashMap<Integer,Equipe>();
		Organisation org=new Organisation();
		for(int i=0;i<liste.size();i++) {
			List<String> split=(List<String>)(liste.get(i));
			String[] split_name_equipe=split.get(0).split(" ");
			
			
			Equipe e=new Equipe(Integer.valueOf(split_name_equipe[1]));
			for(int j=1;j<split.size();j++) {
				Benevole b=(Benevole)Csv.recupLigne("resources/benevoles.csv", Integer.valueOf(split.get(j)), null);
				e.ajouterBenevole(b);
			}
			for(int k=0;k<infos.size();k++) {
				String[] split_first = ((String)infos.get(k)).split(",");
				if(Integer.valueOf((split_first[0].split(" "))[1]) == i+1) {
					Site si2=(Site)Csv.recupLigne("resources/Sites.csv", -1, split_first[1]);
					e.majContraintesIsCheck(si2);
				}
			}
			e.completeInformationsEquipe();
			if(si!= null && eq!=null && eq.getId_equipe()==e.getId_equipe()) {
				e.majContraintesIsCheck(si);
			}
			
			liste_equipe.put(i+1, e);
			
		}
		org.setListe_equipe(liste_equipe);
		for(int i=0;i<liste_equipe.size();i++) {
			Label l2=AjouterLabel(composite,"Equipe "+(i+1),valeurs);
			FontData[] fD1 = l2.getFont().getFontData();
			fD1[0].setHeight(16);
			l2.setFont( new Font(display,fD1[0]));
			if(!liste_equipe.get(i+1).isContraintes_is_check()) {
				l2.setBackground(new Color(display, 255,0,0));
				l2.setForeground(new Color(display, 0,0,0));
				
			}
			
			
			valeurs[2] += 110;
		}
		
		taillex = valeurs[2];
		
		String nameday = current_date.nameDay();

		Heure debut=new Heure();
		System.out.println(nameday);
	
		List<String> param = (List<String>) Csv.recupLigne("resources/parametre.csv", -1, nameday);
		debut.setH(Integer.valueOf((param.get(0).split("H"))[0]));
		debut.setM(Integer.valueOf((param.get(0).split("H"))[1]));
	
		Heure fin = new Heure();
		fin.setH(Integer.valueOf((param.get(1).split("H"))[0]));
		fin.setM(Integer.valueOf((param.get(1).split("H"))[1]));
		
		List<String> param_ph = (List<String>) Csv.recupLigne("resources/parametre.csv", 1, "");
		Heure plage_horraire = new Heure();
		plage_horraire.setH(Integer.valueOf((param_ph.get(0).split("H"))[0]));
		plage_horraire.setM(Integer.valueOf((param_ph.get(0).split("H"))[1]));
		
		valeurs[2]=50;
		valeurs[3]=120;
		
		
		int cpt = 0;
		while(debut.getH() != fin.getH()+plage_horraire.getH() || debut.getM() != fin.getM()+plage_horraire.getM()) {
			
			Label l2=AjouterLabel(composite,""+debut,valeurs);
			FontData[] fD3 = l2.getFont().getFontData();
			fD3[0].setHeight(16);
			
			l2.setFont( new Font(display,fD3[0]));
			
			int heure = debut.getH()+plage_horraire.getH();
			int minute = debut.getM()+plage_horraire.getM();
			
			while(minute >= 60) {
				heure += 1;
				minute %= 60;
			}
			
			debut.setH(heure);
			debut.setM(minute);
			
			for(int j=0;j<liste_equipe.size();j++) {
				valeurs[2] += 110;
				Label l3 = null;
				
				boolean ok = false;
			    for(int k=0;k<infos.size();k++) {
					String[] split = ((String)infos.get(k)).split(",");
					if(Integer.valueOf((split[0].split(" "))[1]) == j+1) {
						int conversMin = debut.getH()*60 + debut.getM();
						int conversMin2 = (Integer.valueOf((split[2].split("H"))[0]) * 60 + Integer.valueOf((split[2].split("H"))[1]));
						int conversMin3 = (Integer.valueOf((split[2].split("H"))[0])+Integer.valueOf((split[3].split("H"))[0]))*60 
								          + (Integer.valueOf((split[2].split("H"))[1])+Integer.valueOf((split[3].split("H"))[1]));
						if((conversMin) > (conversMin2)
								&& (conversMin) <= (conversMin3)
								&& !ok) {
							l3=AjouterLabel(composite,split[1],valeurs);
							Object[] data = {"Equipe "+(j+1),cpt,plage_horraire,current_date};	
							l3.setData(data);
							FontData[] fD2 = l3.getFont().getFontData();
							fD2[0].setHeight(16);
							l3.setFont( new Font(display,fD2[0]));
							if(!liste_equipe.get(j+1).isContraintes_is_check()) {
								l3.setBackground(new Color(display, 255,0,0));
								l3.setForeground(new Color(display, 0,0,0));
								
							}
							l3.addMouseListener(new MouseAdapter() {
								   @Override
								   public void mouseUp(MouseEvent event) {
								      super.mouseUp(event);

								      if (event.getSource() instanceof Label) {
								         Label label = (Label)event.getSource();
								         
								         FenetreFonctionAnnexe.supprimerSiteEdt(label);
								         FenetreFonctionAnnexe.disposePH(getShell());
								         afficherPlageHorraire(getC().get(3),null,null);
								      }
								   }
								});
							ok = true;
							continue;
						}
						
					}
				}
				if(infos.size()==0 || !ok) {
					l3=AjouterLabel(composite,". . .",valeurs);
					FontData[] fD2 = l3.getFont().getFontData();
					fD2[0].setHeight(16);
					l3.setFont( new Font(display,fD2[0]));
				}
				FenetreFonctionAnnexe.setDropEDT(l3,j,plage_horraire,cpt,current_date,this);
			}
			valeurs[2] =50;
			valeurs[3] += 70;
			cpt++;
		}
		tailley = valeurs[3]; 		
		FenetreFonctionAnnexe.AjouterEventListenerScrolled(getSc().get(3), composite, taillex, tailley);
	}
	
	
	public void afficherEquipes(Composite composite) {
		
	    Object nb = Csv.recupLigne("resources/parametre.csv", 0,"nb_personne_equipe");
	    int nb_e = (int)(nb);
        double nb_benevoles=Csv.recupSizeList("resources/benevoles.csv");
	    double nb_equipe_temp =((double)nb_benevoles)/nb_e;
	    final int nb_equipe = (int)Math.ceil(nb_equipe_temp);

		
		 int[] valeurs=new int[4];
		 valeurs[0]=200;
		 valeurs[1]=33*nb_e;
		 valeurs[2]=0;
		 valeurs[3]=0;

		for(int j=0;j<nb_equipe;j++) {
				
			@SuppressWarnings("unchecked")
			List<String> mb_e = (List<String>) Csv.recupLigne("resources/equipes.csv", j, "");
			List<String> mb = new ArrayList<String>();
			
					
			if(mb_e != null && mb_e.size() !=0){
				for(int k=0;k<mb_e.size();k++) {
					if(k==0) {
						mb.add(mb_e.get(k));
					}else {
						Benevole b=(Benevole) Csv.recupLigne("resources/benevoles.csv", Integer.valueOf(mb_e.get(k)), "");
						if(b!=null) {
							mb.add(b.getNom()+" "+b.getPrenom());	
						}
						
					}
					
					
				}
				 AjouterExpandBar(composite, mb,valeurs);
				 valeurs[3]+=valeurs[1]+5;
			}

		}
				
		FenetreFonctionAnnexe.AjouterEventListenerScrolled(getSc().get(0), composite,valeurs[0]+5,valeurs[3]);
	}

	public void afficherDate(Composite composite) {
		composite.setLayout(new FormLayout());
		FormData data1= new FormData();
		Button but_avant=AjouterButtonPush(composite, "<--", "jourAvant");
		
		data1.left = new FormAttachment(2,1);
		data1.right = new FormAttachment(5,0);

		but_avant.setLayoutData(data1);
		
		LocalDate localDate = LocalDate.now();
        String dateString=DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
        
        int annee=Integer.parseInt(dateString.substring(0, 4));
        int mois=Integer.parseInt(dateString.substring(5, 7));
        int jour=Integer.parseInt(dateString.substring(8, 10));
        current_date.setDate(jour, mois, annee);
		Label lab_date=AjouterLabel(composite, current_date.toString(), null);
		FontData[] fD = lab_date.getFont().getFontData();
		fD[0].setHeight(20);
		lab_date.setFont( new Font(display,fD[0]));
		
		FormData data2= new FormData();
		data2.left = new FormAttachment(25,1);
		data2.right = new FormAttachment(32,0);
		lab_date.setLayoutData(data2);
		
		Button but_apres=AjouterButtonPush(composite, "-->", "jourApres");
		FormData data3= new FormData();
		data3.left = new FormAttachment(49,1);
		data3.right = new FormAttachment(52,0);
		but_apres.setLayoutData(data3);
		
		
	}

	
	protected void searchFonction(String name,String m) throws IOException {
		
		switch(name) {
			case "Ajouter":
				ajouter(m);
				break;
			case "Modifier":
				modifier(m);
				break;
			case "Supprimer":
				supprimer(m);
				break;
			case "Automatique":
				Control[] cont=getC().get(0).getChildren();
	    		for(int i=0;i<cont.length;i++) {
	    		
	    			cont[i].dispose();
	    		}
				prepa(name); //appeler juste la fonction de creation d'equipe auto
				
				break;
			case "Manuelle":
				Control[] cont2=getC().get(0).getChildren();
	    		for(int i=0;i<cont2.length;i++) {
	    			
	    			cont2[i].dispose();
	    		}
	    		prepa(name);
	    		
	    		
				break;
			case "Horaire":
				paramHorraire();
				break;
			case "Equipe":
				paramEquipe();
				break;
			case "Fonctionalités":
				fonctionalite();
				break;
			case "Quitter\tCtrl+Q":
				System.exit(1);
				break;
			case "Créateurs":
				createur();
				break;
		}
	}

	protected void searchFonction(String name,Shell s){
        switch(name) {
        case "ajouterBenev":
            FenetreFonctionAnnexe.ajouterBenevole(s);
            s.close();
            break;
        case "ajouterContrainte":
            FenetreFonctionAnnexe.ajouterContrainte(s);
            break;
        case "ajouterSite":
        	FenetreFonctionAnnexe.ajouterSite(s);
        	s.close();
        	break;
        case "supprimerBenevole":
        	FenetreFonctionAnnexe.supprimerBenevole(s);
        	s.close();
        	break;
        case "supprimerSite":
        	FenetreFonctionAnnexe.supprimerSite(s);
        	s.close();
        	break;
        case "modifierBenevole":
        	FenetreFonctionAnnexe.modifierBenevole(s);
        	s.close();
        	break;
        case "modifierSite":
        	FenetreFonctionAnnexe.modifierSite(s);
        	s.close();
        	break;
        case "envoyerEquipe":
        	FenetreFonctionAnnexe.envoyerEquipe(s);
        	s.close();
        	afficherEquipes(getC().get(0));
        	break;
        case "changerParamEquipe":
        	FenetreFonctionAnnexe.modifParamEquipe(s);
        	s.close();
        	break;
        case "changerParamHorraire":
        	FenetreFonctionAnnexe.modifParamHorraire(s);
        	s.close();
        	break;
        case "jourApres":
        	current_date  = FenetreFonctionAnnexe.actualiserHorraire(s,current_date,1);
        	FenetreFonctionAnnexe.disposePH(s);
        	afficherPlageHorraire(getC().get(3),null,null);
        	break;
        case "jourAvant":
        	current_date  = FenetreFonctionAnnexe.actualiserHorraire(s,current_date,-1);
        	FenetreFonctionAnnexe.disposePH(s);
        	afficherPlageHorraire(getC().get(3),null,null);
        	break;
        case "annuler":
        	s.close();
        	break; 
        case "MoinsUn":
        	FenetreFonctionAnnexe.addOrSubsract(-1,s);
        	break;
        case "plusUn":
        	FenetreFonctionAnnexe.addOrSubsract(1,s);
        	break;  
        }	
        
        
    }
	
	public Display getDisplay() {
		return display;
	}

	public void setDisplay(Display display) {
		this.display = display;
	}

	public ArrayList<Composite> getC() {
		return c;
	}

	public void setC(ArrayList<Composite> c) {
		this.c = c;
	}

	public void setFormDataLayout(FormData data1, Composite composite) {
		composite.setLayoutData(data1);
		
	}

	public ArrayList<ScrolledComposite> getSc() {
		return sc;
	}

	public void setSc(ArrayList<ScrolledComposite> sc) {
		this.sc = sc;
	}

	
	

	
	
	
}
