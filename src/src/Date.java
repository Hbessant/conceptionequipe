package src;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * <b> Class Date qui permet de représenter une date. </b>
 * <p> Une date est caractérisé par:
 * <ul>
 * <li> Un entier représentant l'année.</li>
 * <li> Un entier représentant le mois.</li> 
 * <li> Un entier représentant le jour.</li> 
 * </ul></p>
 * <p> Entre autre cette class permet de gérer et de comparer les dates
 * au format JJ/MM/AAAA </p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.1
 */
public class Date {
	
	/**
	 * L'annee de la date
	 * 
	 * @since 1.0
	 */
	private int annee;
	
	/**
	 * Le mois de la date
	 * 
	 * @since 1.0
	 */
	private int mois;
	
	/**
	 * Le jour de la date
	 * 
	 * @since 1.0
	 */
	private int jour;
	
	private Heure debut_horaire;
	
	private Heure fin_horaire;
	/**
	 * Constructeur par defaut. Initialisation avec des valeurs faussé (-1,-1,-1)
	 * 
	 * @since 1.1
	 */
	public Date() {
		this.annee=-1;
		this.mois=-1;
		this.jour=-1;
		this.debut_horaire=new Heure(6,0);
		this.fin_horaire=new Heure(15,0);
	}
	
	/**
	 * Constructeur où l'on choisit les valeurs à initialiser en parametre.
	 * 
	 * @param a l'année voulu.
	 * @param m le mois voulu.
	 * @param j le jour voulu.
	 * @since 1.1
	 */
	public Date(int j, int m, int a) {
		this.annee=a;
		this.mois=m;
		this.jour=j;
		this.debut_horaire=new Heure(6,0);
		this.fin_horaire=new Heure(15,0);
	}
	
	public Date(int j, int m, int a,Heure debut,Heure fin) {
		this.annee=a;
		this.mois=m;
		this.jour=j;
		this.debut_horaire=debut;
		this.fin_horaire=fin;
	}
	
	/**
	 * Retourne l'annee.
	 * 
	 * @return un entier représentant l'année.
	 * @since 1.0
	 */
	public int getAnnee() {
		return annee;
	}
	
	/**
	 * Met à jour l'entier représentant l'année.
	 * 
	 * @param annee l'année de la date.
	 * @since 1.0
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	/**
	 * Retourne le mois.
	 * 
	 * @return un entier représentant le mois.
	 * @since 1.0
	 */
	public int getMois() {
		return mois;
	}
	
	/**
	 * Met à jour l'entier représentant le mois.
	 * 
	 * @param mois le mois de la date.
	 * @since 1.0
	 */
	public void setMois(int mois) {
		this.mois = mois;
	}
	
	/**
	 * Retourne le jour.
	 * 
	 * @return un entier représentant le jour.
	 * @since 1.0
	 */
	public int getJour() {
		return jour;
	}
	
	/**
	 * Met à jour l'entier représentant le jour.
	 * 
	 * @param jour le jour de la date.
	 * @since 1.0
	 */
	public void setJour(int jour) {
		this.jour = jour;
	}
	
	public Heure getDebut_horaire() {
		return debut_horaire;
	}

	public void setDebut_horaire(Heure debut_horaire) {
		this.debut_horaire = debut_horaire;
	}

	public Heure getFin_horaire() {
		return fin_horaire;
	}

	public void setFin_horaire(Heure fin_horaire) {
		this.fin_horaire = fin_horaire;
	}
	
	public void setDate(int j, int m,int a) {
		this.jour=j;
		this.mois=m;
		this.annee=a;
	}

	/**
	 * Fonction qui permet de donner un affichage sous le format: JJ/MM/AAAA.
	 * 
	 * @return la chaine de caractere au bon format.
	 * @since 1.1
	 */
	public String toString() {
		String aAfficher="";
		if(this.jour<10&&this.jour>=0) {
			aAfficher+="0"+this.jour;
		}else {
			aAfficher+=this.jour;
		}
		if(this.mois<10&&this.mois>=0) {
			aAfficher+="/0"+this.mois;
		}else {
			aAfficher+="/"+this.mois;
		}
		return aAfficher+"/"+this.annee;
	}
	
	/**
	 * Fonction qui permet de savoir si deux dates sont égales.
	 * 
	 * @param other la date à comparer.
	 * @return true elles sont bien égales et false sinon.
	 * @since 1.1
	 */
	public boolean equals(Date other) {
		return ((this.annee==other.getAnnee())&&(this.mois==other.getMois())&&(this.jour==other.getJour()));
	}
	
	/**
	 * Fonction qui permet de savoir si une date est avant une autre ou non.
	 * 
	 * @param other la date à comparer.
	 * @return true si elle est bien avant l'autre et false sinon ou si elles sont égales.
	 * @since 1.1
	 */
	public boolean before(Date other) {
		if(this.annee<other.getAnnee()) {
			return true;
		}
		if(this.annee==other.getAnnee()) {
			if(this.mois<other.getMois()) {
				return true;
			}
			if(this.mois==other.getMois()) {
				if(this.jour<other.getJour()) {
					return true;
				}
				return false;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	/**
	 * Fonction qui permet de savoir si une date est valide ou non.<br>
	 * Une date valide est de la forme JJ/MM/AAAA avec J entre 1 et 31 (suivant le mois) et M entre 1 et 12.<br>
	 * Ici le on ne prend pas en compte les années bissextiles.
	 * 
	 * @return true si la date est valide et false sinon.
	 */
	public boolean isValid() {
		switch(this.mois) {
		case 2 :
			if(anneeBissextile(this.annee)) {
				if((this.jour>=1)&&(this.jour<=29)) {
					return true;
				}else {
					return false;
				}
			}else {
				if((this.jour>=1)&&(this.jour<=28)) {
					return true;
				}else {
					return false;
				}
			}
		case 1 :
		case 3 :
		case 5 :
		case 7 :
		case 8 :
		case 10 :
		case 12 :
			if((this.jour>=1)&&(this.jour<=31)) {
				return true;
			}else {
				return false;
			}
		case 4 :
		case 6 :
		case 9 :
		case 11 :
			if((this.jour>=1)&&(this.jour<=30)) {
				return true;
			}else {
				return false;
			}
		default :
			return false; 
		}
	}
	
	/**
	 * Fonction qui permet de savoir si un mois se finnie par 31/30/29/28<br>
	 * 
	 * @return le nombre de jours dans le mois
	 */
	public int nbJoursMois() {
		switch(this.mois) {
		case 2 :
			if(anneeBissextile(this.annee)) {
				if(this.isValid()) {
					return 29;
				}else {
					return -1;
				}
			}else {
				if(this.isValid()) {
					return 28;
				}else {
					return -1;
				}
			}
		case 1 :
		case 3 :
		case 5 :
		case 7 :
		case 8 :
		case 10 :
		case 12 :
			if(this.isValid()) {
				return 31;
			}else {
				return -1;
			}
		case 4 :
		case 6 :
		case 9 :
		case 11 :
			if(this.isValid()) {
				return 30;
			}else {
				return -1;
			}
		default :
			return -1; 
		}
	}
	
	/**
	 * Fonction qui permet de savoir si une année passé en parametre est bissextile ou non
	 * 
	 * @param a l'année a regarder
	 * @return true si l'année est bien bissextile et false sinon
	 * @since 1.1
	 */
	public boolean anneeBissextile(int a) {
		return ((a%4==0)&&(a%100!=0)||(a%400==0));
	}
	
	
	/**
	 * Fonction qui permet d'ajouter un nombre de jours a la date
	 * 
	 * @param j nombre de jours � ajouter
	 * @return 0 si reussie
	 */
	public int addJour(int j) {
		int jourFindeMois=0;
		
		if(this.nbJoursMois() == -1) {
			return -1;
		}
		if(this.getJour()+j > this.nbJoursMois()) {
			jourFindeMois = this.nbJoursMois() - this.getJour();
			this.setJour(01);
			this.addMois(1);
			this.addJour(j-(jourFindeMois+1));
		}else{
			if(this.getJour()+j <=0) {
				this.addMois(-1);
				jourFindeMois=this.nbJoursMois();  //PROBLEME JOUR EN ARRIERE
				this.setJour(this.nbJoursMois());
				
			}else {
				this.setJour(this.getJour()+j);
			}
		}
		return 0;
	}
	
	/**
	 * Fonction qui permet d'ajouter un nombre de mois a la date
	 * 
	 * @param m nombre de mois � ajouter
	 * @return 0 si reussie
	 * @return -1 si date invalide
	 */
	public int addMois(int m) {
		if(!this.isValid()) {
			return -1;
		}
		
		int newMois;
		if((this.getMois())+m > 12){
			newMois = (this.getMois()+m)%12;
			if(newMois == 0) {
				newMois = 12;
			}
			this.setMois(newMois);
			if((int)(m/12) == 0) {
				this.addAnnee((int)(m/12)+1);
			}else {
				this.addAnnee((int)(m/12));
			}
			
		}else {
			if(this.getMois()+m <=0) {
				newMois = (this.getMois()+m)%12;
				if(newMois == 0) {
					newMois = 12;
				}
				this.setMois(newMois);
				if((int)(m/12) == 0) {
					this.addAnnee((int)(m/12)-1);
				}else {
					this.addAnnee((int)(m/12));
				}
				
			}else {
				this.setMois((this.getMois())+m);
			}
			
		}
		return 0;
	}
	
	/**
	 * Fonction qui permet d'ajouter un nombre d'ann�e a la date
	 * 
	 * @param a nombre d'ann�e � ajouter
	 * @return 0 si reussie
	 */
	public int addAnnee(int a) {
		if(!this.isValid()) {
			return -1;
		}
		this.setAnnee(this.getAnnee()+a);
		return 0;
	}
	

	public String nameDay() {
		java.util.Date d = (new GregorianCalendar(getAnnee(), getMois()-1, getJour())).getTime();
		SimpleDateFormat formater = new SimpleDateFormat("EEEE",new Locale("FR","fr"));
		System.out.println(formater.format(d));
		switch (formater.format(d)) {
			case "lundi":
				return "Lundi";
				
			case "mardi":
				return "Mardi";	
				
			case "mercredi":
				return "Mercredi";
				
			case "jeudi":
				return "Jeudi";
				
			case "vendredi":
				return "Vendredi";
				
			case "samedi":
				return "Samedi";
				
			case "dimanche":
				return "Dimanche";
				
		
		}
		return "Lundi";
	}
	
//	/**
//	 * Fonction qui permet d'enlever un nombre de jours a la date
//	 * 
//	 * @param j nombre de jours a enlever
//	 * @return 0 si reussie
//	 */
//	public int substractJour(int j) {
//		int jourFindeMois=0;
//		
//		if(this.nbJoursMois() == -1) {
//			return -1;
//		}
//		if(this.getJour()+j > this.nbJoursMois()) {
//			jourFindeMois = this.nbJoursMois() - this.getJour();
//			this.setJour(01);
//			this.addMois(1);
//			this.addJour(j-(jourFindeMois+1));
//		}else {
//			this.setJour(this.getJour()+j);
//		}
//		return 0;
//	}
//	
//	/**
//	 * Fonction qui permet d'enlever un nombre de mois a la date
//	 * 
//	 * @param m nombre de mois a enlever
//	 * @return 0 si reussie
//	 * @return -1 si date invalide
//	 */
//	public int substractMois(int m) {
//		if(!this.isValid()) {
//			return -1;
//		}
//		
//		int newMois;
//		if((this.getMois())+m > 12){
//			newMois = (this.getMois()-m)%12;
//			if(newMois == 0) {
//				newMois = 12;
//			}
//			this.setMois(newMois);
//			if((int)(m/12) == 0) {
//				this.addAnnee((int)(m/12)+1);
//			}else {
//				this.addAnnee((int)(m/12));
//			}
//			
//		}else {
//			this.setMois((this.getMois())+m);
//		}
//		return 0;
//	}
//	
//	/**
//	 * Fonction qui permet d'enlever un nombre d'annee a la date
//	 * 
//	 * @param a nombre d'annee a enlever 
//	 * @return 0 si reussie
//	 */
//	public int substractAnnee(int a) {
//		if(!this.isValid()) {
//			return -1;
//		}
//		this.setAnnee(this.getAnnee()-a);
//		return 0;
//	}
	
	
}
