package src;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class Fenetre {
	
	private static Label[] lab = {};
	private static Group[] groups= new Group[4];
	private static Date current_date=new Date();
	private static Organisation org=new Organisation();
	private static Shell shell;
	private static Display display;
	
	Fenetre() {
		display = new Display();
	    shell = new Shell(display, SWT.CLOSE | SWT.MAX  | SWT.RESIZE);
	    Group group = new Group(shell, SWT.BORDER | SWT.FLAT);
	    groups[0]=group;
	    shell.setSize(display.getPrimaryMonitor().getClientArea().width, display.getPrimaryMonitor().getClientArea().height);
	    shell.setText("Conception d'equipe");

//	    FormLayout fl = new FormLayout();
//	    shell.setLayout(fl);
	    
	    createMenu(display, group);
	    createZone(display, group);

	    shell.open();

	    while (!shell.isDisposed())
	      if (!display.readAndDispatch())
	        display.sleep();

	    display.dispose();
	}
	
	public static void createMenu(Display display, final Group g) {
		Menu menu = new Menu(shell, SWT.BAR);
		
		//ajout de tous les option du menu
		MenuItem benev = new MenuItem(menu, SWT.CASCADE);
		benev.setText("Benevoles");
		
		MenuItem site = new MenuItem(menu, SWT.CASCADE);
		site.setText("Sites");
		
		MenuItem prepa = new MenuItem(menu, SWT.CASCADE);
		prepa.setText("Preparation");
		
	
		MenuItem donnee = new MenuItem(menu, SWT.CASCADE);
		donnee.setText("Donnees");
		
		MenuItem aide = new MenuItem(menu, SWT.CASCADE);
		aide.setText("Aides");
		
		//ajout des option des cascade
		Menu menuBenev = new Menu(shell, SWT.DROP_DOWN);
		MenuItem ajouterBenev = new MenuItem(menuBenev, SWT.PUSH);
		ajouterBenev.setText("&Ajouter\tCtrl+A");
		MenuItem ModifBenev = new MenuItem(menuBenev, SWT.PUSH);
		ModifBenev.setText("&Modifier\t");
		MenuItem supprBenev = new MenuItem(menuBenev, SWT.PUSH);
		supprBenev.setText("&Supprimer\tCtrl+D");
		ajouterBenev.setAccelerator(SWT.CTRL+'A');
		supprBenev.setAccelerator(SWT.CTRL+'D');
		
		//ajout des option des cascade
		Menu menuPrepa = new Menu(shell, SWT.DROP_DOWN);
		MenuItem prepaAuto = new MenuItem(menuPrepa, SWT.PUSH);
		prepaAuto.setText("&Automatique\t");
		MenuItem prepaManuel = new MenuItem(menuPrepa, SWT.PUSH);
		prepaManuel.setText("&Manuel\t");
		
		//ajout des option des cascade
		Menu menuSite = new Menu(shell, SWT.DROP_DOWN);
		MenuItem AddSite = new MenuItem(menuSite, SWT.PUSH);
		AddSite.setText("&Ajouter\t");
		MenuItem ModifSite = new MenuItem(menuSite, SWT.PUSH);
		ModifSite.setText("&Modifier\t");
		MenuItem DelSite = new MenuItem(menuSite, SWT.PUSH);
		DelSite.setText("&Supprimer\t");
		
		Menu menuAide = new Menu(shell, SWT.DROP_DOWN);
		MenuItem quitterFen = new MenuItem(menuAide, SWT.PUSH);
		quitterFen.setText("&Quitter\tCtrl+Q");
		quitterFen.setAccelerator(SWT.CTRL+'Q');
		
		
		ajouterBenev.addSelectionListener(new SelectionListener() {
	      public void widgetSelected(SelectionEvent e) {
                ajouterBenevole();
	      }
	      public void widgetDefaultSelected(SelectionEvent e) {
	      }
		});
		
		ModifBenev.addSelectionListener(new SelectionListener() {
		      public void widgetSelected(SelectionEvent e) {
	                modifierBenevole();
		      }
		      public void widgetDefaultSelected(SelectionEvent e) {
		      }
			});
		
		supprBenev.addSelectionListener(new SelectionListener() {
	      public void widgetSelected(SelectionEvent e) {
                Supprimerbenevole();
	      }
	      public void widgetDefaultSelected(SelectionEvent e) {
	      }
		});
		
		
		prepaAuto.addSelectionListener(new SelectionListener() {
		      public void widgetSelected(SelectionEvent e) {
	                prepaAuto();
		      }
		      public void widgetDefaultSelected(SelectionEvent e) {
		      }
			});
		
		prepaManuel.addSelectionListener(new SelectionListener() {
		      public void widgetSelected(SelectionEvent e) {
		    	  demanderNbJoueur();
	         
		      }
		      public void widgetDefaultSelected(SelectionEvent e) {
		      }
			});
		
		quitterFen.addSelectionListener(new SelectionListener() {
		      public void widgetSelected(SelectionEvent e) {
	                System.exit(1);
		      }
		      public void widgetDefaultSelected(SelectionEvent e) {
		      }
			});
		AddSite.addSelectionListener(new SelectionListener() {
			
			public void widgetSelected(SelectionEvent e) {
				AjouterSite();
				
			}
			
			public void widgetDefaultSelected(SelectionEvent e) {
				
				
			}
			
		});
		ModifSite.addSelectionListener(new SelectionListener() {
					
					public void widgetSelected(SelectionEvent e) {
						modifierSite();
						
					}
					
					public void widgetDefaultSelected(SelectionEvent e) {
						
						
					}
					
				});
		DelSite.addSelectionListener(new SelectionListener() {
					
					public void widgetSelected(SelectionEvent e) {
						SupprimerSite();
						
					}
					
					public void widgetDefaultSelected(SelectionEvent e) {
						
						
					}
					
				});
		
		benev.setMenu(menuBenev);
		prepa.setMenu(menuPrepa);
		aide.setMenu(menuAide);
		site.setMenu(menuSite);
		shell.setMenuBar(menu);
	 }
	
	protected static void demanderNbJoueur() {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE);
		RowLayout rowLayout = new RowLayout();
		rowLayout.wrap=true;
		rowLayout.justify=true;
		fenetreFille.setLayout(rowLayout);
        fenetreFille.setText("Préparation Manuelle");
        
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        Label label = new Label(fenetreFille, SWT.NONE);
        label.setText("Nombre personne par équipe");
        final Combo comboPers = new Combo(fenetreFille, SWT.DROP_DOWN);
        String[] itemsPers = new String[] { "1", "2","3","4","5","6","7","8","9","10" };
        comboPers.setItems(itemsPers);
        comboPers.setText("1");
        
        Button button1 = new Button(fenetreFille, SWT.NONE);
        button1.setText("Valider");
        
        button1.addSelectionListener(new SelectionListener() {
        	public void widgetSelected(SelectionEvent e) {
        	  prepaManuel(Integer.valueOf(comboPers.getText()));
  	    	  fenetreFille.close();
  	    	  
  	      	}
  	      	public void widgetDefaultSelected(SelectionEvent e) {
  	      	}
        });
        
        fenetreFille.setSize(300, 200);
        fenetreFille.open();
		
	}
	
	
	protected static void AjouterSite() {

		final String filename = "resources/Sites.csv";
		final Shell fenetreFille = new Shell(display);
		fenetreFille.setSize(600, 400);
        fenetreFille.setText("Ajouter Site");
        fenetreFille.setLayout(new GridLayout());
        
        GridLayout layout = new GridLayout();
	    layout.numColumns = 2;
        
        
        //create scrolledComposite
        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        sc2.setLayout(new FillLayout());
        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        sc2.setExpandHorizontal(true);
	    sc2.setExpandVertical(true);
	    
	    //create composite
	    final Composite c2 = new Composite(sc2, SWT.NONE);
	    c2.setLayout(layout);
	    sc2.setContent(c2);
        
	    sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				  c2.layout();
				}
		} );

	    
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        Label label1 = new Label(c2, SWT.NONE);
        label1.setText("Nom du Site");
        final Text text1 = new Text(c2, SWT.BORDER);
        text1.setSize(200, 10);
                   
        Label label2 = new Label(c2, SWT.NONE);
        label2.setText("Spéléologue Requis");
        final Button speleoButton = new Button(c2, SWT.CHECK | SWT.FLAT);
        
        Label label3 = new Label(c2, SWT.NONE);
        label3.setText("Durée (00H00)");
        GridLayout layoutDuree = new GridLayout();
	    layoutDuree.numColumns = 3;
	    final Composite c3 = new Composite(c2, SWT.NONE);
	    c3.setLayout(layoutDuree);
        final Combo comboHeure = new Combo(c3, SWT.DROP_DOWN);
        String[] itemsHeure = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
        comboHeure.setItems(itemsHeure);
        comboHeure.setText("01");
        Label labelH = new Label(c3, SWT.NONE);
        labelH.setText("H");
        final Combo comboMinute = new Combo(c3, SWT.DROP_DOWN);
        String[] itemsMinute = new String[] { "00", "10", "20","30","40","50"};
        comboMinute.setItems(itemsMinute);
        comboMinute.setText("00");
        
        
        
        Label label4 = new Label(c2, SWT.NONE);
        label4.setText("Coordonnées");
        final Text text3 = new Text(c2, SWT.BORDER);
        text3.setSize(200, 10);
        
        Label label5 = new Label(c2, SWT.NONE);
        label5.setText("Materiel");
        final Text text4 = new Text(c2, SWT.BORDER);
        text4.setSize(200, 10);
            
        Button button1 = new Button(c2, SWT.NONE);
        button1.setText("Valider");
        
        button1.addSelectionListener(new SelectionListener() {
    	      public void widgetSelected(SelectionEvent e) {
    	    	  Site s = new Site(text1.getText(),speleoButton.getSelection(),new Heure(Integer.valueOf(comboHeure.getText()),
    	    			  Integer.valueOf(comboMinute.getText())),text3.getText(),text4.getText());
    	    	  Csv.writeInFile(filename, s);
    	    	  afficherSite();
    	    	  fenetreFille.close();
    	    	  //actualiserListBenevole(g,shell);
    	      }
    	      public void widgetDefaultSelected(SelectionEvent e) {
    	      }
    	    });

        Button button2 = new Button(c2, SWT.NONE);
        button2.setText("Annuler");
        button2.addSelectionListener(new SelectionListener() {
  	      public void widgetSelected(SelectionEvent e) {
  	    	  fenetreFille.close();
  	      }
  	      public void widgetDefaultSelected(SelectionEvent e) {
  	      }
  	    });

        fenetreFille.open();
	}
	
	protected static void modifierSite() {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE | SWT.RESIZE | SWT.MAX);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 5;
		fenetreFille.setLayout(gridLayout);
        fenetreFille.setText("Modifier Sites");
        
      //create scrolledComposite
        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        sc2.setLayout(new FillLayout());
        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        sc2.setExpandHorizontal(true);
	    sc2.setExpandVertical(true);
	    
	    //create composite
	    final Composite c2 = new Composite(sc2, SWT.NONE);
	    c2.setLayout(gridLayout);
	    sc2.setContent(c2);
        
	    sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				  c2.layout();
				}
		} );
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        final String filename = "resources/Sites.csv";
        int nb_sites=Csv.recupSizeList(filename);
		Site s;
		 for (int i = 0; i < nb_sites; i++) {
			 s = (Site) Csv.recupLigne(filename, i,"");
		     final Text text2 = new Text(c2, SWT.BORDER);
		     text2.setSize(200, 10);
		     text2.setText(s.getNom());
		     
		     final Text text3 = new Text(c2, SWT.BORDER);
		     text3.setSize(200, 10);
		     text3.setText(String.valueOf(s.isSpeleoRequis()));
		     
//		     final Text text4 = new Text(c2, SWT.BORDER);
//		     text4.setSize(200, 10);
//		     text4.setText(String.valueOf(b.isPermis()));
		     GridLayout layoutDuree = new GridLayout();
		     layoutDuree.numColumns = 3;
		     final Composite c3 = new Composite(c2, SWT.NONE);
		     c3.setLayout(layoutDuree);
		     final Combo comboHeure = new Combo(c3, SWT.DROP_DOWN);
		     String[] itemsHeure = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
	        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
		     comboHeure.setItems(itemsHeure);
		     String[] split=s.getDuree().toString().split("H");
		     comboHeure.setText(split[0]);
		     Label labelH = new Label(c3, SWT.NONE);
		     labelH.setText("H");
		     final Combo comboMinute = new Combo(c3, SWT.DROP_DOWN);
		     String[] itemsMinute = new String[] { "00", "10", "20","30","40","50"};
		     comboMinute.setItems(itemsMinute);
		     comboMinute.setText(split[1]);
		     
		     
		     final Text text4 = new Text(c2, SWT.BORDER);
		     text4.setSize(200, 10);
		     text4.setText(s.getCoordonnees());
		     
		     final Text text5 = new Text(c2, SWT.BORDER);
		     text5.setSize(200, 10);
		     text5.setText(s.getEquipementRequis());
		     
		     
		 }
		 
		 Button buttonSuppr = new Button(c2, SWT.PUSH);
	     buttonSuppr.setText("Modifier");
	     buttonSuppr.addSelectionListener(new SelectionListener() {
		  	 public void widgetSelected(SelectionEvent e) {
		  		List<Site> res=new ArrayList<Site>();
		  	    Control[] c = c2.getChildren();
		  	    String nom="",coordonnees="",equipement="";
	  	    	int heure=-1, minute=-1;
	  	    	boolean is_speleo=false;
		  	    for(int i=0;i<c.length-1;i++) {	
		  	    		int value=i%5;
			  	    	switch(value) {
			  	    	case 0:
			  	    		nom=((Text)(c[i])).getText();
			  	    		break;
			  	    	case 1:
			  	    		is_speleo=Boolean.valueOf(((Text)(c[i])).getText());
			  	    		break;
						case 2:
							Control[] c3=((Composite)c[i]).getChildren();
							if(((Combo)(c3[0])).getText().equals("-1")) {
				  	    		heure = 0;
				  	    	}else {
				  	    		heure = Integer.valueOf(((Combo)(c3[0])).getText());
				  	    	}	
							if(((Combo)(c3[2])).getText().equals("-1")) {
				  	    		minute = 0;
				  	    	}else {
				  	    		minute = Integer.valueOf(((Combo)(c3[2])).getText());
				  	    	}	
							break;
						
						case 3:
							coordonnees=((Text)(c[i])).getText();
							break;
						case 4:
							equipement=((Text)(c[i])).getText();
							Site s = new Site(nom,is_speleo,new Heure(heure,minute),coordonnees,equipement);
							res.add(s);
							break;
						
			  	    	}
		  	    }
		  	    Csv.modifierCsvSite(res);
		  	    
		  	  fenetreFille.close();
		  	 }
		  	 public void widgetDefaultSelected(SelectionEvent e) {
		  	 }
	  	 });
        fenetreFille.setSize(300, 200);
        fenetreFille.open();
      }
	
	
	protected static void prepaManuel(final int nbPersonnesParEquipe) {
		final Shell shell2 = new Shell (display);
	      shell2.setLayout(new FillLayout());
	
	      // set the size of the scrolled content - method 1
	      final ScrolledComposite sc1 = new ScrolledComposite(shell2, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
	      final Composite c1 = new Composite(sc1, SWT.NONE);
	      sc1.setContent(c1);
	      GridLayout layout = new GridLayout();
	      layout.numColumns = 1;
	      c1.setLayout(layout);
	      afficherBenevoles(c1);
	      c1.setSize(c1.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	      
	      
	      
	      double nb_benevoles=Csv.recupSizeList("resources/benevoles.csv");
	      double nb_equipe_temp =((double)nb_benevoles)/nbPersonnesParEquipe;
	      final int nb_equipe = (int)Math.ceil(nb_equipe_temp);
	      final ScrolledComposite sc2 = new ScrolledComposite(shell2, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
	      sc2.setExpandHorizontal(true);
	      sc2.setExpandVertical(true);
	      final Composite c2 = new Composite(sc2, SWT.NONE);
	      sc2.setContent(c2);
	      layout = new GridLayout();
	      layout.numColumns = nb_equipe;	   
	      c2.setLayout(layout);
	      afficherCadreEquipe(c2,nb_equipe,nbPersonnesParEquipe);
	      Button button1 = new Button(c2, SWT.NONE);
	        button1.setText("Valider");
	        button1.addSelectionListener(new SelectionListener() {
	        	public void widgetSelected(SelectionEvent e) {
	        	  org.supprimerAllEquipe();
	        	  HashMap<Integer, Equipe> liste_equipe;
	        	  liste_equipe=recupEquipesManuels(c2,nb_equipe,nbPersonnesParEquipe);
	        	  org.createEquipeManuel(liste_equipe);
	  	    	  affichageEquipe();
	  	    	  AfficherPlageHorraire();
	  	    	  shell2.close();
	  	    	  
	  	      	}
	  	      	public void widgetDefaultSelected(SelectionEvent e) {
	  	      	}
	        });
	      sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	      
	      
	      shell2.open ();
	}
	
	protected static HashMap<Integer, Equipe> recupEquipesManuels(Composite c2,final int nb_equipe,final int nb_pers_equipe) {
		HashMap<Integer, Equipe> liste_equipe=new HashMap<Integer, Equipe>();
		List<Object> listBene=Csv.recupListe("resources/benevoles.csv","");
		Control[] c=c2.getChildren();
		Control[] c_bis=((Group)c[0]).getChildren();
		for(int k=0;k<c_bis.length;k++) {
			System.out.println(c_bis[k]);
		}
		
		
		
		for(int i=0;i<nb_equipe;i++) {
			Equipe e=new Equipe(i+1);
			for(int j=0;j<nb_pers_equipe;j++) {
				for(int k=0;k<listBene.size();k++) {
					int n=((j+1)*(nb_equipe+1))+(i+1);
					String[] split=((Label)c_bis[n]).getText().split("\n");
					if(((Benevole)listBene.get(k)).getNom().equals(split[0]) && ((Benevole)listBene.get(k)).getPrenom().equals(split[1])) {
						e.ajouterBenevole(((Benevole)listBene.get(k)));
					}
				}
			}
			liste_equipe.put(i,e);
		}
		
		System.out.println(liste_equipe);
		
		return liste_equipe;
	}
	
	protected static void afficherCadreEquipe(Composite c2,int nb_equipe,int nbPersParEquip) {
		//creation groups
				Group ed = new Group(c2, SWT.BORDER | SWT.FLAT);
				ed.setText ("Equipes");
				ed.setSize(c2.getSize().x, c2.getSize().y);
				ed.setLocation(c2.getSize().x, c2.getSize().y);
				
				//premiere ligne
				Label label = new Label(ed,SWT.CENTER | SWT.BORDER | SWT.PUSH);
				label.setText("Equipes");
				label.setSize(100,75);
				label.setLocation(0,25);
				
				int posx=100;
				for(int i=1;i<nb_equipe+1;i++) {
					Label l = new Label(ed,SWT.CENTER | SWT.BORDER | SWT.PUSH);
					l.setText("Equipe " + i);
					l.setSize(100,75);
					l.setLocation(posx,25);
					posx += 100;
				}
				
				//generation des lignes suivantes
				posx = 100;
				int posy = 100;
				for(int i=0;i<nbPersParEquip;i++) {
					for(int j=0;j<nb_equipe+1;j++) {
						if(j==0) {
							Label l = new Label(ed,SWT.CENTER | SWT.BORDER | SWT.PUSH);
							l.setText("Membre "+(i+1));
							l.setSize(100,75);
							l.setLocation(0,posy);
						}else {
							Label l = new Label(ed,SWT.CENTER | SWT.BORDER | SWT.PUSH);
							l.setSize(100,75);
							
							l.setLocation(posx,posy);
							posx+=100;
							setDragDrop(l,1);
						}
						
					}
					posy+=75;
					posx = 100;
				}
		
	}
	
	protected static void afficherBenevoles(Composite c1) {
		List<Object> listBene=Csv.recupListe("resources/benevoles.csv","");
		int posY = 25;
		for (int i = 0; i < listBene.size(); i++) {
			final Label label = new Label(c1,SWT.NONE);
			label.setText(((Benevole)listBene.get(i)).getNom()+" "+((Benevole)listBene.get(i)).getPrenom());
			label.setData(listBene.get(i));
			label.setSize(100,25);
			label.setLocation(40,posY);
			setDragDrop(label,1);
			posY += 25;
		 }
	}
	

	protected static void prepaAuto() {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE);
		RowLayout rowLayout = new RowLayout();
		rowLayout.wrap=true;
		rowLayout.justify=true;
		fenetreFille.setLayout(rowLayout);
        fenetreFille.setText("Préparation Automatique");
        
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        Label label = new Label(fenetreFille, SWT.NONE);
        label.setText("Nombre personne par équipe");
        final Combo comboPers = new Combo(fenetreFille, SWT.DROP_DOWN);
        String[] itemsPers = new String[] { "1", "2","3","4","5","6","7","8","9","10" };
        comboPers.setItems(itemsPers);
        comboPers.setText("1");
        
        Button button1 = new Button(fenetreFille, SWT.NONE);
        button1.setText("Valider");
        
        button1.addSelectionListener(new SelectionListener() {
        	public void widgetSelected(SelectionEvent e) {
        	  org.supprimerAllEquipe();
  	    	  org.createEquipeAutoSmartV1(Integer.valueOf(comboPers.getText()));
  	    	  affichageEquipe();
  	    	  AfficherPlageHorraire();
  	    	  fenetreFille.close();
  	    	  
  	      	}
  	      	public void widgetDefaultSelected(SelectionEvent e) {
  	      	}
        });
        
        fenetreFille.setSize(300, 200);
        fenetreFille.open();
		
	}

	
	
	protected static void affichageEquipe() {
		groups[0].dispose();
		groups[0] = new Group(shell, SWT.BORDER | SWT.FLAT);
		groups[0].setText ("EQUIPES");
		groups[0].setSize(200, shell.getSize().y/2);
		groups[0].setLocation(0, 0);
		groups[0].setLayout(new FillLayout());
		
		
		int posY = 25;
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		groups[0].setLayout(gridLayout);
		
		for (int i = 0; i < org.getListe_equipe().size(); i++) {
//			final ExpandBar expandBar = new ExpandBar(groups[0], SWT.NONE);
//			final Composite expandContent = new Composite(expandBar, SWT.NONE);
//			GridLayout layout = new GridLayout();
//			layout.marginLeft = layout.marginTop=
//            layout.marginRight=layout.marginBottom=8;
//            layout.verticalSpacing = 10;
//            expandContent.setLayout(layout);
//            final Label label = new Label(expandContent, SWT.NONE);
//	        label.setText("Hello, world!");
//            final ExpandItem expandItem = new ExpandItem(expandBar, SWT.NONE);
//	        
//            expandBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
//			expandBar.setLocation(40, posY);
//	        expandItem.setText("ExpandItem");
//	        expandItem.setHeight
//	        (expandContent.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
//	        expandItem.setControl(expandContent);
	        
	        
	        final Label label = new Label(groups[0],SWT.NONE);
			label.setText("Equipe "+(i+1));
			label.setSize(100,25);
			label.setLocation(40,posY);
			setDragDrop(label,0);
			
			final Button button=new Button(groups[0],SWT.PUSH);
			button.setText("i");
			button.setSize(25,25);
			button.setLocation(10,posY-2);
			button.addSelectionListener(new SelectionListener() {
	        	public void widgetSelected(SelectionEvent e) {
	  	    	  System.out.println(button.getText());
	  	    	  Equipe e1 = org.recupEquipe(Integer.valueOf(label.getText().split(" ")[1]));
	  	    	  afficherInfoEquipe(e1);
	  	      	}
	  	      	public void widgetDefaultSelected(SelectionEvent e) {
	  	      	}
	        });
			posY += 25;
		 }
		
	}

	protected static void afficherInfoEquipe(Equipe e1) {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE | SWT.RESIZE | SWT.MAX);
		FillLayout principal = new FillLayout();
		principal.type = SWT.HORIZONTAL;
		fenetreFille.setLayout(principal);
        fenetreFille.setText("Informations equipe");
        
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        
        Group benev = new Group(fenetreFille,SWT.V_SCROLL);
        benev.setText("Liste des bénévoles de l'equipe");
        Group info = new Group(fenetreFille, SWT.NONE);
        info.setText("Informations relatives à l'équipe");
        
        //partie benevole
        GridLayout FBenev = new GridLayout();
        FBenev.numColumns = 1;
        benev.setLayout(FBenev);
        ArrayList<Benevole> ListBenev = e1.getBenevoles();
        
        for(int i = 0; i<ListBenev.size();i++) {
        	Group b = new Group(benev,SWT.CENTER);
        	b.setSize(100, 10);
        	Label l1 = new Label(b, SWT.CENTER); 
        	l1.setText(ListBenev.get(i).getNom()+"\n"+ListBenev.get(i).getPrenom());
        	l1.setSize(100, 50);
        }
		 
		//partie info
        FillLayout FInfo = new FillLayout();
        FInfo.type = SWT.VERTICAL;
        info.setLayout(FInfo);
    	Label l1 = new Label(info, SWT.NONE); 
    	if(e1.isVoiture()) {
    		l1.setText("Voiture : OUI");
    	}else {
    		l1.setText("Voiture : Non");
    	}
    	
    	Label l2 = new Label(info, SWT.NONE); 
    	if(e1.get_is_equipe_speleo()) {
    		l2.setText("Acces Site Spéléogues requis : OUI");
    	}else {
    		l2.setText("Acces Site Spéléogues requis : NON");
    	}
    	
    	
    	Label l3 = new Label(info, SWT.NONE);
    	int[] tab=e1.getIdentificateur_niveaux();
    	l3.setText("Nombre Niveaux Identificateurs (0/1/2) : "+tab[0]+"/"+tab[1]+"/"+tab[2]);
        
        fenetreFille.setSize(300, 200);
        fenetreFille.open();
		
	}

	protected static void ajouterBenevole() {
		final String filename = "resources/benevoles.csv";
		final Shell fenetreFille = new Shell(display, SWT.MAX | SWT.RESIZE);
		fenetreFille.setSize(750, 400);
        fenetreFille.setText("Ajouter Benevole");
        fenetreFille.setLayout(new GridLayout());
        
        GridLayout layout = new GridLayout();
	    layout.numColumns = 2;
        
        
        //create scrolledComposite
        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        sc2.setLayout(new FillLayout());
        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        sc2.setExpandHorizontal(true);
	    sc2.setExpandVertical(true);
	    
	    //create composite
	    final Composite c2 = new Composite(sc2, SWT.NONE);
	    c2.setLayout(layout);
	    sc2.setContent(c2);
        
	    sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				  c2.layout();
				}
		} );

	    
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        final List<List<Object>> objects =new ArrayList<List<Object>>();
        
        Label label1 = new Label(c2, SWT.NONE);
        label1.setText("Nom");
        final Text text1 = new Text(c2, SWT.BORDER);
        text1.setSize(200, 10);
               

        Label label2 = new Label(c2, SWT.NONE);
        label2.setText("Prenom");
        final Text text2 = new Text(c2, SWT.BORDER);
        text2.setSize(200, 10);

        Label label3 = new Label(c2, SWT.NONE);
        label3.setText("Permis");
        final Button permisButton = new Button(c2, SWT.CHECK | SWT.FLAT);
        
        Label label4 = new Label(c2, SWT.NONE);
        label4.setText("Speleologue");
        final Combo combo = new Combo(c2, SWT.DROP_DOWN);
        String[] items = new String[] { "0", "1", "2" };
        combo.setItems(items);
        combo.setText("0");
        
        Label label5 = new Label(c2, SWT.NONE);
        label5.setText("Identificateur");
        final Combo comboIdent = new Combo(c2, SWT.DROP_DOWN);
        String[] itemsIdent = new String[] { "0", "1", "2" };
        comboIdent.setItems(itemsIdent);
        comboIdent.setText("0");

        Label label6 = new Label(c2, SWT.NONE);
        label6.setText("Voiture");
        GridLayout layout2 = new GridLayout();
	    layout2.numColumns = 5;
        final Composite c3 = new Composite(c2, SWT.NONE);
	    c3.setLayout(layout2);
        final Combo comboPlace = new Combo(c3, SWT.DROP_DOWN);
        String[] itemsPlace = new String[] { "0", "1", "2","3","4","5","6" };
        comboPlace.setItems(itemsPlace);
        comboPlace.setText("0");
        Label label7 = new Label(c3, SWT.NONE);
        label7.setText("Immatriculation");
        final Text text3=new Text(c3,SWT.BORDER);
        text3.setSize(200, 10);
        Label label8 = new Label(c3, SWT.NONE);
        label8.setText("Modele");
        final Text text4=new Text(c3,SWT.BORDER);
        text4.setSize(200, 10);
        
        GridLayout layout3 = new GridLayout();
	    layout3.numColumns = 3;
	    final Composite c4 = new Composite(fenetreFille, SWT.NONE);
	    c4.setLayout(layout3);
        Button button_addContrainte = new Button(c4, SWT.NONE);
        button_addContrainte.setText("Ajouter Contrainte");
        final Button button1 = new Button(c4, SWT.NONE);
        button1.setText("Valider");
        final Button button2 = new Button(c4, SWT.NONE);
        button2.setText("Annuler");
        
        button1.addSelectionListener(new SelectionListener() {
    	      public void widgetSelected(SelectionEvent e) {
    	    	  int speleo, ident;
    	    	  if(combo.getSelectionIndex() == -1) {
    	    		  speleo = 0;
    	    	  }else {
    	    		  speleo = combo.getSelectionIndex();
    	    	  }
    	    	  if(comboIdent.getSelectionIndex() == -1) {
    	    		  ident = 0;
    	    	  }else {
    	    		  ident = comboIdent.getSelectionIndex();
    	    	  }
    	    	  int id = Csv.RecupidMax(filename)+1;
    	    	  
    	    	  Benevole b = new Benevole(id,text1.getText(), text2.getText(), permisButton.getSelection(), 
    	    			  speleo, ident,new Voiture(Integer.valueOf(comboPlace.getText()),text3.getText(), text4.getText()));
    	    	  Csv.writeInFile(filename, b);
    	    	  if(Integer.valueOf(comboPlace.getText())!=0) {
    	    		  Csv.writeInFile("resources/voitures.csv", b);
    	    	  }
    	    	  Control[] c = c2.getChildren();
    	    	  for(int i=0;i<c.length;i++) {
    	    		  System.out.println(c[i]);
    	    	  }
    	    	  
    	    	  //int nb_contraintes=(c.length-16)/6;
    	    	  for(int a=0;a<objects.size();a++) {
    	    		  if( (!(((Combo)objects.get(a).get(1)).getText().equals("")) && !(((Combo)objects.get(a).get(2)).getText().equals(""))) || (!(((Combo)objects.get(a).get(3)).getText().equals("")) &&  !(((Combo)objects.get(a).get(4)).getText().equals("")))) {
        	    		  Date date =new Date(((DateTime)objects.get(a).get(0)).getDay(), ((DateTime)objects.get(a).get(0)).getMonth()+1, ((DateTime)objects.get(a).get(0)).getYear());
        	    		  Heure h1=new Heure(-1,-1);
        	    		  if(!(((Combo)objects.get(a).get(1)).getText().equals("")) && !(((Combo)objects.get(a).get(2)).getText().equals(""))) {
        	    			  h1.setH(Integer.valueOf(((Combo)objects.get(a).get(1)).getText()));
        	    			  h1.setM(Integer.valueOf(((Combo)objects.get(a).get(2)).getText()));
        	    		  }
        	    		  Heure h2=new Heure(-1,-1);
        	    		  if(!(((Combo)objects.get(a).get(3)).getText().equals("")) &&  !(((Combo)objects.get(a).get(4)).getText().equals(""))) {
        	    			  h2.setH(Integer.valueOf(((Combo)objects.get(a).get(3)).getText()));
        	    			  h2.setM(Integer.valueOf(((Combo)objects.get(a).get(4)).getText()));
        	    		  }
        	    		  Contrainte contrainte=new Contrainte(date, h1, h2);
        	    		  b.ajouterContrainte(contrainte);
        	    		  Csv.writeInFile("resources/contraintes.csv",b);
        	    	  }
    	    	  }
    	    	  
    	    	  fenetreFille.close();
    	    	  //actualiserListBenevole(g,shell);
    	      }
    	      public void widgetDefaultSelected(SelectionEvent e) {
    	      }
    	    });

        
        button2.addSelectionListener(new SelectionListener() {
  	      public void widgetSelected(SelectionEvent e) {
  	    	  fenetreFille.close();
  	      }
  	      public void widgetDefaultSelected(SelectionEvent e) {
  	      }
  	    });
        
        button_addContrainte.addListener(SWT.Selection, new Listener() {
	          public void handleEvent(Event e) {
	        	  
	        	  objects.add(afficherAjoutContrainte(c2));
	              // reset the minimum width and height so children can be seen - method 2
	              sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	              c2.layout();
	          }
	      });


        
//        button_addContrainte.addSelectionListener(new SelectionListener() {
//    	      public void widgetSelected(SelectionEvent e) {
//    	    	  final List<Object> objects=afficherAjoutContrainte(c2);
//    	      }
//    	      public void widgetDefaultSelected(SelectionEvent e) {
//    	      }
//    	    });
        
        fenetreFille.open();
      }
	
	protected static List<Object> afficherAjoutContrainte(Composite c2) {
		List<Object> res=new ArrayList<Object>();
		
		//AJOUTER 1 contrainte lors de la creation d'un bénévole
	      Label label10 = new Label(c2,SWT.NONE);
	      label10.setText("Date");
	      
	      final DateTime datetime= new DateTime(c2, SWT.CALENDAR);
	      res.add(datetime);
	      Label label11 = new Label(c2, SWT.NONE);
	      label11.setText("Heure Arrivée");
	      GridLayout layoutDuree_A = new GridLayout();
		    layoutDuree_A.numColumns = 3;
		    final Composite c3_A = new Composite(c2, SWT.NONE);
		    c3_A.setLayout(layoutDuree_A);
	      final Combo comboHeure_A = new Combo(c3_A, SWT.DROP_DOWN);
	      String[] itemsHeure_A = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
	      		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
	      comboHeure_A.setItems(itemsHeure_A);
	      res.add(comboHeure_A);
	      
	      
	      Label labelH_A = new Label(c3_A, SWT.NONE);
	      labelH_A.setText("H");
	      final Combo comboMinute_A = new Combo(c3_A, SWT.DROP_DOWN);
	      String[] itemsMinute_A = new String[] { "00", "10", "20","30","40","50"};
	      comboMinute_A.setItems(itemsMinute_A);
	      res.add(comboMinute_A);
	      
	      Label label12 = new Label(c2, SWT.NONE);
	      label12.setText("Heure Départ");
	      GridLayout layoutDuree_D = new GridLayout();
		    layoutDuree_D.numColumns = 3;
		    final Composite c3_D = new Composite(c2, SWT.NONE);
		    c3_D.setLayout(layoutDuree_D);
	      final Combo comboHeure_D = new Combo(c3_D, SWT.DROP_DOWN);
	      String[] itemsHeure_D = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
	      		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
	      comboHeure_D.setItems(itemsHeure_D);
	      res.add(comboHeure_D);
	      
	      Label labelH = new Label(c3_D, SWT.NONE);
	      labelH.setText("H");
	      final Combo comboMinute_D = new Combo(c3_D, SWT.DROP_DOWN);
	      String[] itemsMinute_D = new String[] { "00", "10", "20","30","40","50"};
	      comboMinute_D.setItems(itemsMinute_D);
	      res.add(comboMinute_D);
	      return res;
	}
	
//	protected static Contrainte ajouterContrainte() {
//		final Contrainte contrainte=new Contrainte(null, null, null);
//		
//		final Shell fenetreFille = new Shell(display, SWT.MAX | SWT.RESIZE);
//		fenetreFille.setSize(750, 400);
//        fenetreFille.setText("Ajouter Contrainte");
//        fenetreFille.setLayout(new GridLayout());
//        
//        GridLayout layout = new GridLayout();
//	    layout.numColumns = 2;
//        
//        
//        //create scrolledComposite
//        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
//        sc2.setLayout(new FillLayout());
//        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
//        sc2.setExpandHorizontal(true);
//	    sc2.setExpandVertical(true);
//	    
//	    //create composite
//	    final Composite c2 = new Composite(sc2, SWT.NONE);
//	    c2.setLayout(layout);
//	    sc2.setContent(c2);
//        
//	    sc2.addListener( SWT.Resize, new Listener() {
//			public void handleEvent(Event event) {
//				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
//				  c2.layout();
//				}
//		} );
//
//	    
//        fenetreFille.addListener(SWT.Close, new Listener() {
//        	public void handleEvent(Event e) {
//	            System.out.println("Fermeture de la boite de dialogue");
//	          }
//		});
//        
//        Label label1 = new Label(c2,SWT.NONE);
//        label1.setText("Date");
//        final DateTime datetime= new DateTime(c2, SWT.CALENDAR);
//        
//        Label label2 = new Label(c2, SWT.NONE);
//        label2.setText("Heure Arrivée");
//        GridLayout layoutDuree_A = new GridLayout();
//	    layoutDuree_A.numColumns = 3;
//	    final Composite c3_A = new Composite(c2, SWT.NONE);
//	    c3_A.setLayout(layoutDuree_A);
//        final Combo comboHeure_A = new Combo(c3_A, SWT.DROP_DOWN);
//        String[] itemsHeure_A = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
//        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
//        comboHeure_A.setItems(itemsHeure_A);
//        comboHeure_A.setText("01");
//        Label labelH_A = new Label(c3_A, SWT.NONE);
//        labelH_A.setText("H");
//        final Combo comboMinute_A = new Combo(c3_A, SWT.DROP_DOWN);
//        String[] itemsMinute_A = new String[] { "00", "10", "20","30","40","50"};
//        comboMinute_A.setItems(itemsMinute_A);
//        comboMinute_A.setText("00");
//        
//        Label label3 = new Label(c2, SWT.NONE);
//        label3.setText("Heure Départ");
//        GridLayout layoutDuree_D = new GridLayout();
//	    layoutDuree_D.numColumns = 3;
//	    final Composite c3_D = new Composite(c2, SWT.NONE);
//	    c3_D.setLayout(layoutDuree_D);
//        final Combo comboHeure_D = new Combo(c3_D, SWT.DROP_DOWN);
//        String[] itemsHeure_D = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
//        		,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
//        comboHeure_D.setItems(itemsHeure_D);
//        comboHeure_D.setText("01");
//        Label labelH = new Label(c3_D, SWT.NONE);
//        labelH.setText("H");
//        final Combo comboMinute_D = new Combo(c3_D, SWT.DROP_DOWN);
//        String[] itemsMinute_D = new String[] { "00", "10", "20","30","40","50"};
//        comboMinute_D.setItems(itemsMinute_D);
//        comboMinute_D.setText("00");
//        
//        Button button1 = new Button(c2, SWT.NONE);
//        button1.setText("Valider");
//        
//        button1.addSelectionListener(new SelectionListener() {
//        	  
//    	      public void widgetSelected(SelectionEvent e) {
//    	    	  contrainte.setD(new Date(datetime.getDay(), datetime.getMonth()+1, datetime.getYear()));
//    	    	  contrainte.setH1(new Heure(Integer.valueOf(comboHeure_A.getText()), Integer.valueOf(comboMinute_A.getText())));
//    	    	  contrainte.setH2(new Heure(Integer.valueOf(comboHeure_D.getText()), Integer.valueOf(comboMinute_D.getText())));
//    	    	  
//    	    	  fenetreFille.close();
//    	    	  
//    	      }
//    	      public void widgetDefaultSelected(SelectionEvent e) {
//    	      }
//    	      
//    	    });
//
//        Button button2 = new Button(c2, SWT.NONE);
//        button2.setText("Annuler");
//        button2.addSelectionListener(new SelectionListener() {
//        	
//  	      public void widgetSelected(SelectionEvent e) {
//  	    	  fenetreFille.close();
//  	      }
//  	      public void widgetDefaultSelected(SelectionEvent e) {
//  	      }
//  	    });
//        
//        fenetreFille.open();
//        return contrainte;
//	}

	protected static void modifierBenevole() {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE | SWT.RESIZE | SWT.MAX);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 8;
		fenetreFille.setLayout(new FillLayout());
        fenetreFille.setText("Modifier Benevole");
        
      //create scrolledComposite
        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        sc2.setLayout(new FillLayout());
        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        sc2.setExpandHorizontal(true);
	    sc2.setExpandVertical(true);
	    
	    //create composite
	    final Composite c2 = new Composite(sc2, SWT.NONE);
	    c2.setLayout(gridLayout);
	    sc2.setContent(c2);
        
	    sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				  c2.layout();
				}
		} );
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        final String filename = "resources/benevoles.csv";
        int nb_benevoles=Csv.recupSizeList(filename);
		Benevole b;
		 for (int i = 0; i < nb_benevoles; i++) {
			 b = (Benevole) Csv.recupLigne(filename, i,"");
		     final Text text2 = new Text(c2, SWT.BORDER);
		     text2.setSize(200, 10);
		     text2.setText(b.getNom());
		     
		     final Text text3 = new Text(c2, SWT.BORDER);
		     text3.setSize(200, 10);
		     text3.setText(b.getPrenom());
		     
		     final Text text4 = new Text(c2, SWT.BORDER);
		     text4.setSize(200, 10);
		     text4.setText(String.valueOf(b.isPermis()));
		     
		     final Combo comboSpeleo = new Combo(c2, SWT.DROP_DOWN);
		     String[] itemsSpeleo = new String[] { "0", "1", "2" };
		     comboSpeleo.setItems(itemsSpeleo);
		     comboSpeleo.setText(String.valueOf(b.getCompetenceSpeleo()));
		    
		     final Combo comboIdent = new Combo(c2, SWT.DROP_DOWN);
		     String[] itemsIdent = new String[] { "0", "1", "2" };
		     comboIdent.setItems(itemsIdent);
		     comboIdent.setText(String.valueOf(b.getIdentificateur()));
		     
		     final Combo comboPlace = new Combo(c2, SWT.DROP_DOWN);
		     String[] itemsPlace = new String[] { "0", "1", "2","3","4","5","6" };
		     comboPlace.setItems(itemsPlace);
		     if(b.getVoiture()==null) {
		    	 comboPlace.setText(String.valueOf(0));
		     }else {
		    	 comboPlace.setText(String.valueOf(b.getVoiture().getNbPlace()));
		     }
		     
		     final Text text5 = new Text(c2, SWT.BORDER);
		     text5.setSize(200, 10);
		     if(b.getVoiture()==null) {
		    	 text5.setText("empty");
		     }else {
		    	 text5.setText(b.getVoiture().getImmatriculation());
		     }
		     
		     
		     final Text text6 = new Text(c2, SWT.BORDER);
		     text6.setSize(200, 10);
		     if(b.getVoiture()==null) {
		    	 text6.setText("empty");
		     }else {
		    	 text6.setText(b.getVoiture().getModele());
		     }
		     
		     
		 }
		 
		 Button buttonSuppr = new Button(c2, SWT.PUSH);
	     buttonSuppr.setText("Modifier");
	     buttonSuppr.addSelectionListener(new SelectionListener() {
		  	 public void widgetSelected(SelectionEvent e) {
		  		List<Benevole> res=new ArrayList<Benevole>();

		  		Control[] c = c2.getChildren();
		  	    int line = 0;
		  	    String nom="",prenom="",immatriculation="",modele="";
	  	    	int speleo=-1, ident=-1,nb_place=0;
	  	    	boolean have_permis=false;
		  	    for(int i=0;i<c.length-1;i++) {	
		  	    		int value=i%8;
			  	    	switch(value) {
			  	    	case 0:
			  	    		nom=((Text)(c[i])).getText();
			  	    		break;
			  	    	case 1:
			  	    		prenom=((Text)(c[i])).getText();
			  	    		break;
						case 2:
							have_permis=Boolean.valueOf(((Text)(c[i])).getText());	
							break;
						case 3:
							if(((Combo)(c[i])).getText().equals("-1")) {
				  	    		speleo = 0;
				  	    	}else {
				  	    		speleo = Integer.valueOf(((Combo)(c[i])).getText());
				  	    	}	
							break;
						case 4:
							if(((Combo)(c[i])).getText().equals("-1")) {
				  	    		ident = 0;
				  	    	}else {
				  	    		ident = Integer.valueOf(((Combo)(c[i])).getText());
				  	    	}	
							break;
						case 5:
							nb_place=Integer.valueOf(((Combo)(c[i])).getText());	
							break;
						case 6:
							immatriculation=((Text)(c[i])).getText();
							break;
						case 7:
							modele=((Text)(c[i])).getText();
							Voiture v=new Voiture(nb_place,immatriculation,modele);
							Benevole b = new Benevole(line,nom, prenom, have_permis, speleo, ident,v);
							res.add(b);
							line++;
							break;
			  	    	}
		  	    }
		  	    Csv.modifierCsvBenevole(res);
		  	    
		  	    
		  	  fenetreFille.close();
		  	 }
		  	 public void widgetDefaultSelected(SelectionEvent e) {
		  	 }
	  	 });
	    fenetreFille.setMinimumSize(800, 500);
        fenetreFille.open();
      }
	protected static void Supprimerbenevole() {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE | SWT.RESIZE | SWT.MAX);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 10;
		fenetreFille.setLayout(new FillLayout());
        fenetreFille.setText("Supprimer Benevole");
        
      //create scrolledComposite
        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER );
        sc2.setLayout(new FillLayout());
        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        sc2.setExpandHorizontal(true);
	    sc2.setExpandVertical(true);
	    
	    //create composite
	    final Composite c2 = new Composite(sc2, SWT.NONE);
	    c2.setLayout(gridLayout);
	    sc2.setContent(c2);
        
	    sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				  c2.layout();
				}
		} );
        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        
        
        final String filename = "resources/benevoles.csv";
        int nb_benevoles=Csv.recupSizeList(filename);
		Benevole b;
		 for (int i = 0; i < nb_benevoles; i++) {
			 b = (Benevole) Csv.recupLigne(filename, i,"");
			 @SuppressWarnings("unused")
			Button check = new Button(c2, SWT.CHECK | SWT.FLAT);
			 Label label1 = new Label(c2, SWT.NONE);
		     label1.setText(String.valueOf(b.getId()));
		     Label label2 = new Label(c2, SWT.NONE);
		     label2.setText(b.getNom());
		     Label label3 = new Label(c2, SWT.NONE);
		     label3.setText(b.getPrenom());
		     Label label4 = new Label(c2, SWT.NONE);
		     label4.setText(String.valueOf(b.isPermis()));
		     Label label5 = new Label(c2, SWT.NONE);
		     label5.setText(String.valueOf(b.getCompetenceSpeleo()));
		     Label label6 = new Label(c2, SWT.NONE);
		     label6.setText(String.valueOf(b.getIdentificateur()));
		     Label label7 = new Label(c2, SWT.NONE);
		     Label label8 = new Label(c2, SWT.NONE);
		     Label label9 = new Label(c2, SWT.NONE);
		     if(b.getVoiture()==null) {
		    	 label7.setText(String.valueOf(0));
		    	 label8.setText("empty");
		    	 label9.setText("empty");
		     }else {
		    	 label7.setText(String.valueOf(b.getVoiture().getNbPlace()));
		    	 label8.setText(b.getVoiture().getImmatriculation());
		    	 label9.setText(b.getVoiture().getModele());
		     }
		     
		     
		 }
		 
		 Button buttonSuppr = new Button(c2, SWT.PUSH);
	     buttonSuppr.setText("Supprimer");
	     buttonSuppr.addSelectionListener(new SelectionListener() {
		  	 public void widgetSelected(SelectionEvent e) {
		  	    Control[] c = c2.getChildren();
		  	    int line = 0;
		  	    for(int i=0;i<c.length;i++) {
		  	    	if(i%10 == 0 && i!=c.length-1) {
		  	    		if(((Button) c[i]).getSelection()) {
		  	    			System.out.println(line);
		  	    			Csv.deleteLigne(filename, line,null);
		  	    			line-=1;
		  	    		}
		  	    		line+=1;
		  	    	}
		  	    }
		  	    
		  	  fenetreFille.close();
		  	 }
		  	 public void widgetDefaultSelected(SelectionEvent e) {
		  	 }
	  	 });
		 
		 
        
	    fenetreFille.setMinimumSize(800, 500);
        fenetreFille.open();
      }
	
	protected static void SupprimerSite() {
		final Shell fenetreFille = new Shell(shell, SWT.TITLE | SWT.CLOSE | SWT.RESIZE | SWT.MAX);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 6;
		fenetreFille.setLayout(gridLayout);
        fenetreFille.setText("Supprimer Site");
        
        
        
      //create scrolledComposite
        final ScrolledComposite sc2 = new ScrolledComposite(fenetreFille, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        sc2.setLayout(new FillLayout());
        sc2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        sc2.setExpandHorizontal(true);
	    sc2.setExpandVertical(true);
	    
	    //create composite
	    final Composite c2 = new Composite(sc2, SWT.NONE);
	    c2.setLayout(gridLayout);
	    sc2.setContent(c2);
        
	    sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				  c2.layout();
				}
		} );

        fenetreFille.addListener(SWT.Close, new Listener() {
        	public void handleEvent(Event e) {
	            System.out.println("Fermeture de la boite de dialogue");
	          }
		});
        
        
        
        final String filename = "resources/Sites.csv";
        int nb_sites=Csv.recupSizeList(filename);
		Site s;
		 for (int i = 0; i < nb_sites; i++) {
			 s = (Site) Csv.recupLigne(filename, i,"");
			 @SuppressWarnings("unused")
			 Button check = new Button(c2, SWT.CHECK | SWT.FLAT);
			 Label label1 = new Label(c2, SWT.NONE);
		     label1.setText(String.valueOf(s.getNom()));
		     Label label2 = new Label(c2, SWT.NONE);
		     label2.setText(String.valueOf(s.isSpeleoRequis()));
		     Label label3 = new Label(c2, SWT.NONE);
		     label3.setText(s.getDuree().toString());
		     Label label4 = new Label(c2, SWT.NONE);
		     label4.setText(s.getCoordonnees());
		     Label label5 = new Label(c2, SWT.NONE);
		     label5.setText(s.getEquipementRequis());
		     
		     
		 }
		 
		 Button buttonSuppr = new Button(c2, SWT.PUSH);
	     buttonSuppr.setText("Supprimer");
	     buttonSuppr.addSelectionListener(new SelectionListener() {
		  	 public void widgetSelected(SelectionEvent e) {
		  	    Control[] c = c2.getChildren();
		  	    int line = 0;
		  	    for(int i=0;i<c.length;i++) {
		  	    	if(i%6 == 0 && i!=c.length-1) {
		  	    		if(((Button) c[i]).getSelection()) {
		  	    			System.out.println(line);
		  	    			Csv.deleteLigne(filename, line,null);
		  	    			line-=1;
		  	    		}
		  	    		line+=1;
		  	    	}
		  	    }
		  	    afficherSite();
		  	  fenetreFille.close();
		  	 }
		  	 public void widgetDefaultSelected(SelectionEvent e) {
		  	 }
	  	 });
		 
		 
        
        fenetreFille.setSize(300, 200);
        fenetreFille.open();
      }

	public static void createZone(Display display, Group group) {
		//zone benevole
		groups[0].setText ("EQUIPES");
		groups[0].setSize(200, shell.getSize().y/2);
		groups[0].setLocation(0, 0);
		groups[0].setLayout(new FillLayout());
		//afficherBenevoleFile(group);
		Group group2=new Group(shell, SWT.NONE);
		group2.setText ("SITES");
		group2.setSize(200, shell.getSize().y/2);
		group2.setLocation(0, shell.getSize().y/2+2);
		group2.setLayout(new FillLayout());
		groups[1]=group2;
		afficherSite();
		
		//zone Jours
		Group jours = new Group(shell, SWT.BORDER | SWT.FLAT);
		jours.setText ("JOURS");
		jours.setSize(shell.getSize().x - group.getSize().x - 10, 100);
		jours.setLocation(group.getSize().x +10, 0);
		groups[2]=jours;
		
		//	Bouton gauche date
		ToolBar toolbar=new ToolBar(jours,SWT.FLAT);
		toolbar.setSize(250,50);
		toolbar.setLocation(25, (jours.getSize().y-20)/2);
		Image jourAvant = new Image(display,"resources/previous.bmp");
		int width = jourAvant.getBounds().width;
	    int height = jourAvant.getBounds().height;
		Image jourAvant020 = new Image(display,jourAvant.getImageData().scaledTo((int)(width*0.2),(int)(height*0.2)));
		ToolItem btnJourAvant = new ToolItem(toolbar,SWT.PUSH);
		btnJourAvant.setText("<--");
		//btnJourAvant.setImage(jourAvant020);
		jourAvant020.dispose();
		
		
		
        // Date central 
		LocalDate localDate = LocalDate.now();
        String dateString=DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
        System.out.println(dateString);
        int annee=Integer.parseInt(dateString.substring(0, 4));
        int mois=Integer.parseInt(dateString.substring(5, 7));
        int jour=Integer.parseInt(dateString.substring(8, 10));
        current_date.setDate(jour, mois, annee);
        System.out.println(current_date.toString()); 
        ToolBar toolbar3=new ToolBar(jours,SWT.FLAT);
		toolbar3.setSize(250,50);
		toolbar3.setLocation(jours.getSize().x/2-25, (jours.getSize().y-20)/2);
        final ToolItem btnDate= new ToolItem(toolbar3, SWT.PUSH);
        btnDate.setText(current_date.toString());
      
        
        //	Bouton droite date
		ToolBar toolbar2=new ToolBar(jours,SWT.FLAT);
		toolbar2.setSize(250,50);
		toolbar2.setLocation(jours.getSize().x-80, (jours.getSize().y-20)/2);
		Image jourApres = new Image(Display.getDefault(),"resources/nextt.png");
		int width2 = jourApres.getBounds().width;
	    int height2 = jourApres.getBounds().height;
		Image jourApres020 = new Image(display,jourApres.getImageData().scaledTo((int)(width2*0.2),(int)(height2*0.2)));
		ToolItem btnJourApres = new ToolItem(toolbar2,SWT.PUSH);
		btnJourApres.setText("-->");
		//btnJourApres.setImage(jourApres020);
		jourApres020.dispose();		
		
		//Listener Bouton gauche date
		btnJourAvant.addSelectionListener(new SelectionListener() {
	      public void widgetSelected(SelectionEvent e) {
	    	  current_date.addJour(-1);
	    	  btnDate.setText(current_date.toString());
	    	  AfficherPlageHorraire();
	      }
	      public void widgetDefaultSelected(SelectionEvent e) {
	      }
	    });
		
		//Listener Bouton droite date
		btnJourApres.addSelectionListener(new SelectionListener() {
	      public void widgetSelected(SelectionEvent e) {
	    	  current_date.addJour(1);
	    	  btnDate.setText(current_date.toString());
	    	  AfficherPlageHorraire();
	      }
	      public void widgetDefaultSelected(SelectionEvent e) {
	      }
	    });

		
		
		
		//zone equipe/date
		Group ed = new Group(shell, SWT.BORDER | SWT.FLAT);
		ed.setText ("Plage Horaires");
		ed.setSize(shell.getSize().x - group.getSize().x - 10, shell.getSize().y);
		ed.setLocation(group.getSize().x +10, jours.getSize().y +10);
		groups[3]=ed;
		AfficherPlageHorraire();
		
	}
	

	private static void AfficherPlageHorraire() {
		//recup infos
		List<Object> infos=Csv.recupListe("resources/edt.csv", current_date.toString());
		
		//creation groups
		groups[3].dispose();
		Group ed = new Group(shell, SWT.BORDER | SWT.FLAT);
		ed.setText ("Plage Horaires");
		ed.setSize(shell.getSize().x - groups[0].getSize().x - 10, shell.getSize().y);
		ed.setLocation(groups[0].getSize().x +10, groups[2].getSize().y +10);
		groups[3]=ed;
		
		//premiere ligne
		Label label = new Label(groups[3],SWT.CENTER | SWT.BORDER | SWT.PUSH);
		label.setText("Heure");
		label.setSize(100,75);
		label.setLocation(0,25);
		
		int posx=100;
		for(int i=1;i<org.getListe_equipe().size()+1;i++) {
			Label l = new Label(groups[3],SWT.CENTER | SWT.BORDER | SWT.PUSH);
			if(!org.getListe_equipe().get(i-1).isContraintes_is_check()) {
				l.setBackground(new Color(display, 255,0,0));
				l.setForeground(new Color(display, 0,0,0));
				FontData[] fD = l.getFont().getFontData();
				fD[0].setHeight(16);
				l.setFont( new Font(display,fD[0]));
			}
			l.setText("Equipe " + i);
			l.setSize(200,75);
			l.setLocation(posx,25);
			posx += 200;
		}
		
		//generation des lignes suivantes
		int horraire = current_date.getDebut_horaire().getH();
		posx = 100;
		int posy = 100;
		for(int i=0;i<current_date.getFin_horaire().getH()-current_date.getDebut_horaire().getH()+1;i++) {
			for(int j=0;j<org.getListe_equipe().size()+1;j++) {
				if(j==0) {
					Label l = new Label(groups[3],SWT.CENTER | SWT.BORDER | SWT.PUSH);
					l.setText(horraire+"H00");
					l.setSize(100,75);
					l.setLocation(0,posy);
				}else {
					Label l = new Label(groups[3],SWT.CENTER | SWT.BORDER | SWT.PUSH);
					l.setSize(200,75);
					boolean ok = false;
				    for(int k=0;k<infos.size();k++) {
						String[] split = ((String)infos.get(k)).split(",");
						if(Integer.valueOf((split[0].split(" "))[1]) == j) {
							if(Integer.valueOf((split[2].split("H")[0]))-current_date.getDebut_horaire().getH() <= i
									&& Integer.valueOf((split[2].split("H")[0])) + 
									   Integer.valueOf((split[3].split("H")[0]))-current_date.getDebut_horaire().getH() > i 
									&& !ok) {
								l.setText(split[1]);
								ok = true;
							}else {
								if(!ok)
								l.setText("...");
							}
							
						}else {
							if(!ok)
							l.setText("...");	    
						}
					}
					if(infos.size()==0) {
						l.setText("...");
					}
					l.setLocation(posx,posy);
					posx+=200;
					setDragDrop(l,0);
				}
				
			}
			horraire+=1;
			posy+=75;
			posx = 100;
		}
		
		
	}
	

	public static void setDragDrop(final Label label,final int fenetre_num) {

	    Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
	    int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;

	    final DragSource source = new DragSource(label, operations);
	    source.setTransfer(types);
	    source.addDragListener(new DragSourceListener() {
	      public void dragStart(DragSourceEvent event) {
	        event.doit = (label.getText().length() != 0);
	      }

	      public void dragSetData(DragSourceEvent event) {
	        event.data = label.getText();
	      }

	      public void dragFinished(DragSourceEvent event) {
	        if (event.detail == DND.DROP_MOVE)
	          label.setText(label.getText());
	        
	      }
	    });

	    
	    DropTarget target = new DropTarget(label, operations);
	    target.setTransfer(types);
	    target.addDropListener(new DropTargetAdapter() {
	      public void drop(DropTargetEvent event) {
	        if (event.data == null) {
	          event.detail = DND.DROP_NONE;
	          return;
	        }
	        String[] split=((String)event.data).split(" ");
	        label.setText(split[0]+"\n"+split[1]);
	        if(fenetre_num==0) {
	        	Site s=(Site) Csv.recupLigne("resources/Sites.csv",-1, (String) event.data);
	        	System.out.println(event.x);
	        	System.out.println((int) Math.floor(event.x/200)-1);
		        Affectation a=new Affectation(org.recupEquipe((int) Math.floor(event.x/200)-1), s,
		        		new Heure((int) Math.floor(event.y/75)-4+current_date.getDebut_horaire().getH(),0), s.getDuree(), current_date);
		        System.out.println(a.getEquipe().getId_equipe());
		        Csv.writeInFile("resources/edt.csv", a);
		        
		        org.recupEquipe((int) Math.floor(event.x/200)-1).majContraintesIsCheck(s);
		        AfficherPlageHorraire();
	        }
	        

	      }
	    });
	  }
	
	public static void afficherBenevoleFile(Group g) {
		Path file = Paths.get("resources/benevoles.csv");
		List<String> list = null;
		String separator = ",";
		
		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if(list == null) {
			 System.out.println("Il n'y a rien dans le fichier");
		     return;
		 }
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return;
	     }
		 
		 int posY = 25;
		 
		 lab = new Label[list.size()+1];
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 final Label label = new Label(g,SWT.CENTER | SWT.BORDER | SWT.PUSH);
				label.setText(split[1]+" "+split[2]);
				label.setSize(200,25);
				label.setLocation(0,posY);
				setDragDrop(label,0);
				lab[i] = label; 
				posY += 25;
		 }
		 
		
	}
	
	public static void afficherSite() {
		groups[1].dispose();
		groups[1] = new Group(shell,SWT.FLAT);
		groups[1].setText ("SITES");
		groups[1].setSize(200, shell.getSize().y/2);
		groups[1].setLocation(0, shell.getSize().y/2+2);
		groups[1].setLayout(new FillLayout());
		
		
		
		Path file = Paths.get("resources/Sites.csv");
		List<String> list = null;
		String separator = ",";
		
		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if(list == null) {
			 System.out.println("Il n'y a rien dans le fichier");
		     return;
		 }
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return;
	     }
		 
		 int posY = 25;
		 
		 lab = new Label[list.size()+1];
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 final Label label = new Label(groups[1],SWT.CENTER | SWT.BORDER | SWT.PUSH);
				label.setText(split[0]);
				label.setSize(200,25);
				label.setLocation(0,posY);
				label.setData(new Site(split[0], Boolean.valueOf(split[1]), new Heure(Integer.valueOf(split[2].split("H")[0]),Integer.valueOf(split[2].split("H")[1])),
						split[3], split[4]));
				
				setDragDrop(label,0);
				lab[i] = label; 
				posY += 25;
		 }
		 
		
	}
	
//	public static void actualiserListBenevole(Group g) {
//		Path file = Paths.get("resources/benevoles.csv");
//		List<String> list = null;
//		String separator = ",";
//		
//		Control[] c = s.getChildren();
//		Group cg = (Group) c[0];
//		for(int i=0;i<c.length;i++) {
//			System.out.println(c[i]);
//		}
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if(list == null) {
//			 System.out.println("Il n'y a rien dans le fichier");
//		     return;
//		 }
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return;
//	     }
//		 
//		 
//		 int posY = 25;
//		 Label[] l_temp = lab;
//		 lab = new Label[list.size()+1];
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split;
//			 for(int j=0; j<l_temp.length-1;j++) {
//				 split = list.get(i).split(separator);
//				 posY++;
//				 lab[i] = l_temp[i];
//				 System.out.println(lab[j]);
//				 lab[i].setText(split[1]+" "+split[2]);
//				 i++;
//			 }
//			 split = list.get(i).split(separator);
//			 final Label label = new Label(cg,SWT.CENTER | SWT.BORDER | SWT.PUSH);
//				label.setText(split[1]+" "+split[2]);
//				label.setSize(200,25);
//				label.setLocation(0,posY);
//				setDragDrop(label);
//				lab[i] = label;
//				posY += 25;
//				System.out.println(lab[i]+" "+i);
//		 }
//		 
//		 
//	}
	
}
