package src;
/**
 * <b> Class Heure qui permet de représenter une heure. </b>
 * <p> Une heure est caractérisé par:
 * <ul>
 * <li> Un entier représentant les heures.</li>
 * <li> Un entier représentant les minutes.</li> 
 * </ul></p>
 * <p> Entre autre cette class permet de gérer et de comparer les heures
 * au format HH:MM sur un cycle de 24 heures par jours.</p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.0
 */
public class Heure{
	
	/**
	 * Le nombre d'heure de l'horaire à modéliser.
	 * 
	 * @see Heure#getH()
	 * @see Heure#setH(int)
	 * @since 1.0
	 */
	private int h;
	
	/**
	 * Le nombre de minute de l'horaire à modéliser.
	 * 
	 * @see Heure#getM()
	 * @see Heure#setM(int)
	 * @since 1.0
	 */
	private int m;
	
	/**
	 * Constructeur par defaut. Initialisation avec des valeurs faussé (-1,-1)
	 * 
	 * @see Heure#h
	 * @see Heure#m
	 * @since 1.0
	 */
	public Heure() {
		this.h=-1;
		this.m=-1;
	}
	
	/**
	 * Constructeur ou l'on choisit les valeurs d'initialisation en les passant en parametre.
	 * 
	 * @param h le nombre d'heure voulu.
	 * @param m le nombre de minute voulu.
	 * @see Heure#h
	 * @see Heure#m
	 * @since 1.0
	 */
	public Heure(int h, int m) {
		this.h=h;
		this.m=m;
	}

	/**
	 * Retourne le nombre d'heure.
	 * 
	 * @return un entier contenant le nombre d'heure.
	 * @since 1.0
	 */
	public int getH() {
		return h;
	}

	/**
	 * Met à jour l'entier representant le nombre d'heure.
	 * 
	 * @param h le nouveau nombre d'heure.
	 * @since 1.0
	 */
	public void setH(int h) {
		this.h = h;
	}

	/**
	 * Retourne le nombre de minute.
	 * 
	 * @return un entier contenant le nombre de minutes.
	 * @since 1.0
	 */
	public int getM() {
		return m;
	}
	
	/**
	 * Met à jour l'entier representant le nombre de minute.
	 * 
	 * @param m le nouveau nombre de minute.
	 * @since 1.0
	 */
	public void setM(int m) {
		this.m = m;
	}
	
	/**
	 * Fonction qui permet de donner un affichage sous le format: HH:MM.
	 * 
	 * @return la chaine de caractere au bon format.
	 * @since 1.0
	 */
	public String toString() {
		String aAfficher="";
		if((this.h>=0)&&(this.h<=9)) {
			aAfficher+="0"+this.h;
		}else {
			aAfficher+=this.h;
		}
		if((this.m>=0)&&(this.m<=9)) {
			aAfficher+="H0"+this.m;
		}else {
			aAfficher+="H"+this.m;
		}
		return aAfficher;
	}
	
	/**
	 * Fonction qui permet de savoir si deux heures sont égales.
	 * 
	 * @param other l'heure à comparer.
	 * @return true si elles sont égales false sinon.
	 * @since 1.0
	 */
	public boolean equals(Heure other) {
		return (this.h==other.getH())&&(this.m==other.getM());
	}
	
	/**
	 * Fonction qui permet de savoir si une heure est avant une autre ou non.
	 * 
	 * @param other l'heure à comparer.
	 * @return true si elle est bien avant l'autre et false sinon ou si elles sont égales.
	 * @since 1.0
	 */
	public boolean before(Heure other) {
		if(this.h<other.getH()) {
			return true;
		}else {
			if(this.h==other.getH()) {
				if(this.m<other.getM()) {
					return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}
	}
	
	/**
	 * Fonction qui permet de savoir si une heure est valide ou non.<br>
	 * Une heure valide est du type H:M avec H entre 0 et 23 ainsi que M entre 0 et 59.
	 * 
	 * @return true si l'heure est valide et false sinon.
	 * @since 1.0
	 */
	public boolean isValid() {
		return ((this.h>=0&&this.h<=23)&&(this.m>=0&&this.m<=59));
	}
	
	/**
	 * Fonction qui permet de rajouter un nombre de minutes passé en parametre à notre heure actuelle.<br>
	 * 
	 * @param m le nombre de minutes à additioner à l'heure actuelle.
	 * @return 0 si l'heure a bien pu être modifier sinon renvoi -1;
	 * @since 1.0
	 */
	public int addMinutes(int m) {
		int minute=(this.m+m)%60;
		int heure = ((this.m+m)/60)+this.h;
		Heure newH = new Heure(heure,minute);
		if(newH.isValid()) {
			this.h=newH.getH();
			this.m=newH.getM();
			return 0;
		}else {
			return -1;
		}
	}
	
}
