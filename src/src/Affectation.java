package src;

public class Affectation {
	private Equipe equipe;
	private Site site;
	private Heure heure_debut;
	private Heure duree;
	private Date date;
	
	public Affectation(Equipe e, Site s, Heure h_d, Heure du,Date d) {
		this.equipe=e;
		this.site=s;
		this.heure_debut=h_d;
		this.duree=du;
		this.date=d;
		
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public Heure getHeure_debut() {
		return heure_debut;
	}

	public void setHeure_debut(Heure heure_debut) {
		this.heure_debut = heure_debut;
	}

	public Heure getDuree() {
		return duree;
	}

	public void setDuree(Heure duree) {
		this.duree = duree;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
