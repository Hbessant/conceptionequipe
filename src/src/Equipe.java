package src;

import java.util.ArrayList;

/**
 * <b> Class Equipe qui permet de représenter une équipe. </b>
 * <p> Une équipe est caractérisé par:
 * <ul>
 * <li> Un entier représentant son id.</li>
 * <li> Un booléen représentant si il y a une compétence spéléologique dans l'équipe.</li>
 * <li> Une booléen représentant si il y a un permis.</li>
 * <li> Un booléen représentant si il y a un identificateur.</li> 
 * <li> Un booléen représentant si il y a une voiture.</li>
 * <li> Un tableau de Benevole représentant les bénévoles de l'équipe</li>
 * </ul></p>
 * <p> Entre autre cette classe permet de gérer et de comparer les équipes.</p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.0
 */
public class Equipe {
	/**
	 * L'id de l'équipe à modéliser.
	 * 
	 * @since 1.0
	 */
	private int id_equipe;
	/**
	 * Si l'équipe est spécialisé spéléo ou pas
	 * 
	 * @since 1.0
	 */
	private boolean  is_equipe_speleo;
	/**
	 * Si il y a ou non un permis dans l'équipe
	 * 
	 * @since 1.0
	 */
	private boolean permis;
	/**
	 * Si il y a ou non une voiture dans l'équipe
	 * 
	 * @since 1.0
	 */
	private boolean voiture;
	/**
	 * Tableau de benevole présents dans l'équipe
	 * 
	 * @since 1.0
	 */
	private ArrayList<Benevole> benevoles;
	
	private int[] identificateur_niveaux;
	
	private boolean contraintes_is_check;
	
	public Equipe(int id_equipe) {
		this.id_equipe=id_equipe;
		this.is_equipe_speleo=false;
		this.permis=false;
		this.voiture=false;
		this.benevoles=new ArrayList<Benevole>();
		this.identificateur_niveaux=new int[3];
		this.identificateur_niveaux[0]=0;
		this.identificateur_niveaux[1]=0;
		this.identificateur_niveaux[2]=0;
		this.contraintes_is_check=true;
	}
	
	/**
	 * Retourne l'id de l'équipe.
	 * 
	 * @return un entier représentant l'id.
	 * @since 1.0
	 */
	public int getId_equipe() {
		return id_equipe;
	}

	/**
	 * Met à jour l'entier représentant l'id de l'équipe.
	 * 
	 * @param id le nouvel id de l'équipe.
	 * @since 1.0
	 */
	public void setId_equipe(int id_equipe) {
		this.id_equipe = id_equipe;
	}


	/**
	 * Retourne true si l'équipe a une personne avec une competence speleologique.
	 * 
	 * @return un booléen qui dit si l'équipe a une personne avec une competence speleologique.
	 * @since 1.0
	 */
	public boolean get_is_equipe_speleo() {
		return is_equipe_speleo;
	}
	
	/**
	 * Met à jour le booleen représentant la possession de competence spéléologique.
	 * 
	 * @param requireCompetenceSpeleo la possession ou pas de competence spéléologique.
	 * @since 1.0
	 */
	public void set_is_equipe_speleo(boolean is_equipe_speleo) {
		this.is_equipe_speleo = is_equipe_speleo;
	}
	/**
	 * Retourne true si l'équipe a un permis.
	 * 
	 * @return un booléen qui dit si l'équipe a un permis.
	 * @since 1.0
	 */
	public boolean isPermis() {
		return permis;
	}
	/**
	 * Met à jour le booleen représentant la possession d'un permis.
	 * 
	 * @param permis la possession ou pas d'un permis.
	 * @since 1.0
	 */
	public void setPermis(boolean permis) {
		this.permis = permis;
	}
	
	/**
	 * Retourne true si l'équipe a une voiture.
	 * 
	 * @return un booléen qui dit si l'équipe a une voiture.
	 * @since 1.0
	 */
	public boolean isVoiture() {
		return voiture;
	}
	/**
	 * Met à jour le booleen représentant la possession d'une voiture.
	 * 
	 * @param voiture la possession ou pas d'une voiture.
	 * @since 1.0
	 */
	public void setVoiture(boolean voiture) {
		this.voiture = voiture;
	}
	/**
	 * Retourne tableau de Benevole présents dans l'équipe.
	 * 
	 * @return un tableau de Benevole.
	 * @since 1.0
	 */
	public ArrayList<Benevole> getBenevoles() {
		return benevoles;
	}
	/**
	 * Met à jour le tableau de Benevole.
	 * 
	 * @param benevoles le nouveau tableau de Benevole.
	 * @since 1.0
	 */
	public void setBenevoles(ArrayList<Benevole> benevoles) {
		this.benevoles = benevoles;
	}
	public int[] getIdentificateur_niveaux() {
		return identificateur_niveaux;
	}

	public void setIdentificateur_niveaux(int[] identificateur_niveaux) {
		this.identificateur_niveaux = identificateur_niveaux;
	}

	public boolean isContraintes_is_check() {
		return contraintes_is_check;
	}

	public void setContraintes_is_check(boolean contraintes_is_check) {
		this.contraintes_is_check = contraintes_is_check;
	}

	/**
	 * Fonction qui permet d'ajouter un benevole au tableau.
	 * 
	 * @param b le benevole.
	 * @since 1.0
	 */
	public void ajouterBenevole(Benevole b) {
		this.benevoles.add(b);
	}
	/**
	 * Fonction qui permet de supprimer un benevole du tableau.
	 * 
	 * @param b le benevole à supprimer.
	 * @since 1.0
	 */
	public void supprimerBenevole(Benevole b) {
		this.benevoles.remove(b);
	}
	
	public void augmenterNiveauIdentificateur(int a, int b) {
		this.identificateur_niveaux[a]+=b;
	}
	/**
	 * Fonction qui permet de savoir si une equipe sans besoin de competence speleo est valide ou non.<br>
	 * Une equipe valide contient des benevoles avec le niveau de competence speleologique 0, 1 ou 2.
	 * 
	 * @return true si l'équipe est valide et false sinon.
	 * @since 1.0
	 */
	public boolean isValidEquipeNoSpeleoRequired() {
		if(this.benevoles.size()==0) {
			return false;
		}
		for(int i=0;i<this.benevoles.size();i++) {
			int competence_benevole_en_cours=this.benevoles.get(i).getCompetenceSpeleo();
			if(competence_benevole_en_cours!=0 && competence_benevole_en_cours!=1 && competence_benevole_en_cours!=2) {
				return false;
			}
		}
		return true;
	}
	/**
	 * Fonction qui permet de savoir si une equipe ayant besoin de competence speleo est valide ou non.<br>
	 * Une equipe valide contient des benevoles avec le niveau de competence speleologique 1 ou 2 et avec minimum une competence 2 dans l'équipe.
	 * 
	 * @return true si l'équipe est valide et false sinon.
	 * @since 1.0
	 */
	public boolean isValidEquipeSpeleoRequired() {
		if(this.benevoles.size()==0) {
			return false;
		}
		boolean have_competenceLvl2=false;
		for(int i=0;i<this.benevoles.size();i++) {
			int competence_benevole_en_cours=this.benevoles.get(i).getCompetenceSpeleo();
			if(competence_benevole_en_cours!=1 && competence_benevole_en_cours!=2) {
				return false;
			}
			if(competence_benevole_en_cours==2) {
				have_competenceLvl2=true;
			}
		}
		return have_competenceLvl2;
	}
	
	public void majContraintesIsCheck(Site s) {
		if(s.isSpeleoRequis()) {
			if(!isValidEquipeSpeleoRequired()) {
				this.contraintes_is_check=false;
			}
		}
	}
	
	public void completeInformationsEquipe() {
		boolean all_sup_to_zero=true;
		boolean one_speleo_lvl_two=false;
		for(int i=0;i<this.benevoles.size();i++) {
			Benevole benevole_en_cours=this.benevoles.get(i);
			if(benevole_en_cours.getCompetenceSpeleo()==0) {
				all_sup_to_zero=false;
			}
			if(benevole_en_cours.getCompetenceSpeleo()==2) {
				one_speleo_lvl_two=true;
			}
			if(benevole_en_cours.isPermis()) {
				setPermis(true);
			}
			if(benevole_en_cours.getVoiture()!=null) {
				setVoiture(true);
			}
			switch(benevole_en_cours.getIdentificateur()) {
				case 0:
					augmenterNiveauIdentificateur(0, 1);
					break;
				case 1:
					augmenterNiveauIdentificateur(1, 1);
					break;
				case 2:
					augmenterNiveauIdentificateur(2, 1);
					break;
			}
			
		}
		if(all_sup_to_zero && one_speleo_lvl_two) {
			set_is_equipe_speleo(true);
		}else {
			set_is_equipe_speleo(false);
		}
	}
	
	/**
	 * Fonction qui permet de donner un affichage pour une équipe
	 * 
	 * @return la chaine de caractere au bon format.
	 * @since 1.0
	 */
	public String toString() {
		String ch="Equipe ";
		ch+=Integer.toString(this.id_equipe)+" :\n";
		if(this.is_equipe_speleo) {
			ch+="	Equipe spécialisé spéléo : OUI\n";
		}else {
			ch+="	Equipe spécialisé spéléo : NON\n";
		}
		if(this.permis) {
			ch+="	Permis Présent : OUI\n";
		}else {
			ch+="	Permis Présent : NON\n";
		}
		if(this.voiture) {
			ch+="	Voiture Présente : OUI\n";
		}else {
			ch+="	Voiture Présente : NON\n";
		}
		ch+="	Niveaux identificateurs : "+this.identificateur_niveaux[0]+"/"+this.identificateur_niveaux[1]+"/"+this.identificateur_niveaux[2]+"\n";
		ch+="	Liste Bénévoles :\n";
		for(int i=0;i<this.benevoles.size();i++) {
			ch+="		"+this.benevoles.get(i).toString()+"\n";
		}
		return ch;
	}
	
	public int getNbBenevole() {
		return this.benevoles.size();
	}
	
}
