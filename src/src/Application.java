//package src;
//
//public class Application {
//	public static void main(String[] args) {
//		new Fenetre(); 
//	}
//}

package src;

import org.eclipse.swt.layout.*;

public class Application {
	public static void main(String[] args){
		
		FenetreV2 f2 = new FenetreV2("Conception d'equipe"); 

		int x = f2.getDisplay().getPrimaryMonitor().getClientArea().width; 
        int y = f2.getDisplay().getPrimaryMonitor().getClientArea().height;

		String[] menu = {"Benevole","Site","Preparation","Parametres","Aides"};
		String[][] sous_menu = {{"Ajouter","Modifier","Supprimer"},{"Ajouter","Modifier","Supprimer"},{"Automatique","Manuelle"},{"Equipe","Horaire"},{"Fonctionalités","Créateurs","Quitter\tCtrl+Q"}};
		f2.AjouterMenu(menu,sous_menu);

		f2.setLayout(new FormLayout(), f2.getShell());
		
		f2.AjouterScrolledComposite(f2.getShell());
		f2.AjouterScrolledComposite(f2.getShell());
		f2.AjouterScrolledComposite(f2.getShell());
		f2.AjouterScrolledComposite(f2.getShell());
		f2.AjouterComposite(0);
		f2.AjouterComposite(1);
		f2.AjouterComposite(2);
		f2.AjouterComposite(3); 

		
		//haut gauche
		FormData data1= new FormData();
		data1.left = new FormAttachment(0,1);
		data1.right = new FormAttachment((int)(x*0.007),0);
		data1.top = new FormAttachment(0,1);
		data1.bottom = new FormAttachment((int)(y*0.05),0);
		f2.getSc().get(0).setLayoutData(data1);
		
		//f_annexe.AjouterEventListenerScrolled(f2.getSc().get(0), f2.getC().get(0));
		//haut droite
		FormData data2= new FormData();
		data2.left = new FormAttachment(f2.getSc().get(0),1);
		data2.right = new FormAttachment((int)(x*0.09),-1);
		data2.top = new FormAttachment(0,1);

		f2.getSc().get(1).setLayoutData(data2);
		//bas gauche
		FormData data3= new FormData();
		data3.left = new FormAttachment(0,1);
		data3.right = new FormAttachment((int)(x*0.007),0);
		data3.top = new FormAttachment(f2.getSc().get(0),1);
		data3.bottom = new FormAttachment((int)(y*0.093),0);
		f2.getSc().get(2).setLayoutData(data3);
		//f_annexe.AjouterEventListenerScrolled(f2.getSc().get(2), f2.getC().get(2));
		
		//bas droite
		FormData data4= new FormData();
		data4.left = new FormAttachment(f2.getSc().get(2),1);
		data4.right = new FormAttachment((int)(x*0.0525),-1);
		data4.top = new FormAttachment(f2.getSc().get(1),1);
		data4.bottom = new FormAttachment((int)(y*0.093),0);
		f2.getSc().get(3).setLayoutData(data4);
		//f_annexe.AjouterEventListenerScrolled(f2.getSc().get(3), f2.getC().get(3));
		
		f2.addFillLayout(f2.getSc().get(2) );
		f2.afficherDate(f2.getC().get(1));
		f2.afficherSite(f2.getC().get(2));
		f2.afficherPlageHorraire(f2.getC().get(3),null,null);
		f2.afficherEquipes(f2.getC().get(0));
		f2.DisposeElement(f2.getShell());
	}
}