package src;
/**
 * <b> Class Contrainte qui permet de représenter une contrainte. </b>
 * <p> Une contrainte est caractérisé par:
 * <ul>
 * <li> Une Date.</li>
 * <li> Une Heure représentant l'heure d'arrivée.</li>
 * <li> Une Heure représentant l'heure de départ.</li> 
 * </ul></p>
 * <p> Entre autre cette class permet de gérer et de comparer les contraintes.</p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.0
 */
public class Contrainte {
	/**
	 * La date de la contrainte à modéliser.
	 * 
	 * @since 1.0
	 */
	private Date d;
	/**
	 * L'heure de d'arrivée de la contrainte à modéliser.
	 * 
	 * @since 1.0
	 */
	private Heure h1;
	/**
	 * L'heure de départ de la contrainte à modéliser.
	 * 
	 * @since 1.0
	 */
	private Heure h2;
	
	public Contrainte(Date d,Heure h1, Heure h2) {
		this.d=d;
		this.h1=h1;
		this.h2=h2;
	}
	/**
	 * Retourne la date de la contrainte.
	 * 
	 * @return une Date.
	 * @since 1.0
	 */
	public Date getD() {
		return d;
	}
	/**
	 * Met à jour la Date représentant la date de la contrainte.
	 * 
	 * @param d la nouvelle date de la contrainte.
	 * @since 1.0
	 */
	public void setD(Date d) {
		this.d = d;
	}
	/**
	 * Retourne l'heure d'arrivée de la contrainte.
	 * 
	 * @return une Heure.
	 * @since 1.0
	 */
	public Heure getH1() {
		return h1;
	}
	/**
	 * Met à jour l'Heure représentant l'heure d'arrivée de la contrainte.
	 * 
	 * @param h1 la nouvelle heure d'arrivée de la contrainte.
	 * @since 1.0
	 */
	public void setH1(Heure h1) {
		this.h1 = h1;
	}
	/**
	 * Retourne l'heure de départ de la contrainte.
	 * 
	 * @return une Heure.
	 * @since 1.0
	 */
	public Heure getH2() {
		return h2;
	}
	/**
	 * Met à jour l'Heure représentant l'heure de départ de la contrainte.
	 * 
	 * @param h2 la nouvelle heure de départ de la contrainte.
	 * @since 1.0
	 */
	public void setH2(Heure h2) {
		this.h2 = h2;
	}
	/**
	 * Fonction qui permet de donner un affichage sous le format: Disponible le JJ/MM/AAAA de HH:MM à HH:MM.
	 * 
	 * @return la chaine de caractere au bon format.
	 * @since 1.0
	 */
	public String toString() {
		String ch="Disponible le ";
		ch+=this.d.toString()+" de "+this.h1.toString()+" à "+this.h2.toString();
		return ch;
	}
	
}
