package src;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

/**
 * <b> Class Benevole qui permet de représenter un bénévole. </b>
 * <p> Un bénévole est caractérisé par:
 * <ul>
 * <li> Un entier représentant son id.</li>
 * <li> Une chaîne de caractère représentant son nom.</li>
 * <li> Une chaîne de caractère représentant son prénom.</li>
 * <li> Un booléen représentant si il a le permis.</li> 
 * <li> Un entier représentant son niveau de compétence spéléologique.</li>
 * <li> Un booléen représentant si il a la compétence identificateur ou pas.</li> 
 * <li> Un tableau de Contrainte représentant les contraintes d'un bénévole.</li> 
 * </ul></p>
 * <p> Entre autre cette class permet de gérer et de comparer les bénévoles.</p>
 * 
 * @author Hbessant
 * @author Helchida
 * @author NyraWax
 *
 * @version 1.0
 */
public class Benevole {
	/**
	 * L'id du bénévole à modéliser.
	 * 
	 * @since 1.0
	 */
	private int id;
	/**
	 * Le nom du bénévole à modéliser.
	 * 
	 * @since 1.0
	 */
	private String nom;
	/**
	 * Le prénom du bénévole à modéliser.
	 * 
	 * @since 1.0
	 */
	private String prenom;
	/**
	 * Le permis du bénévole si il l'a ou pas.
	 * 
	 * @since 1.0
	 */
	private boolean permis;
	/**
	 * Le niveau de compétence spéléologique du bénévole.
	 * 
	 * @since 1.0
	 */
	private int competenceSpeleo;
	/**
	 * La compétence identificateur du bénévole si il l'a ou pas.
	 * 
	 * @since 1.0
	 */
	private int identificateur;
	/**
	 * Tableau de contraintes d'un bénévole.
	 * 
	 * @since 1.0
	 */
	private ArrayList<Contrainte> contraintes;
	/**
	 * Voiture d'un bénévole, si voiture avec 0 places alors pas de voiture.
	 * 
	 * @since 1.0
	 */
	private Voiture voiture;
	
	private final static String FILE_NAME = "resources/benevoles.csv";
	private static File FILE = Csv.getResource(FILE_NAME);
	
	
	/**
	 * Constructeur par defaut. Initialisation avec des valeurs vides ou à 0
	 * 
	 * @since 1.0
	 */
	public Benevole() {
		this.id=-1;
		this.nom="";
		this.prenom="";
		this.permis=false;
		this.competenceSpeleo=-1;
		this.identificateur=-1;
		this.contraintes=new ArrayList<Contrainte>();
		this.voiture=new Voiture(4,"FS500DS","Peugeot 206");
		
		if(!(FILE.exists())) {
			FILE = new File(FILE_NAME);
		}
		
		Path file = Paths.get(FILE_NAME);
		try {
			Files.write(file, (id+","+nom+","+prenom+","+permis+","+competenceSpeleo+","+identificateur+"\n").getBytes(),StandardOpenOption.APPEND);
		} catch (IOException e) {
			System.out.println("une erreur est survenue");
			return;
		}
	}
	/**
	 * Constructeur. Initialisation avec des valeurs nom prenom et le reste à 0 ou faux
	 * 
	 * @since 1.0
	 */
	public Benevole(int id, String nom,String prenom,Voiture voiture) {
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.permis=false;
		this.competenceSpeleo=0;
		this.identificateur=-1;
		this.contraintes=new ArrayList<Contrainte>();
		if(voiture!=null) {
			this.voiture=voiture;
		}else {
			this.voiture=null;
		}
		
		if(!(FILE.exists())) {
			FILE = new File(FILE_NAME);
		}
		
		Path file = Paths.get(FILE_NAME);
		try {
			Files.write(file, (id+","+nom+","+prenom+","+permis+","+competenceSpeleo+","+identificateur+"\n").getBytes(),StandardOpenOption.APPEND);
		} catch (IOException e) {
			System.out.println("une erreur est survenue");
			return;
		}
	}
	/**
	 * Constructeur. Initialisation avec des valeurs nom, prenom, permis, competenceSpeleo, identificateur
	 * 
	 * @since 1.0
	 */
	public Benevole(int id, String nom,String prenom,boolean permis, int competenceSpeleo, int identificateur, Voiture voiture) {
		this.id=id;
		this.nom=nom;
		this.prenom=prenom;
		this.permis=permis;
		this.competenceSpeleo=competenceSpeleo;
		this.identificateur=identificateur;
		this.contraintes=new ArrayList<Contrainte>();
		if(voiture!=null) {
			this.voiture=voiture;
		}else {
			this.voiture=null;
		}
		
		if(!(FILE.exists())) {
			FILE = new File(FILE_NAME);
		}
		
	
	}
	
	
	/**
	 * Retourne l'id du bénévole.
	 * 
	 * @return un entier représentant l'id.
	 * @since 1.0
	 */
	public int getId() {
		return id;
	}
	/**
	 * Met à jour l'entier représentant l'id du bénévole.
	 * 
	 * @param id le nouvel id du bénévole.
	 * @since 1.0
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Retourne le nom du bénévole.
	 * 
	 * @return une chaine de caractere représentant le nom.
	 * @since 1.0
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Met à jour le string représentant le nom du bénévole.
	 * 
	 * @param nom le nouveau nom du bénévole.
	 * @since 1.0
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Retourne le prénom du bénévole.
	 * 
	 * @return une chaine de caractere représentant le prénom.
	 * @since 1.0
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Met à jour le string représentant le prénom du bénévole.
	 * 
	 * @param prenom le nouveau prénom du bénévole.
	 * @since 1.0
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * Retourne true si le bénévole a le permis.
	 * 
	 * @return un booléen qui dit si le bénévole a le permis ou pas.
	 * @since 1.0
	 */
	public boolean isPermis() {
		return permis;
	}
	/**
	 * Met à jour le booleen représentant si le bénévol a le permis.
	 * 
	 * @param permis la possession ou pas du permis.
	 * @since 1.0
	 */
	public void setPermis(boolean permis) {
		this.permis = permis;
	}
	/**
	 * Retourne le niveau de compétence spéléologique d'un bénévole.
	 * 
	 * @return un entier représentant le niveau de compétence spéléologique.
	 * @since 1.0
	 */
	public int getCompetenceSpeleo() {
		return competenceSpeleo;
	}
	/**
	 * Met à jour l'entier représentant le niveau de compétence spéléologique du benevole.
	 * 
	 * @param competenceSpeleo le niveau de compétence.
	 * @since 1.0
	 */
	public void setCompetenceSpeleo(int competenceSpeleo) {
		this.competenceSpeleo = competenceSpeleo;
	}
	/**
	 * Retourne true si le bénévole est identificateur.
	 * 
	 * @return un booléen qui dit si le bénévole est identificateur ou pas.
	 * @since 1.0
	 */
	public int getIdentificateur() {
		return identificateur;
	}
	/**
	 * Met à jour le booleen représentant si le bénévol est identificateur ou pas.
	 * 
	 * @param identificateur est identificateur ou pas.
	 * @since 1.0
	 */
	public void setIdentificateur(int identificateur) {
		this.identificateur = identificateur;
	}
	/**
	 * Retourne le tableau de contrainte du bénévole.
	 * 
	 * @return un tableau de Contrainte.
	 * @since 1.0
	 */
	public ArrayList<Contrainte> getContraintes() {
		return contraintes;
	}
	/**
	 * Met à jour le tableau de Contrainte du bénévole.
	 * 
	 * @param contraintes le nouveau tableau de Contrainte.
	 * @since 1.0
	 */
	public void setContraintes(ArrayList<Contrainte> contraintes) {
		this.contraintes = contraintes;
	}
	/**
	 * Retourne la voiture du bénévole.
	 * 
	 * @return une voiture.
	 * @since 1.0
	 */
	public Voiture getVoiture() {
		return voiture;
	}
	/**
	 * Met à jour la voiture du bénévole.
	 * 
	 * @param voiture la nouvelle voiture du bénévole.
	 * @since 1.0
	 */
	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}
	/**
	 * Fonction qui permet d'ajouter une contrainte au tableau.
	 * 
	 * @param c la contrainte.
	 * @since 1.0
	 */
	public void ajouterContrainte(Contrainte c) {
		this.contraintes.add(c);
	}
	/**
	 * Fonction qui permet de supprimer une contrainte du tableau.
	 * 
	 * @param c la contrainte à supprimer.
	 * @since 1.0
	 */
	public void supprimerContrainte(Contrainte c) {
		this.contraintes.remove(c);
	}
	/**
	 * Fonction qui permet de donner un affichage pour un bénévole
	 * 
	 * @return la chaine de caractere au bon format.
	 * @since 1.0
	 */
	public String toString() {
		String ch="Bénévole ";
		ch+=Integer.toString(this.id)+" :\n";
		ch+="			Nom : "+this.nom+"\n";
		ch+="			Prénom : "+this.prenom+"\n";
		if(this.permis) {
			ch+="			Permis : OUI\n";
		}else {
			ch+="			Permis : NON\n";
		}
		ch+="			Niveau Compétence Spéléologique : "+Integer.toString(this.competenceSpeleo)+"\n";
		ch+= "			Niveau identificateur : " + Integer.toString(this.identificateur)+"\n";
		if(this.voiture==null) {
			ch+="			Voiture : NON\n";
		}else {
			ch+="			Voiture : OUI\n";	
			ch+=this.voiture.toString();
		}
		if(this.contraintes.size()==0) {
			ch+="			Contraintes : AUCUNE\n";
		}else {
			ch+="			Contraintes :\n";
			for(int i=0;i<this.contraintes.size();i++) {
				ch+="				"+this.contraintes.get(i).toString()+"\n";
			}
		}
		return ch;
	}
	

}
