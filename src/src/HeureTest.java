package src;

import static org.junit.Assert.*;

import org.junit.Test;

public class HeureTest {
	/**
	 * ENSEMBLE DES TESTS DE LA METHODE toString
	 */
	
	@Test
	public void testToStringSimple() {
		Heure h1 = new Heure(12,35);
		assertEquals("12:35",h1.toString());
	}
	
	@Test
	public void testToStringNegatif() {
		Heure h1 = new Heure();
		assertEquals("-1:-1",h1.toString());
	}
	
	@Test
	public void testToStringComplexe() {
		Heure h1 = new Heure(6,9);
		assertEquals("06:09",h1.toString());
	}
	
	/**
	 * ENSEMBLE DES TESTS DE LA METHODE before
	 */
	
	@Test
	public void testBeforeSimple1() {
		Heure h1 = new Heure(6,9);
		Heure h2 = new Heure(10,9);
		assertTrue(h1.before(h2));
	}
	
	@Test
	public void testBeforeSimple2() {
		Heure h1 = new Heure(6,9);
		Heure h2 = new Heure(6,12);
		assertTrue(h1.before(h2));
	}
	
	@Test
	public void testBeforeSimple3() {
		Heure h1 = new Heure(6,9);
		Heure h2 = new Heure(6,9);
		assertFalse(h1.before(h2));
	}
	
	@Test
	public void testBeforeSimple4() {
		Heure h1 = new Heure(6,9);
		Heure h2 = new Heure(3,9);
		assertFalse(h1.before(h2));
	}
	
	/**
	 * ENSEMBLE DES TESTS DE LA METHODE addMinutes
	 */
	
	@Test
	public void testAddMinutesSimple1() {
		Heure h1 = new Heure(6,9);
		assertEquals(0,h1.addMinutes(1));
		assertEquals(6,h1.getH());
		assertEquals(10,h1.getM());
	}
	
	@Test
	public void testAddMinutesSimple2() {
		Heure h1 = new Heure(6,9);
		assertEquals(0,h1.addMinutes(61));
		assertEquals(7,h1.getH());
		assertEquals(10,h1.getM());
	}
	
	@Test
	public void testAddMinutesComplexe() {
		Heure h1 = new Heure(6,9);
		assertEquals(-1,h1.addMinutes(6000));
		assertEquals(6,h1.getH());
		assertEquals(9,h1.getM());
	}
	
	/**
	 * ENSEMBLE DES TESTS DE LA METHODE equals
	 */
	
	@Test
	public void testEqualsSimple1() {
		Heure h1 = new Heure(6,9);
		Heure h2 = new Heure();
		h2.setH(6);
		h2.setM(9);
		assertTrue(h1.equals(h2));
	}
	
	@Test
	public void testEqualsSimple2() {
		Heure h1 = new Heure(6,9);
		Heure h2 = new Heure(7,10);
		assertFalse(h1.equals(h2));
	}
	
	
	
}
