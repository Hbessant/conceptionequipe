//package src;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.nio.file.StandardOpenOption;
//import java.util.ArrayList;
//import java.util.List;
//
//
//public class Csv {
//	
//	private static String separator = ",";
//	
//	public static String getResourcePath(String fileName) {
//	       final File f = new File("");
//	       final String dossierPath = f.getAbsolutePath() + File.separator + fileName;
//	       return dossierPath; 
//	  }
//	
//	public static File getResource(String fileName) {
//	       final String completeFileName = getResourcePath(fileName);
//	       File file = new File(completeFileName);
//	       return file;
//	   }
//	
//	public static void writeInFile(String fileName,int id, String nom,boolean permis, boolean speleo, boolean identif) {
//		Path file = Paths.get(fileName);
//		try {
//			Files.write(file, (id+","+nom+","+permis+","+speleo+","+identif+"\n").getBytes(),StandardOpenOption.APPEND);
//		} catch (IOException e) {
//			System.out.println("une erreur est survenue");
//			return;
//		} 
//	}
//	
//	public static void readInFile(String fileName) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return;
//	     }
//		 
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 int id = Integer.valueOf(split[0]);
//			 String nom = String.valueOf(split[1]);
//			 boolean permis = Boolean.valueOf(split[2]);
//			 Boolean speleo = Boolean.valueOf(split[3]);
//			 Boolean iden = Boolean.valueOf(split[4]);
//			 
//			 System.out.println("id : "+id+" nom : "+nom+" permis : "+permis+" spel : "+speleo+" iden : "+iden);
//		 }
//	}
//	
//	public static int recupSizeList(String fileName) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return 0;
//	     }
//		 
//		 return list.size();
//	}
//	
//	public static String[] recupOneRow(String fileName,int id) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return null;
//	     }
//		 
//		 //System.out.println(list.size());
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(Integer.valueOf(split[0]) == id) {
//				 return split;
//			 } 
//			 
//		 }
//		return null;
//	}
//	
//	public static Benevole recupLigne(String fileName,int ligne) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return null;
//	     }
//		 
//		 Benevole b;
//		 
//		 //System.out.println(list.size());
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(i == ligne) {
//				 b=new Benevole(Integer.parseInt(split[0]), split[1], split[2], Boolean.valueOf(split[3]).booleanValue(),
//						 Integer.parseInt(split[4]), Integer.parseInt(split[5]),Integer.parseInt(split[6]));
//				 return b;
//			 } 
//			 
//		 }
//		return null;
//	}
//	
//	public static Site recupLigneSite(String fileName,int ligne) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return null;
//	     }
//		 
//		 Site s;
//		 
//		 //System.out.println(list.size());
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(i == ligne) {
//				 String[] split2=split[2].split("H");
//				 s=new Site(split[0], Boolean.valueOf(split[1]), new Heure(Integer.valueOf(split2[0]),Integer.valueOf(split2[1])), split[3],split[4]);
//				 return s;
//			 } 
//			 
//		 }
//		return null;
//	}
//	
//	public static File deleteInFile(String fileName, int id, File fileB) {
//		String nameNewFile = "src/resources/CsvCopy.csv";
//		File newFile = new File(nameNewFile);
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		
//		try {
//			if(newFile.createNewFile()) {
//				System.out.println("fichier creer");
//			}
//		} catch (IOException e1) {
//			System.out.println("fichier non creer");
//			return null;
//			
//		}
//		
//		try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		
//		for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if((Integer.valueOf(split[0]) == id)) {
//				
//			 }else {
//				 try {
//					Files.write(Paths.get(nameNewFile), (split[0]+","+split[1]+","+split[2]+","+split[3]
//							+","+split[4]+","+split[5]+"\n").getBytes(),StandardOpenOption.APPEND);
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			 }
//		 }
//		
//		
//		fileB.delete();
//		
//		if(newFile.renameTo(new File(fileName))) {
//			System.out.println("File bien renomé");
//		}else {
//			System.out.println("fichier non renomé !");
//		}
//		
//		return newFile;
//
//	}
//	
//	public static void deleteLigne(String fileName, int line) {
//		String nameNewFile = "resources/Copy.csv";
//		File newFile = new File(nameNewFile);
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		
//		try {
//			if(newFile.createNewFile()) {
//			
//			}
//		} catch (IOException e1) {
//			System.out.println("fichier non creer");
//			return;
//			
//		}
//		
//		try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		
//		for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(i == line) {
//				 
//			 }else {
//				 try {
//						Files.write(Paths.get(nameNewFile), (split[0]+","+split[1]+","+split[2]+","+split[3]+","+split[4]
//								+","+split[5]+","+split[6]+"\n").getBytes(),StandardOpenOption.APPEND);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//			 }
//			 
//		 }
//		
//		
//		Csv.getResource(fileName).delete();
//		
//		if(newFile.renameTo(new File(fileName))) {
//			
//		}else {
//			
//		}
//
//	}
//	
//	public static void deleteLigneSite(String fileName, int line) {
//		String nameNewFile = "resources/Copy.csv";
//		File newFile = new File(nameNewFile);
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		
//		try {
//			if(newFile.createNewFile()) {
//			
//			}
//		} catch (IOException e1) {
//			System.out.println("fichier non creer");
//			return;
//			
//		}
//		
//		try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		
//		for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(i == line) {
//				 
//			 }else {
//				 try {
//						Files.write(Paths.get(nameNewFile), (split[0]+","+split[1]+","+split[2]+","+split[3]+","+split[4]
//								+"\n").getBytes(),StandardOpenOption.APPEND);
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//			 }
//			 
//		 }
//		
//		
//		Csv.getResource(fileName).delete();
//		
//		if(newFile.renameTo(new File(fileName))) {
//			
//		}else {
//			
//		}
//
//	}
//	
//	public static void writeBenevoleInFile(String fileName,Benevole b) {
//		Path file = Paths.get(fileName);
//		try {
//			if(b.getVoiture()==null) {
//				Files.write(file, (b.getId()+","+b.getNom()+","+b.getPrenom()+","+b.isPermis()+","+b.getCompetenceSpeleo()
//				+","+b.getIdentificateur()+","+0+"\n").getBytes(),StandardOpenOption.APPEND);
//			}else {
//				Files.write(file, (b.getId()+","+b.getNom()+","+b.getPrenom()+","+b.isPermis()+","+b.getCompetenceSpeleo()
//				+","+b.getIdentificateur()+","+b.getVoiture().getNbPlace()+"\n").getBytes(),StandardOpenOption.APPEND);
//			}
//			
//		} catch (IOException e) {
//			System.out.println("une erreur est survenue");
//			return;
//		} 
//	}
//	
//	public static void modifierCsvBenevole (List<Benevole> liste_bene) {
//		String nameNewFile = "resources/Copy.csv";
//		File newFile = new File(nameNewFile);
//		Path file = Paths.get("resources/benevoles.csv");
//		List<String> list = null;
//		
//		try {
//			if(newFile.createNewFile()) {
//			
//			}
//		} catch (IOException e1) {
//			System.out.println("fichier non creer");
//			return;
//			
//		}
//		
//		try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		
//		for(int i=0;i<liste_bene.size();i++) {
//			writeBenevoleInFile(nameNewFile, liste_bene.get(i));
//		}
//		
//		Csv.getResource("resources/benevoles.csv").delete();
//		
//		if(newFile.renameTo(new File("resources/benevoles.csv"))) {
//			
//		}else {
//			
//		}
//	}
//	
//	public static void modifierCsvSite (List<Site> liste_site) {
//		String nameNewFile = "resources/Copy.csv";
//		File newFile = new File(nameNewFile);
//		Path file = Paths.get("resources/Sites.csv");
//		List<String> list = null;
//		
//		try {
//			if(newFile.createNewFile()) {
//			
//			}
//		} catch (IOException e1) {
//			System.out.println("fichier non creer");
//			return;
//			
//		}
//		
//		try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		
//		for(int i=0;i<liste_site.size();i++) {
//			writeSiteInFile(nameNewFile, liste_site.get(i));
//		}
//		
//		Csv.getResource("resources/Sites.csv").delete();
//		
//		if(newFile.renameTo(new File("resources/Sites.csv"))) {
//			
//		}else {
//			
//		}
//	}
//	public static void writeSiteInFile(String fileName,Site s) {
//		Path file = Paths.get(fileName);
//		try {
//			Files.write(file, (s.getNom()+","+s.isSpeleoRequis()+","+s.getDuree()+","+s.getCoordonnees()+","+s.getEquipementRequis()+"\n").getBytes(),StandardOpenOption.APPEND);
//							
//		} catch (IOException e) {
//			System.out.println("une erreur est survenue");
//			return;
//		} 
//	}
//	
////	public static void writeSiteInFile(String fileName,Site s) {
////		Path file = Paths.get(fileName);
////		try {
////		    Files.write(file, (s.getNom()+"\n").getBytes(),StandardOpenOption.APPEND);
////		} catch (IOException e) {
////			System.out.println("une erreur est survenue");
////			return;
////		} 
////	}
//	
//	public static int RecupidMax(String fileName) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return -1;
//	     }
//		 
//		 int max = 0;
//		 
//		 //System.out.println(list.size());
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(max < Integer.parseInt(split[0])) {
//				max = Integer.parseInt(split[0]);
//				
//			 } 
//			 
//		 }
//		return max;
//		
//	}
//	
//	public static void deleteAllInFile(String fileName, File fileB) {
//		String nameNewFile = "src/resources/CsvCopy.csv";
//		File newFile = new File(nameNewFile);
//		try {
//			if(newFile.createNewFile()) {
//				System.out.println("fichier creer");
//			}
//		} catch (IOException e1) {
//			System.out.println("fichier non creer");
//			return;
//		}
//		fileB.delete();
//		newFile.renameTo(new File(fileName));
//		System.out.println("contenue du fichier suprimer !");
//		
//
//	}
//	
//	public static List<Benevole> recupListeBenevole(String fileName) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		List<Benevole> res=new ArrayList<Benevole>();
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return null;
//	     }
//		 		 
//		 
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 //System.out.println("date : " + date +" date_csv : "+split[4]);
//			 
//			 res.add(new Benevole(Integer.valueOf(split[0]),split[1],split[2],Boolean.valueOf(split[3])
//					 ,Integer.valueOf(split[4]),Integer.valueOf(split[5]),Integer.valueOf(split[6])));
//			 
//			 
//		 }
//		return res;
//	}
//	
//	public static List<String> recupTabLigneEdt(String fileName,String date) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//		List<String> res=new ArrayList<String>();
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return null;
//	     }
//		 		 
//		 
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 //System.out.println("date : " + date +" date_csv : "+split[4]);
//			 if(date.equals(split[4])){
//				 res.add(list.get(i));
//			 } 
//			 
//		 }
//		return res;
//	}
//	
//	public static void writeAffectationInFile(String fileName, Affectation a) {
//		Path file = Paths.get(fileName);
//		try {
//			Files.write(file, ("Equipe "+a.getEquipe().getId_equipe()+","+a.getSite().getNom()+","+a.getHeure_debut().toString()+","
//						+a.getDuree().toString()+","+a.getDate().toString()+"\n").getBytes(),StandardOpenOption.APPEND);
//			
//		} catch (IOException e) {
//			System.out.println("une erreur est survenue");
//			return;
//		} 
//	}
//	
//	public static Site readSiteInFile(String fileName, String nom_site) {
//		Path file = Paths.get(fileName);
//		List<String> list = null;
//
//		
//		 try {
//			 list = Files.readAllLines(file);
//		} catch (IOException e) {
//	    	 System.out.println("Impossible de lire le fichier");
//	    }
//		 
//		 if (list.size() < 1) {
//	        System.out.println("Il n'y a rien dans le fichier");
//	        return null;
//	     }
//		 
//		 
//		 
//		 for (int i = 0; i < list.size(); i++) {
//			 String[] split = list.get(i).split(separator);
//			 if(split[0].equals(nom_site)) {
//				return new Site(split[0], Boolean.valueOf(split[1]), 
//						new Heure(Integer.valueOf(split[2].split("H")[0]),Integer.valueOf(split[2].split("H")[1])), split[3], split[4]);
//				
//			 } 
//			 
//		 }
//		return null;
//	}
//}


package src;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Csv {
	private static String separator = ",";
	
	public static String getResourcePath(String fileName) {
	       final File f = new File("");
	       final String dossierPath = f.getAbsolutePath() + File.separator + fileName;
	       return dossierPath; 
	  }
	
	public static File getResource(String fileName) {
	       final String completeFileName = getResourcePath(fileName);
	       File file = new File(completeFileName);
	       return file;
	   }
	
	public static int recupSizeList(String fileName) {
		Path file = Paths.get(fileName);
		List<String> list = null;
		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return 0;
	     }
		 
		 return list.size();
	}
	
	public static int RecupidMax(String fileName) {
		Path file = Paths.get(fileName);
		List<String> list = null;

		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return -1;
	     }
		 
		 int max = 0;
		 
		 //System.out.println(list.size());
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 if(max < Integer.parseInt(split[0])) {
				max = Integer.parseInt(split[0]);
				
			 } 
			 
		 }
		return max;
		
	}
	
	public static Object recupLigne(String fileName,int ligne, String dateOrName) {
	
		Path file = Paths.get(fileName);
		List<String> list = null;
		List<String> res=new ArrayList<String>();

		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return null;
	     }
		 
		 Benevole b;
		 String name = ((fileName.split("/"))[1]);
		 
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 if(name.equals("benevoles.csv")) {
				 if(Integer.parseInt(split[0]) == ligne) {
					 if(Integer.valueOf(split[6])==0) {
						 b=new Benevole(Integer.parseInt(split[0]), split[1], split[2], Boolean.valueOf(split[3]).booleanValue(),
								 Integer.parseInt(split[4]), Integer.parseInt(split[5]),null);
					 }else {
						 b=new Benevole(Integer.valueOf(split[0]),split[1],split[2],Boolean.valueOf(split[3])
								 ,Integer.valueOf(split[4]),Integer.valueOf(split[5]),(Voiture)recupLigne("resources/voitures.csv",-1,split[0]));
					 }
					 
					
					 return b;
				 } 
			 }else if(name.equals("edt.csv")) {
				 if(dateOrName.equals(split[4])){
					 res.add(list.get(i));
				 } 
			 }else if(name.equals("Sites.csv")) {
				 if(i==ligne) {
						return new Site(split[0], Boolean.valueOf(split[1]), 
								new Heure(Integer.valueOf(split[2].split("H")[0]),Integer.valueOf(split[2].split("H")[1])), split[3], split[4]);
						
					 }else {
						 if(dateOrName.equals(split[0])) {
							 return new Site(split[0], Boolean.valueOf(split[1]), 
										new Heure(Integer.valueOf(split[2].split("H")[0]),Integer.valueOf(split[2].split("H")[1])), split[3], split[4]);
						 }
					 }
			 }else if(name.equals("voitures.csv")) {
				 if(dateOrName.equals(split[0])) {
					 return new Voiture(Integer.valueOf(split[3]),split[4],split[5]);
				 }
			 }else if(name.equals("parametre.csv")) {	
				 if(i==ligne || dateOrName.equals(split[0])) {
					 if(ligne==0) {
						 return Integer.valueOf(split[1]);
					 }
					 if(ligne==1) {
						 res.add(split[1]);
					 }
					 if(dateOrName.equals(split[0])) {
						 
						 res.add(split[1]);
						 res.add(split[2]);
					 }
				 }
				 
			 }else if(name.equals("parametre.csv")) {
				 if(dateOrName.equals(split[0])) {
					 return split[1];
				 }
			 }else if(name.equals("equipes.csv")) {
				 if(i==ligne) {
					 for(int j=0;j<split.length;j++) {
						 if(!split[j].equals("")) {
							 res.add(split[j]);
						 } 
					 }
					 return res;
				 }
			 }
			 
			 
		 }
		 
		 if(name.equals("edt") || name.equals("parametre.csv")) {
			 return res;
		 }
		return null;
	}
	
	public static List<Object> recupListe(String fileName, String dateOrName) {
		Path file = Paths.get(fileName);
		List<String> list = null;
		List<Object> res=new ArrayList<Object>();
		System.out.println((fileName.split("/"))[1]);
		String name = ((fileName.split("/"))[1]);
		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return null;
	     }
		 		 
		 
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 if(name.equals("benevoles.csv")) {
				 if(Integer.valueOf(split[6])==0) {
					 res.add(new Benevole(Integer.valueOf(split[0]),split[1],split[2],Boolean.valueOf(split[3])
							 ,Integer.valueOf(split[4]),Integer.valueOf(split[5]),null));
				 }else {
					 res.add(new Benevole(Integer.valueOf(split[0]),split[1],split[2],Boolean.valueOf(split[3])
							 ,Integer.valueOf(split[4]),Integer.valueOf(split[5]),(Voiture)recupLigne("resources/voitures.csv",-1,split[0])));
				 }
				 
				 
				 
				 
			 }else if(name.equals("edt.csv")) {
				 if(dateOrName.equals(split[4])){
					 res.add(list.get(i));
				 } 
			 }else if(name.equals("Sites.csv")) {
				
					 res.add(new Site(split[0], Boolean.valueOf(split[1]), 
								new Heure(Integer.valueOf(split[2].split("H")[0]),Integer.valueOf(split[2].split("H")[1])), split[3], split[4]));
						
				
			 }else if(name.equals("voiture.csv")) {
				 res.add(new Voiture(Integer.valueOf(split[3]),split[4],split[5]));
			 }else if(name.equals("equipes.csv")) { 	
				res.add(Csv.recupLigne("resources/equipes.csv", i, ""));
				    
			 }
			 
			 
		 }
		return res;
	}
	
	public static void writeInFile(String fileName,Object o) {
		Path file = Paths.get(fileName);
		String name = ((fileName.split("/"))[1]);
		Benevole b = null;
		Affectation a = null;
		Site s=null;
		Organisation org=null;
		
		
		if(name.equals("benevoles.csv") || name.equals("BenevoleCopy.csv")) {
			 b = (Benevole) o;
		 }else if(name.equals("edt.csv")) {
			 a = (Affectation) o;
		 }else if(name.equals("Sites.csv") || name.equals("SitesCopy.csv")) {
			 s =(Site) o;
		 }else if(name.equals("voitures.csv") || name.equals("VoituresCopy.csv")) {
			 b =(Benevole) o;
		 }else if(name.equals("contraintes.csv") || name.equals("ContraintesCopy.csv")) {
			 b =(Benevole) o;
		 }else if(name.equals("equipes.csv") || name.equals("EquipesCopy.csv")){
			 org=(Organisation) o;
		 }
		 
		try {
			if((name.equals("benevoles.csv") || name.equals("BenevoleCopy.csv")) && b != null) {
				if(b.getVoiture()==null) {
					Files.write(file, (b.getId()+","+b.getNom()+","+b.getPrenom()+","+b.isPermis()+","+b.getCompetenceSpeleo()
					+","+b.getIdentificateur()+","+0+"\n").getBytes(),StandardOpenOption.APPEND);
				}else {
					Files.write(file, (b.getId()+","+b.getNom()+","+b.getPrenom()+","+b.isPermis()+","+b.getCompetenceSpeleo()
					+","+b.getIdentificateur()+","+b.getVoiture().getNbPlace()+"\n").getBytes(),StandardOpenOption.APPEND);
				}
			 }else if(name.equals("edt.csv") && a != null) {
				 Files.write(file, ("Equipe "+a.getEquipe().getId_equipe()+","+a.getSite().getNom()+","+a.getHeure_debut().toString()+","
							+a.getDuree().toString()+","+a.getDate().toString()+"\n").getBytes(),StandardOpenOption.APPEND);
			 }else if(name.equals("Sites.csv") || name.equals("SitesCopy.csv")) {
				 Files.write(file, (s.getNom()+","+s.isSpeleoRequis()+","+s.getDuree()+","
			 +s.getCoordonnees()+","+s.getEquipementRequis()+"\n").getBytes(),StandardOpenOption.APPEND);
			 }else if(name.equals("voitures.csv") || name.equals("VoituresCopy.csv")) {
				 Files.write(file, (b.getId()+","+b.getNom()+","+b.getPrenom()+","
						 +b.getVoiture().getNbPlace()+","+b.getVoiture().getImmatriculation()+","+b.getVoiture().getModele()+"\n").getBytes(),StandardOpenOption.APPEND);
			 }else if(name.equals("contraintes.csv") || name.equals("ContraintesCopy.csv")) {
				 Files.write(file, (b.getId()+","+b.getNom()+","+b.getPrenom()+","
						 +b.getContraintes().get(b.getContraintes().size()-1).getD()+","
						 +b.getContraintes().get(b.getContraintes().size()-1).getH1()+","
						 +b.getContraintes().get(b.getContraintes().size()-1).getH2()+"\n").getBytes(),StandardOpenOption.APPEND);
			 }else if(name.equals("equipes.csv") || name.equals("EquipesCopy.csv")) {
				 for(int i=0;i<org.getListe_equipe().size();i++) {
					 
					 String str=("Equipe "+(i+1));
					 System.out.println("ICI :"+org.recupEquipe(i+1).toString());
					 for(int j=0;j<org.recupEquipe(i+1).getNbBenevole();j++) {
						 str+=","+org.recupEquipe(i+1).getBenevoles().get(j).getId();
					 }
					 str+="\n";
					 Files.write(file,(str).getBytes(),StandardOpenOption.APPEND);
				 }
				 
			 }
			
		} catch (IOException e) {
			System.out.println("une erreur est survenue");
			return;
		} 
	}
	
	public static void deleteLigne(String fileName, int line, Object[] o) {
		String nameNewFile = "";
		if(fileName.equals("resources/benevoles.csv")) {
			nameNewFile = "resources/BenevolesCopy.csv";
		}else if(fileName.equals("resources/voitures.csv")) {
			nameNewFile = "resources/VoituresCopy.csv";
		}else if(fileName.equals("resources/Sites.csv")) {
			nameNewFile = "resources/SitesCopy.csv";
		}else if(fileName.equals("resources/edt.csv")) {
			
			nameNewFile = "resources/edtcpy.csv";
		}
		
		File newFile = new File(nameNewFile);
		Path file = Paths.get(fileName);
		List<String> list = null;
		
		try {
			if(newFile.createNewFile()) {
			
			}
		} catch (IOException e1) {
			System.out.println("fichier non creer");
			return;
			
		}
		
	
		
		try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		if(fileName.equals("resources/edt.csv")) {
			for (int i = 0; i < list.size(); i++) {
				 String[] split = list.get(i).split(separator);
				 String[] date = split[4].split("/");
				 int jour = Integer.valueOf(date[0]);
				 int mois= Integer.valueOf(date[1]);
				 int annee= Integer.valueOf(date[2]);
				 int Debut = Integer.valueOf((split[2].split("H"))[0])*60+Integer.valueOf((split[2].split("H"))[1]);
				 int duree = Integer.valueOf((split[3].split("H"))[0])*60+Integer.valueOf((split[3].split("H"))[1]);
				 int fin = Debut+duree;
				 int myDate = ((Heure)o[0]).getH()*60+((Heure)o[0]).getM();
				 if((jour == ((Date)o[2]).getJour()) && (mois == ((Date)o[2]).getMois()) && (annee == ((Date)o[2]).getAnnee()) //on regarde si c'est le meme jour
					&& split[0].equals((String)o[1]) && split[1].equals((String)o[3]) //on regarde si c'est la meme equipe et le meme site
				    && myDate >= Debut && myDate <= fin ){ //on regarde si l'horraire corespond
					 System.out.println(i);
				 }else {
					 try {
						 for(int j=0;j<split.length;j++) {
							 Files.write(Paths.get(nameNewFile), (split[j]).getBytes(),StandardOpenOption.APPEND);
							 if(j==split.length-1) {
								 Files.write(Paths.get(nameNewFile), ("\n").getBytes(),StandardOpenOption.APPEND);
							 }else {
								 Files.write(Paths.get(nameNewFile), (separator).getBytes(),StandardOpenOption.APPEND);
							 }
						 }
						} catch (IOException e) {
							e.printStackTrace();
						}
					
				 }
			}
			 Csv.getResource(fileName).delete();
				
				if(newFile.renameTo(new File(fileName))) {
						
				}else {
						
				}
			return;
		}
		
		if(fileName.equals("resources/voitures.csv")) {
			for (int i = 0; i < list.size(); i++) {
				 String[] split = list.get(i).split(separator);
				 if(Integer.valueOf(split[0]) != line) {
					 try {
						 for(int j=0;j<split.length;j++) {
							 Files.write(Paths.get(nameNewFile), (split[j]).getBytes(),StandardOpenOption.APPEND);
							 if(j==split.length-1) {
								 Files.write(Paths.get(nameNewFile), ("\n").getBytes(),StandardOpenOption.APPEND);
							 }else {
								 Files.write(Paths.get(nameNewFile), (separator).getBytes(),StandardOpenOption.APPEND);
							 }
						 }
						} catch (IOException e) {
							e.printStackTrace();
						}
				 }
			}
			
		}else {
			for (int i = 0; i < list.size(); i++) {
				 String[] split = list.get(i).split(separator);
				 if(i != line) {
					 try {
						 for(int j=0;j<split.length;j++) {
							 Files.write(Paths.get(nameNewFile), (split[j]).getBytes(),StandardOpenOption.APPEND);
							 if(j==split.length-1) {
								 Files.write(Paths.get(nameNewFile), ("\n").getBytes(),StandardOpenOption.APPEND);
							 }else {
								 Files.write(Paths.get(nameNewFile), (separator).getBytes(),StandardOpenOption.APPEND);
							 }
						 }
						} catch (IOException e) {
							e.printStackTrace();
						}
				 }else {
					 if(fileName.equals("resources/benevoles.csv")) {
						if(Integer.valueOf(split[6])!=0) {
							deleteLigne("resources/voitures.csv", Integer.valueOf(split[0]),null);
						}
						deleteOneBeneFromEquipe(split[0]);
					 }
				 }
			 }
		}
		
		
		Csv.getResource(fileName).delete();
		
		if(newFile.renameTo(new File(fileName))) {
			
		}else {
			
		}

	}
	
	private static void deleteOneBeneFromEquipe(String valueOf) {		
		File newFile = new File("resources/equipesCpy.csv");
		Path file = Paths.get("resources/equipes.csv");
		List<String> list = null;
		
		try {
			if(newFile.createNewFile()) {
			
			}
		} catch (IOException e1) {
			System.out.println("fichier non creer");
			return;
			
		}
		
		try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		
		for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 for(int j =0;j<split.length;j++) {
				 if(!split[j].equals(valueOf)) {
					 if(!split[j].equals("")) {
						 try {
							Files.write(Paths.get("resources/equipesCpy.csv"), (split[j]+(separator)).getBytes(),StandardOpenOption.APPEND);
						} catch (IOException e) {
							e.printStackTrace();
						}
					 }
				 }
			 }
			 try {
					Files.write(Paths.get("resources/equipesCpy.csv"), ("\n").getBytes(),StandardOpenOption.APPEND);
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		Csv.getResource("resources/equipes.csv").delete();
		
		if(newFile.renameTo(new File("resources/equipes.csv"))) {
			
		}else {
			
		}
	}

	public static void deleteAllInFile(String fileName, File fileB) {
		String nameNewFile = "resources/CsvCopy.csv";
		File newFile = new File(nameNewFile);
		try {
			if(newFile.createNewFile()) {
				System.out.println("fichier creer");
			}
		} catch (IOException e1) {
			System.out.println("fichier non creer");
			return;
		}
		fileB.delete();
		newFile.renameTo(new File(fileName));
		System.out.println("contenu du fichier suprimer !");
		

	} 
	
	public static void modifierCsvSite (List<Site> liste_site) {
		String nameNewFile = "resources/SitesCopy.csv";
		File newFile = new File(nameNewFile);
		
		try {
			if(newFile.createNewFile()) {
			
			}
		} catch (IOException e1) {
			System.out.println("fichier non creer");
			return;
			
		}
		
		
		
		for(int i=0;i<liste_site.size();i++) {
			writeInFile(nameNewFile, liste_site.get(i));
		}
		
		Csv.getResource("resources/Sites.csv").delete();
		
		if(newFile.renameTo(new File("resources/Sites.csv"))) {
			
		}else {
			
		}
	}
	
	public static void modifierCsvBenevole (List<Benevole> liste_bene) {
		String nameNewFile = "resources/BenevoleCopy.csv";
		File newFile = new File(nameNewFile);
		
		
		try {
			if(newFile.createNewFile()) {
			
			}
		} catch (IOException e1) {
			System.out.println("fichier non creer");
			return;
			
		}
		
	
		for(int i=0;i<liste_bene.size();i++) {
			writeInFile(nameNewFile, liste_bene.get(i));
		}
		
		modifierCsvVoiture(liste_bene);
		
		Csv.getResource("resources/benevoles.csv").delete();
		
		if(newFile.renameTo(new File("resources/benevoles.csv"))) {
			
		}else {
			
		}
	}
		
	public static void modifierCsvVoiture (List<Benevole> liste_bene) {
			String nameNewFile = "resources/VoituresCopy.csv";
			File newFile = new File(nameNewFile);
			
			
			try {
				if(newFile.createNewFile()) {
				
				}
			} catch (IOException e1) {
				System.out.println("fichier non creer");
				return;
				
			}
			
		
			for(int i=0;i<liste_bene.size();i++) {
				if(liste_bene.get(i).getVoiture().getNbPlace()!=0) {
					writeInFile(nameNewFile, liste_bene.get(i));
				}
				
			}
			
			Csv.getResource("resources/voitures.csv").delete();
			
			if(newFile.renameTo(new File("resources/voitures.csv"))) {
				
			}else {
				
			}
		}
	
	public static void modifierCsvParam (Map<String,List<String>> param) {
		String nameNewFile = "resources/parametreCpy.csv";
		File newFile = new File(nameNewFile);
		Path file = Paths.get("resources/parametre.csv");

		List<String> list = null;
		
		
		try {
			if(newFile.createNewFile()) {
			
			}
		} catch (IOException e1) {
			System.out.println("fichier non creer");
			return;
			
		}
		
		Path newFilePath = Paths.get("resources/parametreCpy.csv");
		
		 try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	     }
		
		boolean ok = false;
		 for (int i = 0; i < list.size(); i++) {
			String[] split = list.get(i).split(separator);
			split = list.get(i).split(separator);
			ok = false;
			for (Map.Entry<String, List<String>> entry : param.entrySet()) {
		        if(entry.getKey().equals(split[0])) {
		        	try {
		 				Files.write(newFilePath, (split[0]).getBytes(),StandardOpenOption.APPEND);
		 			} catch (IOException e1) {
		 				e1.printStackTrace();
		 			}
		        	for(int j=0;j<entry.getValue().size();j++) {
						 try {
							Files.write(newFilePath, (","+entry.getValue().get(j)).getBytes(),StandardOpenOption.APPEND);
						} catch (IOException e) {
							e.printStackTrace();
						}
					 } 
		
		        	try {
						Files.write(newFilePath, ("\n").getBytes(),StandardOpenOption.APPEND);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
		        	ok = true;
		        	continue;
		        }
		    }
			if(!ok) {
				try {
	 				Files.write(newFilePath, (split[0]).getBytes(),StandardOpenOption.APPEND);
	 			} catch (IOException e1) {
	 				e1.printStackTrace();
	 			}
				for(int j=1;j<split.length;j++) {
					 try {
						Files.write(newFilePath, (","+split[j]).getBytes(),StandardOpenOption.APPEND);
					} catch (IOException e) {
						e.printStackTrace();
					}
				 } 
				 try {
					Files.write(newFilePath, ("\n").getBytes(),StandardOpenOption.APPEND);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		 
		
			 
		
		Csv.getResource("resources/parametre.csv").delete();
		
		if(newFile.renameTo(new File("resources/parametre.csv"))) {
			
		}else {
			
		}
	}

	public static String recupIdFromName(String string) {
		
		Path file = Paths.get("resources/benevoles.csv");
		List<String> list = null;
		try {
			 list = Files.readAllLines(file);
		} catch (IOException e) {
	    	 System.out.println("Impossible de lire le fichier");
	    }
		 
		 if (list.size() < 1) {
	        System.out.println("Il n'y a rien dans le fichier");
	        return null;
	     }
		 
		 
		 for (int i = 0; i < list.size(); i++) {
			 String[] split = list.get(i).split(separator);
			 
					 if(split[1].equals(string.split(" ")[0]) && split[2].equals(string.split(" ")[1])) {
						return split[0];
					 }
					 
					
		 }
		 
		
		return null;
	}

	
	
}


