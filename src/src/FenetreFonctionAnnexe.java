package src;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class FenetreFonctionAnnexe{
	
	
	
	static void AjouterEventListenerScrolled(final ScrolledComposite sc2,final Composite c2,final int x,final int y) {
		sc2.addListener( SWT.Resize, new Listener() {
			public void handleEvent(Event event) {
				  sc2.setMinSize(x, y);
				  c2.layout();
				}
		} );
	}
	
	static void ajouterBenevole(Shell s) {
        ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
        Composite c = (Composite) (sc.getChildren()[0]);
        Control[] cont = c.getChildren();
        List<Object> benev=new ArrayList<Object>();
        List<List<Object>> objects=new ArrayList<List<Object>>();
        List<Object> liste = new ArrayList<Object>(5);
        int cpt=0;
        for(int i=0;i<cont.length;i++) {
            if(i>11) {
            	if(!(cont[i] instanceof Label)){

            		if(cont[i] instanceof DateTime) {
            			liste.add(cont[i]);
            			cpt++;
            		}else {
            			Control[] cc=((Composite) cont[i]).getChildren();
            			for(int b=0;b<cc.length;b++) {
            				if(!(cc[b] instanceof Label)) {
            					liste.add(cc[b]);
            					cpt++;
            				}
            			}
            		}
                    
            		if(cpt==5) {
                    	cpt=0;
                    	objects.add(liste);
                    	liste=new ArrayList<Object>(5);
                    }
                    
                }
            }else {
                if(!(cont[i] instanceof Label)){
                    benev.add(cont[i]);
                }
            }
        }
        
        
        
        int id = Csv.RecupidMax("resources/benevoles.csv")+1;
        Control[] cont2=((Composite)benev.get(5)).getChildren();
        Benevole b = new Benevole(id,((Text)benev.get(0)).getText(), ((Text)benev.get(1)).getText(), ((Button)benev.get(4)).getSelection(), 
        		Integer.valueOf(((Combo)benev.get(2)).getText()), Integer.valueOf(((Combo)benev.get(3)).getText()),
        		new Voiture(Integer.valueOf(((Combo)cont2[0]).getText()),((Text)cont2[2]).getText(), ((Text)cont2[4]).getText()));
	  	Csv.writeInFile("resources/benevoles.csv", b);
	  	if(Integer.valueOf(((Combo)cont2[0]).getText())!=0) {
  		  Csv.writeInFile("resources/voitures.csv", b);
	  	}
	  	

	  	for(int a=0;a<objects.size();a++) {
        	if( (!(((Combo)objects.get(a).get(1)).getText().equals("")) && !(((Combo)objects.get(a).get(2)).getText().equals(""))) || (!(((Combo)objects.get(a).get(3)).getText().equals("")) &&  !(((Combo)objects.get(a).get(4)).getText().equals("")))) {
	    		  Date date =new Date(((DateTime)objects.get(a).get(0)).getDay(), ((DateTime)objects.get(a).get(0)).getMonth()+1, ((DateTime)objects.get(a).get(0)).getYear());
	    		  Heure h1=new Heure(-1,-1);
	    		  if(!(((Combo)objects.get(a).get(1)).getText().equals("")) && !(((Combo)objects.get(a).get(2)).getText().equals(""))) {
	    			  h1.setH(Integer.valueOf(((Combo)objects.get(a).get(1)).getText()));
	    			  h1.setM(Integer.valueOf(((Combo)objects.get(a).get(2)).getText()));
	    		  }
	    		  Heure h2=new Heure(-1,-1);
	    		  if(!(((Combo)objects.get(a).get(3)).getText().equals("")) &&  !(((Combo)objects.get(a).get(4)).getText().equals(""))) {
	    			  h2.setH(Integer.valueOf(((Combo)objects.get(a).get(3)).getText()));
	    			  h2.setM(Integer.valueOf(((Combo)objects.get(a).get(4)).getText()));
	    		  }
	    		  Contrainte contrainte=new Contrainte(date, h1, h2);
	    		  b.ajouterContrainte(contrainte);
	    		  Csv.writeInFile("resources/contraintes.csv",b);
	    	  }
        		
        }

    }
	
	public static void ajouterContrainte(Shell s) {
        ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
        Composite c2 = (Composite) (sc.getChildren()[0]);
        
        Label label10 = new Label(c2,SWT.NONE);
          label10.setText("Date");
          
          new DateTime(c2, SWT.CALENDAR);
    
          Label label11 = new Label(c2, SWT.NONE);
          label11.setText("Heure Arrivée");
          GridLayout layoutDuree_A = new GridLayout();
            layoutDuree_A.numColumns = 3;
            final Composite c3_A = new Composite(c2, SWT.NONE);
            c3_A.setLayout(layoutDuree_A);
          final Combo comboHeure_A = new Combo(c3_A, SWT.DROP_DOWN);
          String[] itemsHeure_A = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
                  ,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
          comboHeure_A.setItems(itemsHeure_A);

          
          
          Label labelH_A = new Label(c3_A, SWT.NONE);
          labelH_A.setText("H");
          final Combo comboMinute_A = new Combo(c3_A, SWT.DROP_DOWN);
          String[] itemsMinute_A = new String[] { "00", "10", "20","30","40","50"};
          comboMinute_A.setItems(itemsMinute_A);
          Label label12 = new Label(c2, SWT.NONE);
          label12.setText("Heure Départ");
          GridLayout layoutDuree_D = new GridLayout();
            layoutDuree_D.numColumns = 3;
            final Composite c3_D = new Composite(c2, SWT.NONE);
            c3_D.setLayout(layoutDuree_D);
          final Combo comboHeure_D = new Combo(c3_D, SWT.DROP_DOWN);
          String[] itemsHeure_D = new String[] { "00", "01", "02","03","04","05","06","07", "08", "09","10"
                  ,"11","12","13","14", "15", "16","17","18","19","20","21", "22", "23"};
          comboHeure_D.setItems(itemsHeure_D);
      
          
          Label labelH = new Label(c3_D, SWT.NONE);
          labelH.setText("H");
          final Combo comboMinute_D = new Combo(c3_D, SWT.DROP_DOWN);
          String[] itemsMinute_D = new String[] { "00", "10", "20","30","40","50"};
          comboMinute_D.setItems(itemsMinute_D);
    
          sc.setMinSize(c2.computeSize(SWT.DEFAULT, SWT.DEFAULT));
          c2.layout();
    }

	public static void ajouterSite(Shell s) {
		ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
        Composite c = (Composite) (sc.getChildren()[0]);
        Control[] cont = c.getChildren();
        List<Object> sites=new ArrayList<Object>();
        for(int i=0;i<cont.length;i++) {
        	if(!(cont[i] instanceof Label)) {
        		if(cont[i] instanceof Composite) {
	    			Control[] cc=((Composite) cont[i]).getChildren();
	    			for(int b=0;b<cc.length;b++) {
	    				if(!(cc[b] instanceof Label)) {
	    					sites.add(cc[b]);
	    				}
        			}
        		} else {
        			sites.add(cont[i]);
        		}
        	}
        }
        
        Site site = new Site(((Text)sites.get(0)).getText(),((Button)sites.get(1)).getSelection(),new Heure(Integer.valueOf(((Combo)sites.get(2)).getText()),
  			  Integer.valueOf(((Combo)sites.get(3)).getText())),((Text)sites.get(4)).getText(),((Text)sites.get(5)).getText());
  	  	Csv.writeInFile("resources/Sites.csv", site);
		
	}
	
	public static void setDrag(final Label label) {

	    Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
	    int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;

	    final DragSource source = new DragSource(label, operations);
	    source.setTransfer(types);
	    source.addDragListener(new DragSourceListener() {
	      public void dragStart(DragSourceEvent event) {
	        event.doit = (label.getText().length() != 0);
	      }

	      public void dragSetData(DragSourceEvent event) {
	        event.data = label.getText();
	      }

	      public void dragFinished(DragSourceEvent event) {
	        if (event.detail == DND.DROP_MOVE)
	          label.setText(label.getText());
	        
	      }
	    });
	}
	    
	    public static void setDrop(final Label label) {

	    	 Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
	 	    int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;

	 	    
	 	    DropTarget target = new DropTarget(label, operations);
	 	    target.setTransfer(types);
	 	    target.addDropListener(new DropTargetAdapter() {
	 	      public void drop(DropTargetEvent event) {
	 	        if (event.data == null) {
	 	          event.detail = DND.DROP_NONE;
	 	          return;
	 	        }
	 	        label.setText((String)event.data);
	 	        label.pack();
	 	        
	 	        }
	 	    });
	  }
	    
	    public static void setDropEDT(final Label label,final int equipe, final Heure heure,final int cpt, final Date date, final FenetreV2 f) {

	    	 Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
	 	    int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK;

	 	    
	 	    DropTarget target = new DropTarget(label, operations);
	 	    target.setTransfer(types);
	 	    target.addDropListener(new DropTargetAdapter() {
	 	      public void drop(DropTargetEvent event) {
	 	        if (event.data == null) {
	 	          event.detail = DND.DROP_NONE;
	 	          return ;
	 	        }
	 	        
	 	        Site s=(Site) Csv.recupLigne("resources/Sites.csv",-1, (String) event.data);
	 	        System.out.println(equipe+" "+heure);
	 	        @SuppressWarnings("unchecked")
				List<String> benev_list = (List<String>) Csv.recupLigne("resources/equipes.csv", equipe, "");
	 	        Equipe e = new Equipe(equipe+1);
	 	        for(int i=1;i<benev_list.size();i++) {
	 	        	Benevole b = (Benevole) Csv.recupLigne("resources/benevoles.csv", Integer.valueOf(benev_list.get(1)), "");
	 	        	e.ajouterBenevole(b);
	 	        }
	 	        
	 	        Heure h = new Heure();
	 	       
	 	       @SuppressWarnings("unchecked")
	 	       List<String> param = (List<String>) Csv.recupLigne("resources/parametre.csv", -1, date.nameDay());
	 	       h.setH(Integer.valueOf((param.get(0).split("H"))[0]));
	 		   h.setM(Integer.valueOf((param.get(0).split("H"))[1]));
	 		   
	 		   for(int m=0;m<cpt;m++) {
	 			   	int heures = h.getH()+heure.getH();
	 				int minutes = h.getM()+heure.getM();
	 				
	 				while(minutes >= 60) {
	 					heures += 1;
	 					minutes %= 60;
	 				}
	 				
	 				h.setH(heures);
	 				h.setM(minutes);
	 		   }
	 		   	
	 	        Affectation a=new Affectation(e,s,h,s.getDuree(),date);
	 	        Csv.writeInFile("resources/edt.csv", a);
	 	        disposePH(f.getShell());
	        	f.afficherPlageHorraire(f.getC().get(3),s,e);
		        return;
	 	        }
	 	    });
		
	  }

		public static void envoyerEquipe(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[1]);
	        Composite c = (Composite) (sc.getChildren()[0]);
	        Control[] cont = c.getChildren();
	        Object nb = Csv.recupLigne("resources/parametre.csv", 0,"nb_personne_equipe");
	        int nb_e = (int)(nb);
	        double nb_benevoles=Csv.recupSizeList("resources/benevoles.csv");
		    double nb_equipe_temp =((double)nb_benevoles)/nb_e;
		    final int nb_equipe = (int)Math.ceil(nb_equipe_temp);
		    List<List<String>> tab_membre=new ArrayList<List<String>>(nb_equipe);
		    int cpt=0;
	        for(int i=nb_e+2;i<cont.length;i++) {

	        	if(cpt!=nb_equipe) {
	        		if(i%(nb_e+1)==0) {
	        			cpt++;
	        			continue;
	        		}
	        		try {
						tab_membre.get(cpt).add(((Label)cont[i]).getText());
					} catch (Exception e) {
						tab_membre.add(new ArrayList<String>());
						tab_membre.get(cpt).add(((Label)cont[i]).getText());
					}
	        		
	        		
	        		
	        	}
	        	
	        	
	        }
	        System.out.println(tab_membre);
	        

			Csv.deleteAllInFile("resources/equipes.csv", new File("resources/equipes.csv"));
			Path file = Paths.get("resources/equipes.csv");
			for(int j=0;j<tab_membre.size();j++) {
				try {
					Files.write(file,("Equipe "+(j+1)).getBytes(),StandardOpenOption.APPEND);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for(int k=0;k<tab_membre.get(j).size();k++) {
					 try {
						if(!tab_membre.get(j).get(k).equals(".....")) {
							Files.write(file,(","+Csv.recupIdFromName(tab_membre.get(j).get(k))).getBytes(),StandardOpenOption.APPEND);
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					Files.write(file,("\n").getBytes(),StandardOpenOption.APPEND);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
					
		}

		public static void supprimerBenevole(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c = (Composite) (sc.getChildren()[0]);
			Control[] cont = c.getChildren();
	  	    int line = 0;
	  	    for(int i=10;i<cont.length;i++) {
	  	    	
	  	    	if(i%10 == 0 && i!=cont.length-1) {
	  	    		if(((Button) cont[i]).getSelection()) {
	  	    			
	  	    			Csv.deleteLigne("resources/benevoles.csv", line,null);
	  	    			line-=1;
	  	    		}
	  	    		line+=1;
	  	    	}
	  	    }
			
		}

		public static void supprimerSite(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c = (Composite) (sc.getChildren()[0]);
			Control[] cont = c.getChildren();
	  	    int line = 0;
	  	    for(int i=6;i<cont.length;i++) {
	  	    	
	  	    	if(i%6 == 0 && i!=cont.length-1) {
	  	    		if(((Button) cont[i]).getSelection()) {
	  	    			
	  	    			Csv.deleteLigne("resources/Sites.csv", line,null);
	  	    			line-=1;
	  	    		}
	  	    		line+=1;
	  	    	} 
	  	    }
	  	    
		}

		public static void modifierBenevole(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			List<Benevole> res=new ArrayList<Benevole>();
	  		
	  	    int line = 0;
	  	    String nom="",prenom="",immatriculation="",modele="";
  	    	int speleo=-1, ident=-1,nb_place=0;
  	    	boolean have_permis=false;
	  	    for(int i=9;i<c.length;i++) {	

	  	    		int value=i%9;
		  	    	switch(value) {
		  	    	case 0:
		  	    		nom=((Text)(c[i])).getText();
		  	    		break;
		  	    	case 1:
		  	    		prenom=((Text)(c[i])).getText();
		  	    		break;
					case 2:
						if(((Combo)(c[i])).getText().equals("-1")) {
			  	    		speleo = 0;
			  	    	}else {
			  	    		speleo = Integer.valueOf(((Combo)(c[i])).getText());
			  	    	}	
							
						break;
					case 3:
						if(((Combo)(c[i])).getText().equals("-1")) {
			  	    		ident = 0;
			  	    	}else {
			  	    		ident = Integer.valueOf(((Combo)(c[i])).getText());
			  	    	}	
						break;
					case 4:
						have_permis=((Button)c[i]).getSelection();
						break;
					case 6:
						
						nb_place=Integer.valueOf(((Combo)(c[i])).getText());
						break;
					case 7:
						
						immatriculation=((Text)(c[i])).getText();
						break;
					case 8:
						
						modele=((Text)(c[i])).getText();
						Voiture v=new Voiture(nb_place,immatriculation,modele);
						Benevole b = new Benevole(line,nom, prenom, have_permis, speleo, ident,v);
						res.add(b);
						line++;
						break;
		  	    	}
	  	    }
	  	    Csv.modifierCsvBenevole(res);
			
		}

		public static void modifierSite(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			List<Site> res=new ArrayList<Site>();
			
			String nom="",coordonnees="",equipement="";
  	    	int heure=-1, minute=-1;
  	    	boolean is_speleo=false;
	  	    for(int i=5;i<c.length;i++) {	
	  	    		int value=i%5;
		  	    	switch(value) {
		  	    	case 0:
		  	    		nom=((Text)(c[i])).getText();
		  	    		break;
		  	    	case 1:
		  	    		is_speleo=((Button)c[i]).getSelection();
		  	    		break;
					case 2:
						Control[] c3=((Composite)c[i]).getChildren();
						if(((Combo)(c3[0])).getText().equals("-1")) {
			  	    		heure = 0;
			  	    	}else {
			  	    		heure = Integer.valueOf(((Combo)(c3[0])).getText());
			  	    	}	
						if(((Combo)(c3[2])).getText().equals("-1")) {
			  	    		minute = 0;
			  	    	}else {
			  	    		minute = Integer.valueOf(((Combo)(c3[2])).getText());
			  	    	}	
						break;
					
					case 3:
						coordonnees=((Text)(c[i])).getText();
						break;
					case 4:
						equipement=((Text)(c[i])).getText();
						Site si = new Site(nom,is_speleo,new Heure(heure,minute),coordonnees,equipement);
						res.add(si);
						break;
					
		  	    	}
	  	    }
	  	    Csv.modifierCsvSite(res);
			
		}

		public static void modifParamEquipe(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			
			Map<String,List<String>> param = new HashMap<String, List<String>>();;
			List<String> l = new ArrayList<>();
			l.add(((Text)(c[1])).getText());
			System.out.println(l);
			param.put("nb_personne_equipe",l);
			
			Csv.modifierCsvParam(param);
			
			
		}

		public static void modifParamHorraire(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			
			Map<String,List<String>> param = new HashMap<String, List<String>>();;
			List<String> l = new ArrayList<>();
			
			//echelle de temps
			l.add(((Combo)(c[1])).getText());
			param.put("echelle_temps", l);
			
			//les horraires
			for(int i=2;i<c.length-1;i++) {
				List<String> l1 = new ArrayList<>();
				Control[] comp = ((Composite)(c[i+1])).getChildren();
				String heure_debut = (((Combo)comp[1]).getText())+"H"+(((Combo)comp[3]).getText());
				String heure_fin= (((Combo)comp[5]).getText())+"H"+(((Combo)comp[7]).getText());
				l1.add(heure_debut);
				l1.add(heure_fin);
				param.put(((Label)c[i]).getText(),l1);
				i++;
			}
			Csv.modifierCsvParam(param);
			
			
		}

		public static Date actualiserHorraire(Shell s, Date d,int a) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[1]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			
			Label l = (Label) c[1];
			if(a>0) {
				d.addJour(a);
			}else if(a < 0) {
				d.addJour(a);
			}
			l.setText(d.toString());
			return d;
		}

		public static void disposePH(Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[3]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			
			for(Control cE: c) {
				((Label)(cE)).dispose();
			}
		}

		public static void addOrSubsract(int i,Shell s) {
			ScrolledComposite sc = (ScrolledComposite) (s.getChildren()[0]);
	        Composite c1 = (Composite) (sc.getChildren()[0]);
			Control[] c = c1.getChildren();
			int add = Integer.valueOf(((Text)c[1]).getText())+i;
			if(add <0) {
				add = 0;
			}
			if(add > Csv.recupSizeList("resources/benevoles.csv")) {
				add = Csv.recupSizeList("resources/benevoles.csv");
			}
			((Text)c[1]).setText(String.valueOf(add)); 
			
		}

		public static void supprimerSiteEdt(Label label) {
			System.out.println(label.getText());
			Object[] o = (Object[]) label.getData();
			Object[] res = new Object[4];
			 Heure h = new Heure();
	 	       
	 	     @SuppressWarnings("unchecked")
	 	     List<String> param = (List<String>) Csv.recupLigne("resources/parametre.csv", -1, ((Date)o[3]).nameDay());
	 	     h.setH(Integer.valueOf((param.get(0).split("H"))[0]));
	 		 h.setM(Integer.valueOf((param.get(0).split("H"))[1]));
	 		   
	 		 for(int m=0;m<(int)o[1];m++) {
	 			int heures = h.getH()+((Heure)o[2]).getH();
	 		    int minutes = h.getM()+((Heure)o[2]).getM();
	 				
	 			while(minutes >= 60) {
	 				heures += 1;
	 				minutes %= 60;
	 			}
	 				
	 			h.setH(heures);
	 			h.setM(minutes);
	 		  }
	 		  res[0] = h;
	 		  res[1] = o[0];
	 		  res[2] = o[3];
	 		  res[3] = label.getText();
	 	      Csv.deleteLigne("resources/edt.csv", -1, res);

			
		}

		public static void disposeSite(Composite composite) {
			Control[] c = composite.getChildren();
			
			for(Control cE: c) {
				((Label)(cE)).dispose();
			}
			
		}

}
